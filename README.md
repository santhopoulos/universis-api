# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

This repository provides information on the API methods and the schema models.

Check them out!
https://api.universis.io/api-docs/

## How to install
  Check out the installation instructions in [INSTALL.md](INSTALL.md)


## Services
Summary of available routes

* Instructors: https://gitlab.com/universis/universis-api/blob/master/server/routes/instructors.md
