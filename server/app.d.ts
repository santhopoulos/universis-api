// Universis Project Copyright (c) 2020, All rights reserved LGPL-3.0-or-later license
// eslint-disable-next-line no-unused-vars
import {Application} from "express-serve-static-core";

declare const app: Application;

export default app;
