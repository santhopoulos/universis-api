import createError from 'http-errors';
import express from 'express';
import engine from 'ejs-locals';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import sassMiddleware from 'node-sass-middleware';
import {
    ExpressDataApplication,
    serviceRouter,
    dateReviver,
    getMetadataDocument,
    getEntitySetIndex
} from '@themost/express';
import indexRouter from './routes/index';
import apiDocsRouter from './routes/api-docs';
import {instructorsRouter} from './routes/instructors';
import {studentsRouter} from './routes/students';
import contentRouter from './routes/content';
import {validateScope} from './routes/scope';
import {messagesRouter} from './routes/messages';
import {accessLogHandler} from './log';
import {noCache, TraceContext, addTrace} from "./utils";
import {ConsoleLogger} from './services/console-logger';
import {TraceUtils} from "@themost/common";

import cors from 'cors';
import passport from 'passport';
import i18n from 'i18n';
import {useHttpBearerAuthorization} from './auth';


// import intl extensions
import './extensions/intl-extensions';
// import data model extensions for adding additional query options in system queries
// e.g. /api/courseClasses/?$filter=year eq currentYear() and period eq currentPeriod()
import './extensions/data-model-extensions';
// import function context extensions for calculating values for data model attributes
// e.g.
// {
//     "@id": "http://universis.io/schemas/dueDate",
//     "name": "dueDate",
//     "description": "The due date of something.",
//     "type": "DateTime",
//     "value": "javascript:return this.today();"
// }
import './extensions/functions-context-extensions';
// import data context extensions (localization etc)
import './extensions/data-context-extensions';
import {ScopeAccessConfiguration, DefaultScopeAccessConfiguration} from "./services/scope-access-configuration";

// import response to csv parser
import {csvParser} from "./middlewares/csv";
// import response to xls parser
import {xlsParser} from "./middlewares/xls";
import {TranslateService} from "./services/translate-service";
import {icsParser} from "./middlewares/ics";
import { DefaultServerSentEventService, ServerSentEventService } from '@universis/sse';

// initialize express application
// https://expressjs.com/en/api.html#express
let app = express();

// setup the logger
// https://github.com/expressjs/morgan
app.use(accessLogHandler());

TraceUtils.useLogger(new ConsoleLogger());
if (process.env.NODE_ENV === 'development') {
    TraceUtils.level('debug');
}


// data context setup
// https://github.com/kbarbounakis/most-data-express#Usage
const dataApplication = new ExpressDataApplication(path.resolve(__dirname, 'config'));

if (dataApplication.getConfiguration().hasStrategy(ScopeAccessConfiguration) === false) {
    // set OAuth2 scope access configuration strategy
    dataApplication.getConfiguration().useStrategy(ScopeAccessConfiguration, DefaultScopeAccessConfiguration);
}

// merge i18n configuration
// https://github.com/mashpie/i18n-node#configure
let i18nConfiguration = Object.assign({
    locales: ['en'],
    defaultLocale: 'en',
    directory: path.resolve(process.cwd(), 'locales'),
    autoReload: false,
    updateFiles: false,
    syncFiles: false,
    objectNotation: true
}, dataApplication.getConfiguration().getSourceAt('settings/i18n'))
// finally configure i18n
i18n.configure(i18nConfiguration);

// register translate service
// note that this service is one of universis api services and may be overridden
if (dataApplication.hasStrategy(TranslateService) === false) {
    dataApplication.useService(TranslateService);
}

// register sse service
// this service may be overridden
if (dataApplication.hasStrategy(ServerSentEventService) === false) {
    dataApplication.useStrategy(ServerSentEventService, DefaultServerSentEventService);
}

// enable CORS
// https://github.com/expressjs/cors#configuring-cors#usage
// todo:: use application configuration to configure cors
const corsOptions = Object.assign({
    origin:true, 
    credentials: true
}, dataApplication.getConfiguration().getSourceAt('settings/cors'));
TraceUtils.info('Application', 'Configuring CORS', corsOptions);
app.use(cors(corsOptions));

// use ejs-locals for all ejs templates
// https://github.com/RandomEtc/ejs-locals#About
app.engine('ejs', engine);

// view engine setup
// https://expressjs.com/en/guide/using-template-engines.html
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// setup logger (for development mode)
// https://github.com/expressjs/morgan
if (process.env.NODE_ENV === 'development') {
    app.use(logger('dev'));
}

// use date reviver for parsing date formatted strings
const jsonOptions = Object.assign({}, dataApplication.getConfiguration().getSourceAt('settings/json'),
    {reviver: dateReviver}
);
app.use(express.json(jsonOptions));

// parse urlencoded payloads
// https://expressjs.com/en/api.html#express.urlencoded
app.use(express.urlencoded({extended: false}));

// use cookie parser
// https://github.com/expressjs/cookie-parser
app.use(cookieParser());

// init i18n for request
app.use(i18n.init);
/**
 * @name Request~context
 * @type {ExpressDataContext}
 */

//use response to csv middleware
app.use(csvParser());

//use response to xls middleware
app.use(xlsParser());
app.use(icsParser());

// use data middleware (register req.context)
// https://github.com/kbarbounakis/most-data-express#Usage
app.use(dataApplication.middleware(app));

// Add logger with the context in scope
app.use(addTrace);

// set data application
app.set('ExpressDataApplication', dataApplication);

// bind express data context localization methods to req
app.use((req, res, next) => {

    // set context locale from request
    req.context.locale  = req.locale;
    /**
     * Implements i18n.__ method to ExpressDataContext
     * @returns {string}
     */
    req.context.__  = function() {
        return i18n.__.apply(req, arguments);
    };
    /**
     * Implements i18n.__n method to ExpressDataContext
     * @returns {string}
     */
    req.context.__n  = function() {
        return i18n.__n.apply(req, arguments);
    };

    // continue
    next();
});

// use passport bearer authorization
useHttpBearerAuthorization();

// use sass middleware
// https://github.com/sass/node-sass-middleware#Usage
app.use(sassMiddleware({
    src: path.join(process.cwd(), 'public'),
    dest: path.join(process.cwd(), 'public'),
    indentedSyntax: false, // true = .sass and false = .scss
    sourceMap: true
}));

// use static files
// https://expressjs.com/en/starter/static-files.html
app.use(express.static(path.join(process.cwd(), 'public')));
// register routes
// https://expressjs.com/en/guide/routing.html
// register index router
app.use('/', indexRouter);
// register api-docs router
app.use('/api-docs', apiDocsRouter(dataApplication.getConfiguration()));
// ***************************************
//
// register api routers section
//
// ***************************************
// register @themost api router
// set entity set index as public endpoint
app.get(/^\/api\/?$/, getEntitySetIndex());
// set metadata service as public endpoint
app.get('/api/\\$metadata', getMetadataDocument());
// noinspection JSCheckFunctionSignatures
app.use('/api', passport.authenticate('bearer', {session: false}), validateScope(), noCache(), serviceRouter);
// content router
app.use('/api/content/', passport.authenticate('bearer', {session: false}), contentRouter);
// instructors router
app.use('/api/instructors', passport.authenticate('bearer', {session: false}), validateScope(), instructorsRouter(dataApplication.getConfiguration()));
// students router
app.use('/api/students', passport.authenticate('bearer', {session: false}), validateScope(), noCache(), studentsRouter(dataApplication.getConfiguration()));
// messages router
app.use('/api/messages/', passport.authenticate('bearer', {session: false}), messagesRouter(dataApplication.getConfiguration()));
// ***************************************

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// default error handler
// noinspection JSUnusedLocalSymbols
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    // if requests accepts json
    // https://expressjs.com/en/api.html#req.accepts
    let status = err.status || err.statusCode || 500;
    if (req.accepts('json')) {

        req.context && req.context.trace && req.context.trace.error('ERROR', err);

        // do not render html error and send error as json
        res.status(status);
        // serialize error
        let error = {};
        Object.getOwnPropertyNames(err).forEach(key => {
            if (process.env.NODE_ENV !== 'development' && key==='stack') {
                return;
            }
            error[key] = err[key];
        });
        const proto = Object.getPrototypeOf(err);
        if (proto && proto.constructor.name) {
            error['type'] = proto.constructor.name;
        }
        return res.json(error);
    }
    // otherwise send html content
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(status);
    res.render('error');
});


module.exports = app;
