import passport from 'passport';
import BearerStrategy from 'passport-http-bearer';
import {HttpForbiddenError, HttpError} from '@themost/common/errors';
import {OAuth2ClientService} from "./services/oauth2-client-service";
import {writeAccessLog} from './log';
import url from 'url';
import _ from 'lodash';

class HttpBearerTokenRequired extends HttpError {
    constructor() {
        super(499, 'A token is required to fulfill the request.');
        this.code = 'E_TOKEN_REQUIRED';
        this.title = 'Token Required';
    }
}

class HttpBearerTokenNotFound extends HttpError {
    constructor() {
        super(498, 'Token was not found.');
        this.code = 'E_TOKEN_NOT_FOUND';
        this.title = 'Invalid token';
    }
}

class HttpBearerTokenExpired extends HttpError {
    constructor() {
        super(498, 'Token was expired or is in invalid state.');
        this.code = 'E_TOKEN_EXPIRED';
        this.title = 'Invalid token';
    }
}

class HttpAccountDisabled extends HttpForbiddenError {
    constructor() {
        super('Access is denied. User account is disabled.');
        this.code = 'E_ACCOUNT_DISABLED';
        this.statusCode = 403.2;
        this.title = 'Disabled account';
    }
}

function writeTokenInfoAccessLog(req, res, startAt, endAt) {
    // format referrer to include original request
    const referrer = url.format({
        protocol: 'http',
        host: req.headers.host,
        pathname: url.parse(req.originalUrl).pathname
    });
    const logReq = Object.assign({
    }, req, {
        // override method
        method: 'GET',
        // override headers
        headers: _.cloneDeep(req.headers),
        // override url for log parsers
        url: '/token/info',
        // override url as /token/info for log parsers
        originalUrl: '/token/info'
    });
    // set new referrer
    logReq.headers['referer'] = referrer;
    const logRes = {
        // this is the only attribute that needs to be passed to morgan
        statusCode: res.statusCode
    };
    writeAccessLog(logReq, logRes, startAt, endAt);
}

/***
 * Implements passport HTTP bearer authorization
 */
function useHttpBearerAuthorization() {
    // passport bearer authorization strategy
    // https://github.com/jaredhanson/passport-http-bearer#usage
    passport.use(new BearerStrategy({
        passReqToCallback: true
        },
        /**
         * @param {Request} req
         * @param {string} token
         * @param {Function} done
         */
        function(req, token, done) {
            /**
             * Gets OAuth2 client services
             * @type {*}
             */
            let client = req.context.getApplication().getStrategy(OAuth2ClientService);
            // if client cannot be found
            if (typeof client === 'undefined') {
                // throw configuration error
                return done(new Error('Invalid application configuration. OAuth2 client service cannot be found.'))
            }
            if (token == null) {
                // throw 499 Token Required error
                return done(new HttpBearerTokenRequired());
            }
            // start log token info request
            const startAt = process.hrtime();
            // get token info
            client.getTokenInfo(req.context, token).then(info => {
                // end log token info request
                const endAt = process.hrtime();
                if (typeof info === 'undefined' || info === null) {
                    // write access log for token expired
                    writeTokenInfoAccessLog(req, { statusCode: 200 }, startAt, endAt);
                    // the specified token cannot be found - 498 invalid token with specific code
                    return done(new HttpBearerTokenNotFound());
                }
                // if the given token is not active throw token expired - 498 invalid token with specific code
                if (!info.active) {
                    // write access log for token expired
                    writeTokenInfoAccessLog(req, { statusCode: 200 }, startAt, endAt);
                    return done(new HttpBearerTokenExpired());
                }
                // find user from token info
                return req.context.model('User').where('name').equal(info.username).silent().getItem().then( user => {
                    // user cannot be found and of course cannot be authenticated (throw forbidden error)
                    if (typeof user === 'undefined' || user === null) {
                        // write access log for forbidden
                        writeTokenInfoAccessLog(req, { statusCode: 200 }, startAt, endAt);
                        return done(new HttpForbiddenError());
                    }
                    // check if user has enabled attribute
                    if (user.hasOwnProperty('enabled') && !user.enabled) {
                        writeTokenInfoAccessLog(req, { statusCode: 200 }, startAt, endAt);
                        //if user.enabled is off throw forbidden error
                        return done(new HttpAccountDisabled('Access is denied. User account is disabled.'));
                    }
                    // create native context object because context has not been initialized yet
                    const logReq = Object.assign({ }, req, {
                        context: {
                            user: {
                                name: user.name
                            }
                        }
                    });
                    // response ok
                    const logRes = {
                        statusCode: 200
                    };
                    // write log
                    writeTokenInfoAccessLog(logReq, logRes, startAt, endAt);
                    // otherwise return user data
                    return done(null, {
                        "name": user.name,
                        "authenticationProviderKey": user.id,
                        "authenticationType":'Bearer',
                        "authenticationToken": token,
                        "authenticationScope": info.scope
                    });
                });
            }).catch(err => {
                // end log token info request with error
                const endAt = process.hrtime();
                const statusCode = (err && err.statusCode) || 500;
                // write internal request log
                writeTokenInfoAccessLog(req, { statusCode: statusCode }, startAt, endAt);
                if (err && err.statusCode === 404) {
                    // revert 404 not found returned by auth server to 498 invalid token
                    return done(new HttpBearerTokenNotFound());
                }
                // otherwise continue with error
                return done(err);
            });
        }
    ));
}

module.exports.useHttpBearerAuthorization = useHttpBearerAuthorization;

