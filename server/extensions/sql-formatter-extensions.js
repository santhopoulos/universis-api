import { QueryExpression, SqlFormatter } from '@themost/query';
// extend SqlFormatter to support SqlFormatter.$cond() and $SqlFormatter.$switch() dialects
// important note: remote these extensions after @themost/query update to ^2.7.4
if (typeof SqlFormatter.prototype.$cond !== 'function') {
    Object.assign(SqlFormatter.prototype, {

        /**
         * Formats a condition expression
         * e.g. formatter.format($eq: [ { $name: 'category' }, null ], 'Unknown', new QueryField('category'))
         * CASE category IS NULL WHEN 1 THEN 'Unknown' ELSE category END
         * @param {*} ifExpr 
         * @param {*} thenExpr 
         * @param {*} elseExpr 
         * @returns 
         */
        $cond(ifExpr, thenExpr, elseExpr) {
            // validate ifExpr which should an instance of QueryExpression or a comparison expression
            let ifExpression;
            if (ifExpr instanceof QueryExpression) {
                ifExpression = this.formatWhere(ifExpr.$where);
            } else if (this.isComparison(ifExpr)) {
                ifExpression = this.formatWhere(ifExpr);
            } else {
                throw new Error('Condition parameter should be an instance of query or comparison expression');
            }
            return `(CASE ${ifExpression} WHEN 1 THEN ${this.escape(thenExpr)} ELSE ${this.escape(elseExpr)} END)`;
        },
        /**
         * Formats a switch expression
         * e.g. CASE WHEN weight>100 THEN 'Heavy' WHEN weight<20 THEN 'Light' ELSE 'Normal' END
         * @param {{branches: Array<{ case: *, then: * }>, default: *}} expr
         * @returns {string}
         */
        $switch(expr) {
            const branches = expr.branches;
            const defaultValue = expr.default;
            if (Array.isArray(branches) === false) {
                throw new Error('Switch branches must be an array');
            }
            if (branches.length === 0) {
                throw new Error('Switch branches cannot be empty');
            }
            let str = '(CASE';
            str += ' ';
            str += branches.map((branch) => {
                let caseExpression;
                if (branch.case instanceof QueryExpression) {
                    caseExpression = this.formatWhere(branch.case.$where);
                } else if (this.isComparison(branch.case)) {
                    caseExpression = this.formatWhere(branch.case);
                } else {
                    throw new Error('Case expression should be an instance of query or comparison expression');
                }
                return `WHEN ${caseExpression} THEN ${this.escape(branch.then)}`;
            }).join(' ');
            if (typeof defaultValue !== 'undefined') {
                str += ' ELSE ';
                str += this.escape(defaultValue);
            }
            str += ' END)';
            return str;
        }
    })
}