import ActionStatusType from "../models/action-status-type-model";
import { DataObjectState } from "@themost/data";
import { DataError, DataNotFoundError } from "@themost/common";

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
	// operate only on insert
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	const context = event.model.context;
	const target = event.model.convert(event.target);
	const studentId =
		typeof target.object === "object" ? target.object.id : target.object;
	// get student
	const student = await context
		.model("Student")
		.where("id")
		.equal(studentId)
		.select(
			"id",
			"studentStatus/alternateName as studentStatus",
			"studentIdentifier",
			"person/familyName as familyName",
			"person/givenName as givenName"
		)
		.getItem();

	if (student == null) {
		throw new DataNotFoundError(
			"The specified student cannot be found or is inaccessible",
			null,
			"Student"
		);
	}
	// validate current student status
	const allowedStudentStatuses = ["erased", "declared", "graduated"];
	if (!allowedStudentStatuses.includes(student.studentStatus)) {
		throw new DataError(
			"E_STATUS",
			`The update student status action can only be performed upon erased, declared or graduated students. Target student ${
				student.id
			} is ${student.studentStatus || "empty status"}.`,
			null,
			"UpdateStudentStatusAction",
			"studentStatus"
		);
	}
	// get new student status
	if (target.studentStatus == null) {
		throw new DataError(
			"E_TARGET_STATUS",
			"The target student status cannot be empty at this context",
			null,
			"UpdateStudentStatusAction",
			"studentStatus"
		);
	}
	// try alternateName first which is most likely to be passed, then id
	const newStudentStatus =
		(await context
			.model("StudentStatus")
			.where("alternateName")
			.equal(target.studentStatus.alternateName)
			.getItem()) ||
		(await context
			.model("StudentStatus")
			.where("id")
			.equal(target.studentStatus.id || target.studentStatus)
			.getItem());
	// and validate it
	if (!(newStudentStatus && newStudentStatus.alternateName === "active")) {
		throw new DataError(
			"E_TARGET_STATUS",
			`The update student status action can only be performed for active target student status. Current target status is ${
				newStudentStatus && newStudentStatus.alternateName
			}`,
			null,
			"UpdateStudentStatusAction",
			"studentStatus"
		);
	}
	// set action description
	event.target.description = `${context.__("Change student status")}: [${
		student.studentIdentifier
	}] ${student.familyName} ${student.givenName} (${
		student.studentStatus
	}) ->  (${newStudentStatus.alternateName})`;
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	// operate only on update
	if (event.state !== DataObjectState.Update) {
		return;
	}
	const context = event.model.context;
	const target = event.model.convert(event.target);
	const previous = event.previous;
	// get action status
	const actionStatus = await target.property("actionStatus").getItem();
	// if action status is other than completed
	if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
		// exit
		return;
	}
	// get previous action status
	const previousActionStatus = previous && previous.actionStatus;
	if (previousActionStatus == null) {
		throw new DataError(
			"E_PREVIOUS_STATUS",
			"The previous action status cannot be empty at this context.",
			null,
			"UpdateStudentStatusAction"
		);
	}
	// validate previous status
	if (
		previousActionStatus.alternateName !== ActionStatusType.ActiveActionStatus
	) {
		// throw error for invalid previous action status
		throw new DataError(
			"E_STATUS_STATE",
			"Invalid action state. The action cannot be completed due to its previous state.",
			null,
			"UpdateStudentStatusAction"
		);
	}
	// get student (will always exist due to previous validation on before save listener)
	const student = await target.property("object").getItem();
	// cover status cases to update student
	const Student = context.model("Student");

	// switch on student status
	if (student.studentStatus.alternateName === "erased") {
		// get removal attributes that are transfered from remove action
		const StudentRemoveAction = context.model("StudentRemoveAction");
		const removalAttributes = StudentRemoveAction.fields
			.filter((removalAttribute) => {
				return (
					!removalAttribute.primary &&
					Student.fields.find((field) => field.name === removalAttribute.name)
				);
			})
			.map((removalAttribute) => removalAttribute.name);
		// clear all removal attributes of student
		removalAttributes.forEach((removalAttribute) => {
			student[removalAttribute] = null;
		});
	} else if (student.studentStatus.alternateName === "declared") {
		// get declaration attributes that are transfered from the declare action
		const StudentDeclareAction = context.model("StudentDeclareAction");
		const declarationAttributes = StudentDeclareAction.fields
			.filter((declarationAttribute) => {
				return (
					!declarationAttribute.primary &&
					Student.fields.find(
						(field) => field.name === declarationAttribute.name
					)
				);
			})
			.map((declarationAttribute) => declarationAttribute.name);
		// but apply some extra (later calculated) attributes
		Array.prototype.push.apply(declarationAttributes, [
			"graduationRankByInscriptionYear",
			"graduationRankByGraduationYear",
			"graduationEvent",
		]);
		// clear all declaration attributes of student
		declarationAttributes.forEach((declarationAttribute) => {
			student[declarationAttribute] = null;
		});
		// try to find and cancel the latest student declare action
		const studentDeclareAction = await context
			.model("StudentDeclareAction")
			.where("object")
			.equal(student.id)
			.and("actionStatus/alternateName")
			.notEqual(ActionStatusType.CancelledActionStatus)
			.orderByDescending("id")
			.select("id")
			.getItem();
		if (studentDeclareAction) {
			// assign cancelled status
			Object.assign(studentDeclareAction, {
				actionStatus: {
					alternateName: ActionStatusType.CancelledActionStatus,
				},
				$state: 2,
			});
			// and update
			await context.model("StudentDeclareAction").save(studentDeclareAction);
		} else {
			// find and remove the declaration for this student
			const studentDeclaration = await context
				.model("StudentDeclaration")
				.where("student")
				.equal(student.id)
				.select("id")
				.getItem();
			if (studentDeclaration) {
				await context.model("StudentDeclaration").remove(studentDeclaration);
			}
		}
	} else if (student.studentStatus.alternateName === "graduated") {
		// get graduation attributes that are transfered from the graduate action
		const StudentGraduateAction = context.model("StudentGraduateAction");
		const graduationAttributes = StudentGraduateAction.fields
			.filter((graduationAttribute) => {
				return (
					!graduationAttribute.primary &&
					Student.fields.find(
						(field) => field.name === graduationAttribute.name
					)
				);
			})
			.map((graduationAttribute) => graduationAttribute.name);
		// but apply extra (later calculated) attributes
		Array.prototype.push.apply(graduationAttributes, [
			"graduationRankByInscriptionYear",
			"graduationRankByGraduationYear",
			"graduationEvent",
		]);
		// clear all graduation attributes of student
		graduationAttributes.forEach((graduationAttribute) => {
			student[graduationAttribute] = null;
		});
	} else {
		throw new DataError(
			"E_STATUS",
			`The update student status action can only be performed upon erased, declared or graduated students. Target student ${student.id} is ${student.studentStatus.alternateName}.`,
			null,
			"UpdateStudentStatusAction",
			"studentStatus"
		);
	}
	// assign active status to student
	Object.assign(student, {
		studentStatus: {
			alternateName: "active",
		},
		$state: 2,
	});
	// and finally update student
	await context.model("Student").save(student);
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	// execute async method
	return afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	// execute async method
	return beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
