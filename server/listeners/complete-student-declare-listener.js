/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";
import ActionStatusType from '../models/action-status-type-model';
import Student from '../models/student-model';
import * as _ from 'lodash';
import {DataConflictError} from "../errors";


export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert or update)
        if (event.state === 1) {
            // get action status
            const actionStatus = event.target.actionStatus;
            if (actionStatus == null) {
                throw new DataConflictError('Action status cannot be found or is inaccessible.');
            }
            if (actionStatus.alternateName === ActionStatusType.ActiveActionStatus) {
                const context = event.model.context;
                /**
                 * @type {Student|DataObject|*}
                 */
                const student = context.model('Student').convert(event.target.object);
                const studentStatus = await context.model('Student').silent().where('id').equal(event.target.object)
                    .silent()
                    .select('studentStatus')
                    .value();
                if (studentStatus == null) {
                    throw new DataError('Action object cannot be found or is inaccessible');
                }
                // if student is active or declared
                if (studentStatus.alternateName === 'active' || studentStatus.alternateName === 'declared') {
                    //check if there is an active student suspend action for this student
                    const action = await context.model(event.model.name).where('object').equal(student.id).and('actionStatus/alternateName').equal('ActiveActionStatus').silent().count();
                    if (action) {
                        throw new DataError('ERR_ACTION_EXISTS',
                            'There is already an active declare action.'
                            , null,
                            'Student', 'studentStatus');
                    }
                    if (!event.target.initiator) {
                        throw new DataError('ERR_INVALID_ACTION',
                            'Initiator action is missing.'
                            , null,
                            'GraduationRequestAction', 'studentStatus');
                    }
                } else {
                    throw new DataError('ERR_STATUS',
                        'Invalid student status. Student suspend action may execute upon an active student only.'
                        , null,
                        'Student', 'studentStatus');
                }
            }
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        let target = event.target;
        // get own properties
        const attributes = event.model.attributes.filter( attribute => {
            if (attribute.primary) {
                return false;
            }
            return attribute.model === event.model.name;
        }).map( attribute => {
            return attribute.name;
        });
        // get action status
        const actionStatus = event.target.actionStatus;
        if (actionStatus == null) {
            throw new DataError('Action status cannot be found or is inaccessible');
        }
        // on insert
       if (event.state===1) {
           if (!(actionStatus.alternateName === ActionStatusType.ActiveActionStatus || actionStatus.alternateName === ActionStatusType.CompletedActionStatus)) {
               throw new DataError('Action status should be active while inserting declare action');
           }
           // do student declaration (patch student)
           // pick student declaration attributes
           // add declaration
           /**
            * @type {GraduationRequestAction|DataObject|*}
            */
           const request = await context.model('GraduationRequestAction').where('id')
               .equal(event.target.initiator)
               .expand('graduationEvent')
               .and('actionStatus/alternateName').equal('CompletedActionStatus')
               .getTypedItem();
           if (!request) {
               throw new DataError('ERR_INVALID_ACTION',
                   'Invalid initiator action status. Student declare action may execute upon an active graduation request action only.'
                   , null,
                   'GraduationRequestAction', 'actionStatus');
           }    // validate student graduation rules
           // validate student graduation rules
           const result = await request.validateRequest();
           if (result && result.success === false) {
               throw new DataError('ERR_INVALID_DATA',
                   context.__('Graduation rules are not met or required documents are not approved')
                   , null,
                   'GraduationRequestAction');
           }
           /**
            * @type {Student|DataObject|*}
            */
           const studentId = await context.model('StudentDeclareAction').where('id').equal(event.target.id).select('object').value();
           const student = context.model('Student').convert(studentId);

           const studentDeclarationItem = await context.model('StudentDeclaration').where('student').equal(student.id).silent().getItem();
           if (studentDeclarationItem) {
               // student is declared
               // save declare action with student declaration info
               event.target.declaredYear = studentDeclarationItem.declaredYear;
               event.target.declaredDate = studentDeclarationItem.declaredDate;
               event.target.declaredPeriod = studentDeclarationItem.declaredPeriod;
               event.target.graduationGrade = studentDeclarationItem.graduationGrade;
               event.target.graduationGradeAdjusted = studentDeclarationItem.graduationGradeAdjusted;
               event.target.graduationGradeScale = studentDeclarationItem.graduationGradeScale;
               event.target.graduationGradeWrittenInWords = studentDeclarationItem.graduationGradeWrittenInWords;
               event.target.graduationDate = request.graduationEvent.startDate;
               event.target.actionStatus = {
                   "alternateName": ActionStatusType.CompletedActionStatus
               };
               await event.model.save(event.target);
           }
           else {
               // get gradeScale and calculate grade
               const graduationDegree = await student.calculateGraduationDegree();
               if (graduationDegree) {
                   const studentDeclaration = _.pick(target, attributes);
                   // set student attribute
                   studentDeclaration.student = student.id;
                   studentDeclaration.graduationGrade = graduationDegree.graduationGrade;
                   studentDeclaration.graduationGradeScale = graduationDegree.gradeScale;
                   studentDeclaration.graduationGradeAdjusted = graduationDegree.formattedGrade;
                   studentDeclaration.graduationGradeWrittenInWords = graduationDegree.graduationGradeWrittenInWords && graduationDegree.graduationGradeWrittenInWords.upperCase;
                   studentDeclaration.lastObligationDate = graduationDegree.lastObligation && graduationDegree.lastObligation.resultDate;

                   if (studentDeclarationItem == null) {
                       // save student declaration
                       const result = await context.model('StudentDeclaration').silent().save(studentDeclaration);
                       // assign id and studentStatus
                       const studentUpdate = {
                           id: student.id,
                           studentStatus: {
                               alternateName: 'declared'
                           },
                           lastObligationDate: graduationDegree.lastObligation && graduationDegree.lastObligation.resultDate
                       };
                       // update student status
                       await context.model('Student').silent().save(studentUpdate);
                       // finally save declare action
                       event.target.graduationGrade = graduationDegree.graduationGrade;
                       event.target.graduationGradeAdjusted = graduationDegree.formattedGrade;
                       event.target.graduationGradeScale = graduationDegree.gradeScale;
                       event.target.graduationGradeWrittenInWords = graduationDegree.graduationGradeWrittenInWords.upperCase;
                       event.target.graduationTitle = graduationDegree.degreeDescription;
                       event.target.graduationDate = request.graduationEvent.startDate;
                       event.target.lastObligationDate = graduationDegree.lastObligation && graduationDegree.lastObligation.resultDate;
                       await event.model.save(event.target);
                   }

               } else {
                   throw new DataError('ERR_INVALID_GRADUATION_DEGREE',
                       'Invalid graduation degree. Student declare action cannot complete.'
                       , null,
                       'StudentDeclaration', 'graduationGrade');
               }
           }
       }
       else {
           if (event.state === 2) {
               // get previous state
               const previousActionStatus = event.previous.actionStatus;
               // get target object
               target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();

               // ActiveActionStatus => CancelledActionStatus
               // change student status to active, delete studentDeclarations
               if ((previousActionStatus.alternateName === ActionStatusType.ActiveActionStatus || previousActionStatus.alternateName === ActionStatusType.CompletedActionStatus)
                   && target.actionStatus.alternateName === ActionStatusType.CancelledActionStatus) {
                    // change student status
                   const studentUpdate = {
                       id: target.object.id,
                       studentStatus: {
                           alternateName: 'active'
                       }
                   };
                   // update student status
                   await context.model('Student').silent().save(studentUpdate);
                   // get student declaration
                   const studentDeclarations = await context.model('StudentDeclaration').where('student').equal(target.object).silent().getItems();
                   // delete student declaration
                   if (studentDeclarations.length > 0) {
                       //delete studentDeclarations
                       await context.model('StudentDeclaration').silent().remove(studentDeclarations);
                   }

                   // change initiator (GraduationRequestAction) status to Active
                   const request = await context.model('GraduationRequestAction').where('id')
                       .equal(target.initiator)
                       .getTypedItem();
                   if (request) {
                       request.actionStatus = {
                           "alternateName": ActionStatusType.ActiveActionStatus
                       };
                       //set status to active
                       await context.model('GraduationRequestAction').silent().save(request);
                   }
                    // remove  StudentGraduateAction
                    const graduateAction = await context.model('StudentGraduateAction').select('id').where('initiator')
                        .equal(target.initiator)
                        .getItem();
                    if (graduateAction) {
                        // remove  StudentGraduateAction
                        await context.model('StudentGraduateAction').silent().remove(graduateAction);
                    }
               }
               // ActiveActionStatus => CancelledActionStatus
               // create requestDocumentActions
               if (previousActionStatus.alternateName === ActionStatusType.ActiveActionStatus &&
                   target.actionStatus.alternateName === ActionStatusType.CompletedActionStatus) {
                   // complete action by adding requestDocumentActions
                   // get related graduation request
                   const request = await context.model('GraduationRequestAction')
                       .where('id').equal(target.initiator).expand(
                           {
                               name: 'graduationEvent',
                               options:
                                   {
                                       $expand: 'reportTemplates($select=id)'
                                   }
                           }, 'student'
                       ).silent()
                       .getItem();

                   const reportTemplates = request.graduationEvent.reportTemplates.map(x => {
                       return x.id;
                   });

                   if (reportTemplates && reportTemplates.length>0) {
                       // find documentConfigurations for these templates
                       const documentConfigurations = await context.model('DocumentConfiguration').where('reportTemplate').in(reportTemplates).silent().getTypedItems();
                       if (documentConfigurations && documentConfigurations.length > 0) {
                           let actions = [];
                           for (let i = 0; i < documentConfigurations.length; i++) {
                               const documentConfiguration = documentConfigurations[i];
                               const exists = await context.model('RequestDocumentAction').where('initiator').equal(request.id)
                                    .and('object').equal(documentConfiguration.id).count();
                                if (!exists) {
                                    let newAction = {
                                        student: request.student,
                                        object: documentConfiguration,
                                        actionStatus: {
                                            alternateName: 'ActiveActionStatus'
                                        },
                                        initiator: request.id,
                                        owner: request.student.user
                                    };
                                    actions.push(newAction);
                                }
                           }

                           if (actions.length) {
                               await context.model('RequestDocumentAction').silent().save(actions);
                           }
                       }
                   }
                   // create studentGraduationAction
                   const studentGraduationAction = _.pick(target, attributes);
                   delete studentGraduationAction.id;
                   studentGraduationAction.initiator = target.initiator;
                   studentGraduationAction.object = target.object;
                   studentGraduationAction.graduationYear = target.declaredYear;
                   studentGraduationAction.graduationPeriod = target.declaredPeriod;
                   await context.model('StudentGraduateAction').silent().save(studentGraduationAction);
               }
           }
       }

    })().then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
