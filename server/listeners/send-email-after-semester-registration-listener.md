# SendEmailAfterRegisterAction

Send email after a new student semester register action 

### Installation

SendEmailAfterRegisterAction must be registered in application configuration services:

        "services": [
                ...
                 {
                      "serviceType": "./listeners/send-email-after-semester-registration-listener#SendEmailAfterRegisterAction"
                 }
                ...
            ]
