import util from 'util';
import {TraceUtils} from '@themost/common/utils';
import {ValidationResult} from "../errors";
import _ from 'lodash';
import {LangUtils} from '@themost/common/utils';

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    try {
        const context = event.model.context,
              /**
               * @type {{courseClass:string|*}}
               */
              target = event.target,
              courseClasses = context.model('CourseClass'),
              studentCourses = context.model('StudentCourse');

        updateClassTotalStudents(context, target, event.state, function (err) {
            if (err) {
               return callback(err);
            }
            if (event.state === 1 || event.state === 2) {
                if (target.courseClass) {
                    // get studentCourse
                    studentCourses.where('course').equal(target['course']).and('student').equal(target['student'])
                        .silent().flatten().getItem().then(studentCourse=> {
                        if (studentCourse) {
                            // update student course attributes if this is the last registration
                            if ((((target.registrationYear && target.registrationYear.id)>studentCourse.lastRegistrationYear)
                                || ((target.registrationYear && target.registrationYear.id)===studentCourse.lastRegistrationYear
                                    && target.registrationPeriod>=studentCourse.lastRegistrationPeriod) )
                                && studentCourse.isPassed===0) {

                                // check studyProgram setting
                              context.model('Student').where('id').equal(target['student'])
                                    .silent().select('studyProgram/useCurrentCourseAttributes as useCurrentCourseAttributes').value().then(useCurrentCourseAttributes => {
                                    if (LangUtils.parseInt(useCurrentCourseAttributes) !== 0) {
                                        studentCourse.semester = target.hasOwnProperty('semester') && studentCourse.semester != (target.semester && target.semester.id) ? target.semester : studentCourse.semester;
                                        studentCourse.units = target.hasOwnProperty('units') && studentCourse.units != target.units ? target.units : studentCourse.units;
                                        studentCourse.coefficient = target.hasOwnProperty('coefficient') && studentCourse.coefficient != target.coefficient ? target.coefficient : studentCourse.coefficient;
                                        studentCourse.courseType = target.hasOwnProperty('courseType') && studentCourse.courseType != (target.courseType && target.courseType.id) ? target.courseType : studentCourse.courseType;
                                        studentCourse.programGroup = target.hasOwnProperty('programGroup') && studentCourse.programGroup != (target.programGroup && target.programGroup.id) ? target.programGroup : studentCourse.programGroup;
                                        studentCourse.hours = target.hasOwnProperty('hours') && studentCourse.hours != target.hours ? target.hours : studentCourse.hours;
                                        studentCourse.ects = target.hasOwnProperty('ects') && studentCourse.ects != target.ects ? target.ects : studentCourse.ects;
                                        // update also title
                                        studentCourse.courseTitle = target.name || studentCourse.courseTitle;
                                    }
                                    // always update last registration year and period
                                    studentCourse.lastRegistrationYear = target.registrationYear;
                                    studentCourse.lastRegistrationPeriod = target.registrationPeriod;

                                    return context.unattended(function (cb) {
                                        studentCourses.save(studentCourse, function (err) {
                                            cb(err);
                                        });
                                    }, function (err) {
                                        if (err) {
                                            TraceUtils.error(err);
                                            return callback(err);
                                        }
                                        callback();
                                    });
                                });
                            }
                            callback();
                        }
                        else {

                            //add student course
                            const item = {course: target['course'],
                                student: target['student'],
                                courseTitle: target.name,
                                semester: target.semester,
                                specialty: target.specialty.specialty,
                                units: target.units,
                                coefficient: target.coefficient,
                                courseType: target.courseType,
                                programGroup: target.programGroup,
                                parentCourse: target.parentCourse,
                                hours: target.hours,
                                ects:target.ects,
                                lastRegistrationYear:target.registrationYear,
                                lastRegistrationPeriod:target.registrationPeriod
                            };
                           return  context.unattended(function (cb) {
                                studentCourses.save(item, function (err) {
                                    cb(err);
                                });
                            }, function (err) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return callback(err);
                                }
                                callback();
                            });
                        }
                    });
                }
            }
            else {
                callback();
            }
        });
    }
    catch (e) {
        callback(e)
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    try {
        const context = event.model.context, target = event.target;
        updateClassTotalStudents(context, target, 4, function (err) {
            if (err) {
                TraceUtils.error(err);
                return callback();
            }
            else {
                callback();
            }
        });
    }
    catch (e) {
        callback(e)
    }
}


function updateClassTotalStudents(context,target, state,callback)
{
    try {
         if (state === 2)
            return callback(null);
        if (!target.courseClass)
            return callback(null);

        context.model('CourseClass').where('id').equal(target.courseClass).select(['id', 'numberOfStudents', 'maxNumberOfStudents'])
            .expand({
                'name': 'students',
                'options': {
                    '$select': 'courseClass,count(id) as total',
                    '$group': 'courseClass'
                }
            }).silent().first(function (err, result) {
            if (err) {
                return callback(err);
            }
            if (_.isNil(result))
                return callback(null);
            const maxNumberOfStudents = LangUtils.parseInt(result["maxNumberOfStudents"]);
            let numberOfStudents = LangUtils.parseInt(result["numberOfStudents"]);
            if (Array.isArray(result["students"])) {
                numberOfStudents = result["students"].length ? result["students"][0].total : numberOfStudents = 0;
            }

           // if (maxNumberOfStudents === 0)
           // return callback(null);
            if (state === 1)
                numberOfStudents += 1;
            else
                numberOfStudents -= 1;

            if (numberOfStudents > maxNumberOfStudents && state !== 4 && maxNumberOfStudents!==0)
                return callback(new ValidationResult(false,'FAIL',util.format(context.__("Class limit (%s) has been reached."),maxNumberOfStudents)));

            if (numberOfStudents < 0) {
                numberOfStudents = 0;
            }
            context.unattended(function (cb) {
                context.model('CourseClass').save({id: target.courseClass, numberOfStudents: numberOfStudents}, function (err) {
                    cb(err);
                });
            }, function (err) {
                if (err) {
                    TraceUtils.error(err);
                }
                callback(err);
            });
        });
    }
    catch(e)
    {
        callback(e);
    }
}
