import {DataError} from "@themost/common";
import Student from '../models/student-model';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert)
        if (event.state === 1) {
            let context = event.model.context;
            const studentId = context.model('Student').convert(event.target.student).getId();
            const student = await context.model('Student').where('id').equal(studentId).getTypedItem();
            // student can be active or graduated if graduation action is cancelled
             if (student.studentStatus.alternateName !=='active' && student.studentStatus.alternateName!=='graduated') {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student cannot be declared.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        // on insert
        if (event.state === 1) {
            const student = await context.model('Student')
                .find(event.target.student)
                .select('id', 'studentStatus')
                .silent()
                .getItem();
            // change student status
            student.studentStatus = {
                alternateName: 'declared'
            };
            // do update
            await context.model('Student').silent().save(student);
        }
        //
        else {
            return;
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
