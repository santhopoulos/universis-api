import _ from 'lodash';
import {ValidationResult} from "../errors";
import util from "util";
import {LangUtils, TraceUtils} from "@themost/common";

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {

    const target = event.model.convert(event.target);
    const context = event.model.context;
    let classes;
    if (util.isArray(target['classes'])) {
        classes = target['classes'].filter(function (x) {
            return x.$state !== 4;
        });
        if (classes.length === 0) {
            return;
        }
        const filtered = classes.filter(function (x) {
            x.validationResult = x.validationResult || {};
            return x.validationResult.success === true;
        });
        if (filtered.length === 0) {
            return;
        }
        // get student
        const studentId = event.model.idOf(event.target["student"]);
        const student = await context.model('Student').where('id').equal(studentId).select('id', 'department', 'studyProgram', 'semester', 'specialtyId').flatten().silent().getTypedItem();

        // this rule will be executed only if checkSemesterRules is set to true, this will be changed later on.
        const department = await context.model('LocalDepartment').where('id').equal(student.department).select('checkSemesterRules').first();
        if (LangUtils.parseInt(department['checkSemesterRules']) === 0) {
            return;
        }
        const semester = target.semester || student.semester;
        // get studyProgramSemesterRules for student semester
        let studyProgramSemesterRules = await context.model('StudyProgramSemesterRule').where('semester').equal(semester)
            .or('semester').equal(null)
            .and('studyProgram').equal(student.studyProgram)
            .and('additionalType').equal('RegistrationRule')
            .expand('courseTypes')
            .silent()
            .getItems();

        let rules = studyProgramSemesterRules.filter(x=>{
            return x.semester===semester;
        });
        if (rules && rules.length===0) {
            rules = studyProgramSemesterRules.filter(x => {
                return x.semester === null;
            });
        }
        if (rules && rules.length===0) {
            return;
        }
        let message;
        let validationResults = [];
        for (let i = 0; i < rules.length; i++) {
            const rule = rules[i];
            const numberOfCourses=LangUtils.parseInt(rule["courses"]);
            const numberOfUnits=LangUtils.parseInt(rule["units"]);
            const numberOfHours=LangUtils.parseInt(rule["hours"]);
            const numberOfEcts=LangUtils.parseInt(rule["ects"]);
            let registeredCourses= 0;
            let registeredUnits= 0;
            let registeredHours=0;
            let registeredEcts=0;

            const filteredByType = filtered.filter(function (x) {
                if (_.isObject(x.courseType))
                    x.courseType = x.courseType.id;
                if (rule.courseTypes.indexOf(x.courseType)>-1 || rule.courseTypes.map(z=> z.id).indexOf(x.courseType) > -1)
                    return x;
            });
            if (rule.courseTypes && rule.courseTypes.length === 0) {
                rule.courseTypeDescription = context.__('All types');
                // check all types, do not filter
                for (let j = 0; j < filtered.length; j++) {
                    if (LangUtils.parseBoolean(filtered[j]["calculatedInRegistration"])) {
                        registeredUnits += filtered[j]["units"];
                        registeredHours += filtered[j]["hours"];
                        registeredEcts += filtered[j]["ects"];
                        registeredCourses += 1;
                    }
                }
            }
            else {
                // get course types description
                rule.courseTypeDescription = rule.courseTypes.map(courseType=>{
                    return courseType.name;
                }).join(',');
                for (let k = 0; k < filteredByType.length; k++) {
                    if (LangUtils.parseBoolean(filteredByType[k]["calculatedInRegistration"])) {
                        registeredUnits += filteredByType[k]["units"];
                        registeredHours += filteredByType[k]["hours"];
                        registeredEcts += filteredByType[k]["ects"];
                        registeredCourses += 1;
                    }
                }
            }
            if (registeredCourses > numberOfCourses && numberOfCourses>0) {
                message = util.format(context.__("The maximum allowed number of registered courses [%s] of type <%s> has been exceeded"), numberOfCourses, rule["courseTypeDescription"]);
                validationResults.push(new ValidationResult(false, 'FAIL', message));
            }
            if (registeredUnits > numberOfUnits && numberOfUnits>0) {
                message = util.format(context.__('The maximum allowed number of units of registered courses [%s] of type <%s> has been exceeded'), numberOfUnits, rule["courseTypeDescription"]);
                validationResults.push(new ValidationResult(false, 'FAIL', message));
            }
            if (registeredHours > registeredHours && registeredHours>0) {
                message = util.format(context.__('The maximum allowed number of hours of registered courses [%s] of type <%s> has been exceeded'), numberOfHours, rule["courseTypeDescription"]);
                validationResults.push(new ValidationResult(false, 'FAIL', message));
            }
            if (registeredEcts > numberOfEcts && numberOfEcts>0) {
                message = util.format(context.__('The maximum allowed number of ects of registered courses [%s] of type <%s> has been exceeded'), numberOfEcts, rule["courseTypeDescription"]);
                validationResults.push(new ValidationResult(false, 'FAIL', message));
            }
        }
        if (validationResults.length > 0) {
            const validationRes = new ValidationResult(false, 'FAIL', context.__('The registration is not valid.'));
            validationRes.validationResults = [];
            validationRes.validationResults.push.apply(validationRes.validationResults, validationResults);
            return (validationRes);
        }
        else
            return;
        }
}


export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    if (event.state !== 2 && event.state!==1) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then((validationResult) => {
        event.target.validationResult = validationResult;
        if (validationResult && !validationResult.success)
        {
            return callback(validationResult);
        }
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
