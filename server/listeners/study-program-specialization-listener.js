import StudyProgramSpecialty from '../models/study-program-specialty-model';
import {TranslateService} from "../services/translate-service";
// eslint-disable-next-line no-unused-vars
import {ExpressDataContext} from '@themost/express';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    // add printName if not supplied
    if (!event.target.printName)
    {
        event.target.printName = event.target.name;
    }
    return callback();
}
/**
 * Add core specialization while inserting a new study program
 * @param {DataEventArgs} event
 */
export async function afterSaveAsync(event) {
    // after insert
    if (event.state === 1) {
        // check if studyProgram will be copied
        let copyProgram;
        const specialties = event.target.specialties;
        if (event.target.specialties && event.target.specialties.length>0) {
            copyProgram = event.target.specialties.find(x => {
                return x.studyProgram !== event.target.id;
            });
        }
        // noinspection JSValidateTypes
        /**
         * get current context
         * @type {ExpressDataContext}
         */
        const context = event.model.context;
        // get translate service
        const translateService = context.application.getStrategy(TranslateService);
        if (!copyProgram) {
            // and try to get default name and abbreviation
            const name = translateService.translate('Specialization.CoreName');
            const abbreviation = translateService.translate('Specialization.CoreAbbreviation');
            // finally insert core specialization
            const studyProgram = context.model('StudyProgram').convert(event.target).getId();
            await context.model("StudyProgramSpecialty").silent().save({
                studyProgram: studyProgram,
                name: name,
                abbreviation: abbreviation,
                specialty: -1
            });
            // add also calculation rule for all course types
            await context.model("CalculationRule").silent().save({
                studyProgram: event.target,
                ruleType: {
                    "alternateName": "CourseType"
                },
                checkValues: -1,
                coefficient: 1
            });
        }
        else
        {
            //build rule tasks
            const tasks = [
                {
                    items: event.target.graduationRules,
                    invoke: event.target.setGraduationRules.bind(event.target),
                                    },
                {
                    items: event.target.thesisRules,
                    invoke: event.target.setThesisRules.bind(event.target)
                },
                {
                    items: event.target.internshipRules,
                    invoke: event.target.setInternshipRules.bind(event.target)
                },
                {
                    items: event.target.inscriptionRules,
                    invoke: event.target.setProgramInscriptionRule.bind(event.target)
                }
            ];
            // add all attributes from previous program
            specialties.map(x=>{
                x.studyProgram= event.target.id;
                delete x.id;
                return x;
            });
            event.target.specialties = await context.model("StudyProgramSpecialty").silent().save(specialties);
            event.target.specialties.forEach(x=>{
                const specialty =context.model("StudyProgramSpecialty").convert(x);
                if (x.rules && x.rules.length) {
                    tasks.push({
                        items: x.rules,
                        invoke: specialty.setSpecialtyRules.bind(specialty)
                    });
                }
            });


            // add program groups
            if (event.target.groups && event.target.groups.length>0)
            {
                event.target.groups.map(x=>{
                    x.program= event.target.id;
                    x.oldId = x.id;
                    delete x.id;
                    return x;
                });
                const groups = Object.assign(event.target.groups, await context.model("ProgramGroup").silent().save(event.target.groups));
                    groups.forEach(x=>{
                        if (x.parentGroup!=null) {
                            const parentGroup = groups.find(y => {
                                return y.oldId === x.parentGroup;
                            });
                            if (parentGroup) {
                                x.parentGroup = parentGroup.id
                            }
                        }
                        const programGroup =context.model("ProgramGroup").convert(x);
                        if (x.rules && x.rules.length) {
                            tasks.push({
                                items: x.rules,
                                invoke: programGroup.setRegistrationRules.bind(programGroup)
                            });
                        }
                    });
                    event.target.groups =groups;
                await context.model("ProgramGroup").silent().save(groups);
            }

            // add program courses
             if (event.target.programCourses && event.target.programCourses.length>0) {
                 event.target.programCourses.map(x => {
                     x.studyProgramCourse.studyProgram = event.target.id;
                     if (x.studyProgramCourse.programGroup != null) {
                         const programGroup = event.target.groups.find(y => {
                             return y.oldId === x.studyProgramCourse.programGroup;
                         });
                         if (programGroup) {
                             x.studyProgramCourse.programGroup = programGroup.id;
                         }
                     }
                     delete x.studyProgramCourse.id;
                     delete x.studyProgramCourse.identifier;
                     delete x.id;
                     return x;
                 });
                 const programCourses = Object.assign(event.target.programCourses, await context.model("SpecializationCourse").silent().save(event.target.programCourses));
                 programCourses.forEach(x => {
                     const programCourse = context.model("StudyProgramCourse").convert(x.studyProgramCourse);
                     if (x.rules && x.rules.length) {
                         tasks.push({
                             items: x.rules,
                             invoke: programCourse.setProgramCourseRegistrationRules.bind(programCourse)
                         });
                     }
                 });
             }
            // add rules
            for(const task of tasks) {
                if (task.items && task.items.length) {
                    const invoke = task.invoke;
                    const hasComplexRules = task.items.find(x => {
                        return x.ruleExpression != null;
                    });
                    if (hasComplexRules) {
                        // ruleExpression should be replaced from new ids
                        let ruleExpression = task.items[0].ruleExpression;
                        let newRules = [];
                        //each rule should be saved to get new id and replace it to ruleExpression
                        for (let i = 0; i < task.items.length; i++) {
                            const rule = task.items[i];
                            if (rule.refersTo === 'ProgramGroup') {
                                // change program groups ids
                                rule.programGroups.map(x => {
                                    // find new id
                                    const programGroup = event.target.groups.find(y => {
                                        return y.oldId === x.id;
                                    });
                                    if (programGroup) {
                                        x.id= programGroup.id;
                                    }
                                    return x;
                                });
                            }
                            const oldId = rule.id;
                            delete rule.id;
                            // save new rule
                            const result = await invoke([rule]);
                            const newRule = result[(result.length - 1)];
                            newRules.push(newRule);
                            ruleExpression = ruleExpression.replace(new RegExp(`\\[%${oldId}\\]`, 'g'), `[%${newRule.id}]`);
                        }
                        // update ruleExpression
                        newRules = newRules.map(x => {
                            x.ruleExpression = ruleExpression;
                            return x;
                        });
                        // save rules
                        await invoke(newRules);
                    } else {
                        // remove id and save
                        task.items.map(x => {
                                // change program groups ids
                                if (x.refersTo === 'ProgramGroup') {
                                    // change program groups ids
                                    x.programGroups.map(y => {
                                        // find new id
                                        const programGroup = event.target.groups.find(z => {
                                            return z.oldId === y.id;
                                        });
                                        if (programGroup) {
                                            y.id= programGroup.id;
                                        }
                                        return y;
                                    });
                                }
                            delete x.id;
                        });
                        await invoke(task.items);
                    }
                }
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 */
export async function beforeRemoveAsync(event) {
    const context = event.model.context;
    // remove calculation rules
    const rules = await context.model('CalculationRule').where('studyProgram').equal(event.target.id).getItems();
    if (rules && rules.length > 0) {
        await context.model('CalculationRule').remove(rules);
    }
}


