import { DataError } from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return ThesisMemberUpdateListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return ThesisMemberUpdateListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return ThesisMemberUpdateListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

class ThesisMemberUpdateListener {

    static async beforeSaveAsync(event) {
        // get context
        const context = event.model.context;
        let status;
        if (event.state != 1) {
            return;
        }
        const thesis = context.model('Thesis').convert(event.target.thesis);
        // get status
        status = await context.model('Thesis')
                .where('id').equal(thesis.getId())
                .select('status/alternateName')
                .silent()
                .value();
        // throw error of thesis status is either completed or cancelled
        if (status === 'completed' || status === 'cancelled') {
            throw new DataError('E_THESIS_STATUS', context.__('Invalid thesis status. Members of a completed or cancelled thesis cannot be modified.'), null, 'Thesis');
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        // add or update ThesisCommitteeItem

        // get context
        const context = event.model.context;
        const thesisId = context.model('Thesis').convert(event.target.thesis).getId();
        const member = context.model('Instructor').convert(event.target.member);
       // check if factor has changed
        if (event.state===2) {
            if (event.previous.factor != event.target.factor && event.target.hasOwnProperty('factor')) {
                //factor changed and should update student thesis results
                const studentTheses = await context.model('StudentThesis').where('thesis').equal(thesisId).getTypedItems();
                for (let i = 0; i < studentTheses.length; i++) {
                    const studentThesis = studentTheses[i];
                    studentThesis.grade = await studentThesis.calculateGrade();
                }
                await context.model('StudentThesis').save(studentTheses);
            }
        }
        let committeeItem = await context.model('ThesisCommitteeItem').where('thesis').equal(thesisId)
            .and('instructor').equal(member.getId()).silent().getItem();
        let updateCommitteeItem = false;
        if (committeeItem)
        {
            // thesis committee exists, check factors and indexes
            updateCommitteeItem = event.target.hasOwnProperty('factor') || event.target.hasOwnProperty('index');
            // check if factor or index is supplied
            if (event.target.hasOwnProperty('factor') ) {
                committeeItem.factor = event.target.factor;
            }
            if (event.target.hasOwnProperty('index') ) {
                committeeItem.index = event.target.index;
            }
        }
        else
        {
            updateCommitteeItem = true;
            committeeItem = {
                    thesis: event.target.thesis,
                    instructor: event.target.member,
                    factor: event.target.factor,
                    index: event.target.index
                };
        }
        if (updateCommitteeItem) {
            await context.model('ThesisCommitteeItem').silent().save(committeeItem);
        }

        // if event state is other than insert exit
        if (event.state != 1) {
            return;
        }
         // get all thesis students
        const students = await context.model('StudentThesis')
            .where('thesis').equal(thesisId)
            .silent()
            .take(-1)
            .getItems();
        // if thesis does not have any student    
        if (students.length === 0) {
            // return
            return;
        }
        // otherwise create a student thesis result for each student 
        // which refers to newly added member (instructor)
        const items = students.map( x => {
            return {
                // set student
                student: x.student,
                // set instructor
                instructor: event.target.member,
                //set thesis
                thesis: thesisId
            }
        });
        await context.model('StudentThesisResult').silent().save(items);
    }

    static async beforeRemoveAsync(event) {
        // get context
        const context = event.model.context;
        let status;
        const thesisRole = await context.model('ThesisRole').where('id').equal(event.target.id).flatten().getItem();
        // get status
        status = await context.model('Thesis')
            .where('id').equal(thesisRole.thesis)
            .select('status/alternateName')
            .silent()
            .value();
        // throw error of thesis status is either completed or cancelled
        if (status === 'completed' || status === 'cancelled') {
            throw new DataError('E_THESIS_STATUS',  context.__('Invalid thesis status. Members of a completed or cancelled thesis cannot be modified.'), null, 'Thesis');
        }
        // check if student thesis result is defined

        let studentThesisResults = await context.model('StudentThesisResult').where('thesis').equal(thesisRole.thesis)
            .and('instructor').equal(thesisRole.member)
            .silent().getItems();
        const graded = studentThesisResults.filter(x => {
            return x.grade != null;
        });
        if (graded.length > 0) {
            throw new DataError('E_THESIS_STATUS', context.__('Cannot remove member. Members with grades cannot be deleted.'), null, 'Thesis');
        }
        // delete student thesis results
        await context.model('StudentThesisResult').remove(studentThesisResults);
        // remove also ThesisCommitteeItem
        const committeeItem = await context.model('ThesisCommitteeItem').where('thesis').equal(thesisRole.thesis)
            .and('instructor').equal(thesisRole.member).flatten().getItem();
        if (committeeItem) {
            // remove
            await context.model('ThesisCommitteeItem').silent().remove(committeeItem);
        }
    }
}
