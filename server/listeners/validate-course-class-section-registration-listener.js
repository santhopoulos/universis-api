import { DataError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { ValidationResult } from '../errors';


export function beforeSave(event, callback) {
    (async () => {
        // on insert or update
        if (event.state === DataObjectState.Insert || event.state === DataObjectState.Update) {
            const target = event.target;
            const context = event.model.context;
            const previous = event.previous;
            // get section
            const section = target.section;
            const student = (target.student.id || target.student) || (previous && previous.student); // fallback to previous state
            const courseClass = (target.courseClass.id || target.courseClass) || (previous && previous.courseClass); // fallback to previous state
            if (section && section != (previous && previous.section)) {
                const typedSection = await context.model('CourseClassSection')
                    .where('courseClass').equal(courseClass)
                    .and('section').equal(section)
                    .select('id', 'maxNumberOfStudents')
                    .getTypedItem();
                if (typedSection == null) {
                    const validationResult = Object.assign(new ValidationResult(false, 'ECCSECT', 'The specified course class section cannot be found or is inaccessible.'), {'statusCode': 404});
                    target.validationResult = validationResult;
                    return validationResult;
                }
                // validate section rules
                const validationResult = await typedSection.validate(student);
                if (validationResult && validationResult.finalResult) {
                    if (validationResult.finalResult.success === false) {
                        Object.assign(validationResult.finalResult, {'statusCode': 500});
                        target.validationResult = validationResult.finalResult;
                        return validationResult.finalResult;
                    }
                }
                // validate max number of students
                if (typedSection.maxNumberOfStudents) {
                    // count students registered in this section
                    const studentsRegistered = await context.model('StudentCourseClass')
                        .where('courseClass').equal(courseClass)
                        .and('section').equal(section)
                        .silent()
                        .count();
                    if ((studentsRegistered + 1) > typedSection.maxNumberOfStudents) {
                        const validationResult = Object.assign(new ValidationResult(false, 'EFAIL', context.__('The maximum number of students that can be registered to the section has been exceeded')), {'statusCode': 500});
                        target.validationResult = validationResult;
                        return validationResult;
                    }
                }
                // exit
                return;
            }
        }
        // exit
        return;
    })().then((validationResult) => {
        if (validationResult && !validationResult.success) {
            return callback(validationResult);
        }   
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
