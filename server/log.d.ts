import {RequestHandler} from 'express';

export declare interface LogRequestConnection {
    remoteAddress: string;
}

export declare interface LogRequestUser {
    name: string;
}

export declare interface LogRequestContext {
    user: LogRequestUser;
}

export declare interface LogRequest {
    headers: any;
    method: string;
    url: string;
    connection: LogRequestConnection;
    httpVersion: string;
    httpVersionMajor: string;
    httpVersionMinor: string;
    context?: LogRequestContext
}

export declare interface LogResponse {
    headers?: any;
    statusCode: number
}

/**
 * Writes an entry in application access log file based on the given request and response
 * @param {LogRequest} req
 * @param {LogResponse} res
 * @param {*} startAt
 * @param {*} endAt
 */
export declare function writeAccessLog(req: LogRequest, res: LogResponse, startAt: any, endAt: any);

/**
 * Gets application access log handler in order to use it as a middleware in express
 * @returns {RequestHandler}
 */
export declare function accessLogHandler(): RequestHandler;
