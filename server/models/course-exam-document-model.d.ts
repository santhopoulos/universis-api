import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import CourseExam = require('./course-exam-model');
import User = require('./user-model');
import DocumentStatus = require('./document-status-model');

/**
 * @class
 */
declare class CourseExamDocument extends DataObject {

     
     /**
      * @description Ο κωδικός του εγγράφου
      */
     public id: number; 
     
     /**
      * @description Η εξέταση του μαθήματος
      */
     public courseExam: CourseExam|any; 
     
     /**
      * @description Ο χρήστης που δημιούργησε το έγγραφο.
      */
     public user?: User|any; 
     
     /**
      * @description Περιγραφή του εγγράφου
      */
     public documentSubject?: string; 
     
     /**
      * @description Το αρχικό xml έγγραφο που απέστειλε ο χρήστης.
      */
     public originalDocument?: string; 
     
     /**
      * @description Το υπογεγραμμένο xml έγγραφο που απέστειλε ο χρήστης.
      */
     public signedDocument?: string; 
     
     /**
      * @description Signature Block
      */
     public signatureBlock?: string; 
     
     /**
      * @description Το πιστοποιητικό του χρήστη.
      */
     public userCertificate?: string; 
     
     /**
      * @description Η κατάσταση του εγγράφου
      */
     public documentStatus?: DocumentStatus|any; 
     
     /**
      * @description Μία αναλυτική περιγραφή της κατάστασης του εγγράφου.
      */
     public documentStatusReason?: string; 
     
     /**
      * @description Κλειδί ελέγχου εγγράφου
      */
     public checkHashKey?: string; 
     
     /**
      * @description Ημερομηνία δημιουργίας
      */
     public dateCreated?: Date; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public dateModified: Date; 

}

export = CourseExamDocument;