import async from 'async';
import {ValidationResult, DataConflictError} from "../errors";
import {DataObject} from "@themost/data/data-object";
import {TraceUtils } from '@themost/common/utils';
import {LangUtils} from '@themost/common/utils';
import _ from 'lodash';
import {EdmMapping, EdmType} from "@themost/data/odata";
import util from "util";
import {
    DataError,
    DataNotFoundError,
    HttpConflictError,
    HttpError,
    HttpNotFoundError,
    HttpServerError
} from "@themost/common";
import {QueryEntity} from "@themost/query";
const crypto = require('crypto');
const fs = require('fs');
const readFile = util.promisify(fs.readFile);
import {xlsPostParser, toXlsx} from "../middlewares/xls";
import path from 'path';
import '@themost/promise-sequence';

@EdmMapping.entityType('CourseExam')
    /**
     * @class
     */
class CourseExam extends DataObject {

    constructor() {
        super();
    }

    save(context, callback) {
        try {
            const self = this;
            //ensure grades list
            self.grades = self.grades || [];

            //clear validation result
            delete self.validationResult;

            //get original classes
            self.validateState(function(err) {
                if (err) {
                    self.validationResult = new ValidationResult(false, err.code || 'EFAIL', self.context.__('Course exam status is not valid for editing.'), err.message);
                    return callback();
                }
                if (self.grades.length===0)
                {
                    self.validationResult = new ValidationResult(false, 'EFAIL', self.context.__('Course grades cannot be empty.'), err.message);
                    return callback();
                }
                else {

                    //get all course exam grades
                    const query = `SELECT * FROM [dbo].ufnCourseExamStudentGrades (${self.id})`;
                    context.db.execute(query, null, function (err, examGrades) {
                        if (err) {
                            return callback(err);
                        }
                        //get grade scale
                        context.model('CourseExam').select('id', 'gradeScale').expand(({
                            'name': 'gradeScale',
                            'options': {
                                '$expand': 'values'
                            }
                        })).where('id').equal(self.id).first(function (err, courseExam) {
                            if (err) {
                                return callback(err);
                            }
                            if (_.isNil(courseExam)) {
                                return callback(new Error(context.__('Course exam cannot be found.')));
                            }
                            self.grades.forEach(function (x) {
                                try {
                                    //clear validation result if any
                                    delete x.validationResult;

                                    const examGrade = examGrades.find(function (y) {
                                        return x.student === y.student && x.courseExam === y.courseExam;
                                    });
                                    //check if grade exists in exam grades
                                    if (typeof examGrade === 'undefined') {
                                        x.validationResult = new ValidationResult(false, 'EAVAIL', context.__('The student specified is unavailable for the selected course exam.'));
                                    } else {
                                        //check if grade is changed, remove if not changed
                                        const gradeScale = context.model('GradeScale').convert(courseExam.gradeScale);
                                        let formattedGrade;
                                        try {
                                            if (_.isObject(x.formattedGrade)) {
                                                x.formattedGrade = x.formattedGrade.result;
                                            }
                                            formattedGrade = gradeScale.convertFrom(x.formattedGrade);
                                            if (examGrade.examGrade == formattedGrade) {
                                                //remove from grades
                                                x.status = 0;
                                            } else {
                                                //set values
                                                x.status = {
                                                    "alternateName": "pending"
                                                }; //pending
                                                if (_.isNil(examGrade.status)) { //insert grade
                                                    x.$state = 1;
                                                    x.courseClass = examGrade.courseClass;
                                                    if (_.isNil(x.formattedGrade))
                                                        x.status = 0;
                                                } else { //update grade
                                                    x.$state = 2;
                                                    x.id = examGrade.id;
                                                    x.previousGrade=examGrade.examGrade;
                                                    x.previousFormattedGrade=examGrade.formattedGrade;
                                                    if (_.isNil(x.formattedGrade) && (_.isNil(x.deleted) === true || x.deleted === false))
                                                        x.status = 0;
                                                    if (x.formattedGrade==='-') {
                                                        x.formattedGrade = null;
                                                    }
                                                }
                                            }
                                        } catch (e) {
                                            x.validationResult = new ValidationResult(false, 'ERANGE', context.__(e.message));
                                        }
                                    }
                                } catch (e) {
                                    return callback(e);
                                }
                            });

                            context.db.executeInTransaction(function (cb) {
                                async.eachSeries(self.grades, function (item, cb1) {
                                    try {

                                        if (item.validationResult || item.status === 0) {
                                            if (item.status === 0) {
                                                item.validationResult = (new ValidationResult(true, 'NOACTION', context.__('Student grade is not supplied or is the same.')));
                                                return cb1();
                                            } else
                                                return cb1();
                                        }
                                        context.db.execute('SELECT [dbo].[ufnConvertGrade] (?,?) as grade', [item.formattedGrade, courseExam.gradeScale.id], function (err, result) {
                                            if (err) {
                                                return cb1(err);
                                            }
                                            item.examGrade = result[0].grade;
                                            item.grade1 = item.examGrade;
                                            item.courseExam = self.id;

                                            if (_.isNil(item.examGrade) && !_.isNil(item.formattedGrade)) {
                                                item.validationResult = new ValidationResult(false, 'INVDATA', context.__("The specified grade is not valid."));
                                                return cb1();
                                            }
                                            item.isPassed = item.examGrade >= courseExam.gradeScale.scaleBase;

                                            //if validationResult is undefined and grade status=pending, continue with saving
                                            const studentGrade = context.model('StudentGrade').convert(item);
                                            studentGrade.validate(function (err) {
                                                if (err) {
                                                    item.validationResult = new ValidationResult(false, 'INVDATA', err.message);
                                                    return cb1();
                                                }
                                                studentGrade.save(context, function (err) {
                                                    if (err) {
                                                        //if err is instance of ValidationResult
                                                        if (err instanceof ValidationResult) {
                                                            //push validation result to array
                                                            item.validationResult = err;
                                                        } else {
                                                            //convert error to ValidationResult
                                                            item.validationResult = new ValidationResult(false, 'FAIL', 'An internal error occurred while saving grade.', err.message);
                                                        }
                                                    } else {
                                                        item.validationResult = (new ValidationResult(true, 'SUCC', context.__('Student grade successfully saved.')));
                                                    }
                                                    //exit without error
                                                    cb1();
                                                });
                                            });
                                        });
                                    } catch (er) {
                                        cb1(er);
                                    }
                                }, function (err) {
                                    cb(err);
                                });

                            }, function (err) {
                                if (err) {
                                    if (err instanceof ValidationResult) {
                                        self.validationResult = err;
                                    } else {
                                        TraceUtils.error(err);
                                        self.validationResult = new ValidationResult(false, err.code || 'EFAIL', self.context.__('Course exam cannot be saved.'), err.message);
                                    }
                                    return callback();
                                } else {
                                    const failed = self.grades.find(function (x) {
                                        if (typeof x.validationResult === 'undefined')
                                            return false;
                                        return !x.validationResult.success;
                                    });
                                    if (typeof self.validationResult === 'undefined') {
                                        if (typeof failed !== 'undefined') {
                                            self.validationResult = new ValidationResult(true, 'PSUCC', self.context.__('Course exam successfully saved but with errors.'));
                                        } else {
                                            self.validationResult = new ValidationResult(true, 'SUCC', self.context.__('Course exam successfully saved.'));
                                        }
                                        //add to event log
                                        return context.model('CourseExam').where('id').equal(self.id).select("id", "year", "name", "examPeriod/name as examPeriod", "course/displayCode as displayCode", "course/name as courseName").flatten().silent().first(function (err, exam) {
                                            if (err) {
                                                return callback(err);
                                            }
                                            if (exam) {
                                                context.unattended(function (cb) {
                                                    const thisCourseExam = context.model('CourseExam').convert(exam);
                                                    return thisCourseExam.createDocument(function (err, docId) {
                                                        if (err) {
                                                            return cb(err);
                                                        }
                                                        // set document id
                                                        self.docId = docId;
                                                        return context.model('CourseExam').save(self).then(() => {
                                                            return cb();
                                                        }).catch(err => {
                                                            return cb(err);
                                                        });
                                                    });
                                                }, function (err) {
                                                    if (err instanceof ValidationResult) {
                                                        self.validationResult = err;
                                                    }
                                                    else {
                                                        if (err) {
                                                            TraceUtils.error(err);
                                                            self.validationResult = new ValidationResult(false, err.code || 'EFAIL', self.context.__('Course exam cannot be saved.'), err.message);
                                                        }
                                                    }
                                                    return callback();
                                                });
                                            } else {
                                                return callback();
                                            }
                                        });
                                    }
                                }
                                return callback();
                            });
                        });
                    });
                }
            });
        }
        catch(er) {
            callback(er);
        }
    }

    validateState(callback) {
        try {
            const self = this;
            /**
             *
             * @type {DataContext}
             */
            const context = self.context;
            context.model('CourseExamInstructor').where('courseExam').equal(self.id).select( "courseExam/id as id", "courseExam/status/alternateName as status", "courseExam/completedByUser as completedByUser").flatten().silent().first(function (err, exam) {
                if (err) {
                    return callback(err);
                }
                if (!_.isNil(exam)) {
                    context.model('CourseExam').resolveMethod('me', [], function (err, user) {
                        if (err) {
                            return callback(err);
                        }
                        const previousExamStatus = exam["status"];
                        let status;
                        if (!self.status) {
                            if (previousExamStatus === "completed" || previousExamStatus==='open') {
                                //check if completedByUser is null or  same with logged in user
                                if (LangUtils.parseInt(exam["completedByUser"]) !== user)
                                    return callback(new Error(context.__('Course exam is in edit state by a different user.')));
                                else
                                    return callback(null);
                            }
                            else
                                return callback(new Error(context.__('Course exam cannot be saved due to its state.')));
                        }
                        else {
                            status = self.context.model('CourseExamStatus').convert(self.status).alternateName;
                            // allow only completed status to be set
                            if (status !== "completed") {
                                return callback(new Error(context.__('Course exam status is not valid.')));
                            }
                            if (previousExamStatus === "completed" || previousExamStatus === 'open' || previousExamStatus==='inprogress') {
                                //set user
                                if (previousExamStatus==='inprogress' && user.id!==self.completedByUser.id)
                                {
                                    // another user is processing this course exam
                                    return callback(new Error(context.__('Course exam is in edit state by a different user.')));
                                }
                                self.completedByUser = user;
                                self.dateCompleted = new Date();
                                return callback(null);
                            }
                            else {
                                // previous exam status does not allow status changing
                                return callback(new Error(context.__('Course exam status cannot be set due to its state.')));
                            }
                        }
                    });
                }
                else
                    return callback(new Error(context.__('Course exam cannot be found.')));
            });
        }
        catch(e) {
            callback(e);
        }
    }

    courseOf(callback) {
        this.attrOf('course', callback);
    }

    createDocument(callback) {
        try {
            const self = this, context = self.context;
            context.model('CourseExam').resolveMethod('me', [], function (err, user) {
                if (err) {
                    return callback(err);
                }
                const query = `sp_createCourseExamGradesDocument ${self.id},${user}`;
                context.db.execute(query, null, function (err, result) {
                    if (err) {
                        TraceUtils.error(err);
                        return callback(err);
                    }
                    else {
                        const docId = result[0].docId;
                        context.unattended(function (cb) {
                            context.model('CourseExamDocument')
                                .where('id').equal(docId)
                                .select( "id", "courseExam", "originalDocument", "courseExam/course/department/organization as institute")
                                .expand('courseExam')
                                .flatten().silent().first(function (err, exam) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return cb(err);
                                }
                                if (_.isNil(exam)) {
                                    TraceUtils.error(`CourseExamDocument with id ${docId} cannot be found`);
                                    return cb(new Error(`CourseExamDocument with id ${docId} cannot be found`));
                                }
                                const doc = exam.originalDocument;
                                // Calculate signed document (if institute applies digital signing)
                                try {
                                    const instituteId = exam.institute;
                                    context.model('Institute').where('id').equal(instituteId)
                                        .expand('instituteConfiguration')
                                        .silent()
                                        .getItem().then(institute => {
                                        /*if (institute.instituteConfiguration && institute.instituteConfiguration.useDigitalSignature) {
                                            calculateSignedDocument(context.getApplication().getConfiguration().getSourceAt("settings/digitalSigning/certificatePath"), Buffer.from(doc)).then((signedDoc) => {
                                                exam.signedDocument = signedDoc;
                                                // Calculate hashkey of signed document
                                                const hash_algorithm = crypto.createHash('sha1');
                                                // create buffer from base64 signed document
                                                hash_algorithm.update(Buffer.from(exam.signedDocument, 'base64'));
                                                exam.checkHashKey = hash_algorithm.digest('base64');
                                                context.model('CourseExamDocument').save(exam, function (err) {
                                                    cb(err);
                                                });
                                            }).catch((signError) => {
                                                TraceUtils.error(signError);
                                                cb(signError);
                                            });
                                        }*/
                                        // institute not applying digitial signing
                                        let shasum = crypto.createHash('sha1');
                                        //add hash key
                                        shasum.update(doc, 'utf8');
                                        exam.checkHashKey = shasum.digest('base64');
                                        context.model('CourseExamDocument').save(exam, function (err) {
                                            cb(err);
                                        });
                                    });
                                } catch(e){
                                    self.validationResult = new ValidationResult(false, e.message || 'EFAIL', self.context.__('Course exam signing cannot be executed'), e.message);
                                    return cb(new Error(`Error signing document 2 ${docId}`));
                                }
                            });
                        }, function (err) {
                            if (err) {
                                TraceUtils.error(err);
                            }
                            return callback(err,docId);
                        });
                    }
                });
            });
        }
        catch (e) {
            return callback(e);
        }
    }

    /**
     * @returns {Promise<*>}
     */
    async preCheck () {
        let self = this;
        try {
            /**
             * @type {DataContext}
             */
            let context = self.context;
            //ensure grades list and filter undefined grades
            self.grades = (self.grades || []).filter(x=>{
                return x.formattedGrade !== undefined;
            });
            // allow empty grades, do not check grades.length

            //check if file refers to specific courseExam only if grades exist
            if (self.grades.length>0) {
                const invalidExam = self.grades.find(x => {
                    return x.courseExam !== self.id;
                });
                if (invalidExam) {
                    self.validationResult = new ValidationResult(false, 'EFAIL', self.context.__('At least one grade refers to other exam.'));
                    return;
                }
            }
            // get all course exam grades
            let examGrades = await new Promise((resolve, reject) => {
                context.db.execute(`SELECT * FROM [dbo].ufnCourseExamStudentGrades (?)`, [self.id], (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(result);
                });
            });
            /**
             *
             * @type {GradeScale|any}
             */
            let gradeScale = await this.context.model('GradeScale').where('id').equal(this.gradeScale).expand('values').silent().getTypedItem();
            if (typeof gradeScale === 'undefined') {
                throw new DataNotFoundError('Course grade scale cannot be found or is inaccessible.');
            }
            // enumerate grades

            let forEachGrade = async grade => {
                try {
                    let previousGrade = examGrades.find( examGrade => grade.student === examGrade.student );
                    if (typeof previousGrade === 'undefined') {
                        // student not found
                        grade.validationResult = new ValidationResult(false, 'EAVAIL', 'The student specified is unavailable for the selected course exam.');
                        return;
                    }
                    // set current grade
                    if (_.isObject(grade.formattedGrade)) {
                        grade.formattedGrade = grade.formattedGrade.result;
                    }
                    grade.grade1 = grade.examGrade = gradeScale.convertFrom(grade.formattedGrade);
                    if (grade.examGrade == previousGrade.examGrade) {

                        grade.validationResult = new ValidationResult(true, 'UNMOD', context.__('Grade has not been modified.'));
                        return grade;
                    }
                    else {
                        if (typeof previousGrade.examGrade === 'undefined' || previousGrade.examGrade === null) {
                            // insert grade
                            grade.validationResult = new ValidationResult(true, 'INS', context.__('A new student grade will be inserted.'));
                        }
                        else {
                            // update grade
                            grade.validationResult = new ValidationResult(true, 'UPD', context.__('Student grade already exists and it will be replaced.'));
                        }
                    }
                    // only if exam grade is not null (e.g. has not been deleted)
                    if (grade.examGrade != null) {
                        // validate deny grading of class status
                        const denyGrading = await context.model('StudentClassStatus')
                            .where('id').equal(previousGrade.classRegistrationStatus)
                            .select('denyGrading')
                            .silent()
                            .value();
                        if (denyGrading) {
                            grade.validationResult = new ValidationResult(false, 'E_DENY_GRADING', context.__('Grading is denied due to the student course class status'));
                            return;
                        }
                    }
                    const studentGrade = context.model('StudentGrade').convert(grade);
                    return await new Promise((resolve) => {
                        studentGrade.isPassed = studentGrade.examGrade >= gradeScale.scaleBase;
                        studentGrade.validate(function (err) {
                            if (err) {
                                if (err instanceof ValidationResult) {
                                    grade.validationResult = err;
                                }
                                else {
                                    TraceUtils.error(err);
                                    grade.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                                }
                            }
                            return resolve();
                        });
                    });
                }
                catch (err) {
                    TraceUtils.error(err);
                    if (err instanceof RangeError) {
                        grade.validationResult = new ValidationResult(false, 'ERANGE', context.__(err.message));
                    }
                    else {
                        grade.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                    }
                }
            };

            await Promise.sequence(self.grades.map((grade) => {
                return () => forEachGrade(grade);
            }));
            if (self.grades.find( x=> x.validationResult.success === false)) {
                self.validationResult = new ValidationResult(true, 'PSUCC', self.context.__('Course exam contains warnings-errors.'));
            }
            else {
                self.validationResult = new ValidationResult(true, 'SUCC', self.context.__('Course exam validation check was completed successfully.'));
            }
        }
        catch (err) {
            TraceUtils.error(err);
            self.validationResult = new ValidationResult(false, err.code || 'EFAIL', self.context.__('Course exam cannot be validated due to internal error.'), err.message);
        }
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
    @EdmMapping.func("students",EdmType.CollectionOf("CourseExamStudentGrade"))
    getStudents() {
        const self = this;
        // check if user has permissions to courseExam
        return self.context.model('CourseExam').where('id').equal(self.id).first().then(function (courseExam) {
            if (_.isNil(courseExam)) {
                return Promise.reject(new HttpNotFoundError());
            }
            const params = Object.assign({}, self.context.params);
            delete params.id;
            const entityExpr = util.format('ufnCourseExamStudentGrades(%s)', self.id);
            const entity = new QueryEntity(entityExpr).as('CourseExamStudentGrades');
            const q = self.context.model('CourseExamStudentGrade').asQueryable();
            //replace entity reference
            q.query.from(entity);
            //return queryable
            return Promise.resolve(q.silent());
        }).catch(function (err) {
            TraceUtils.error(err);
            if (err instanceof HttpError) {
                return Promise.reject(err);
            }
            return Promise.reject(new HttpServerError());
        });
    }

    @EdmMapping.func("exportStudents", EdmType.EdmStream)
    async exportStudents() {
        // prepare students - also validates permissions on object
        const prepareStudents = await this.getStudents();
        // get export view
        const view = prepareStudents.model.getDataView('export');
        // get students
        const students = await prepareStudents.select('export').getAllItems();
        // create and return xlsx buffer
        return await toXlsx(view.toLocaleArray(students)); // TODO: Maybe handle csv later.
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func("actions",EdmType.CollectionOf("ExamDocumentUploadAction"))
    getDocumentActions() {
        return this.context.model('ExamDocumentUploadAction').where('object').equal(this.id).prepare();
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func("participants",EdmType.CollectionOf("CourseExamParticipateAction"))
    getExamParticipants() {
        return this.context.model('CourseExamParticipateAction').where('courseExam').equal(this.id).prepare();
    }

    /**
     * Returns an array of objects which represent statistics about course exam grades
     * @returns Promise<*>
     */
    async getStatistics() {
        // return statistics (group by grades)
        const q = this.context.model('StudentGrade').asQueryable();
        return await q.select('examGrade as examGrade', 'isPassed', 'count(id) as total')
            .where('courseExam').equal(this.getId())
            .and('status/alternateName').notEqual('pending')
            .groupBy('examGrade', 'isPassed')
            .silent()
            .getItems();
    }

    async import(file) {
        const req = {
            file: Object.assign(file, {
                mimetype: file.contentType,
                destination: path.dirname(file.path),
                filename: path.basename(file.path),
                originalfilename: file.contentFileName,
                originalname: file.contentFileName
            })
        };
        const res = {};
        await new Promise((resolve, reject) => {
            xlsPostParser({
                name: "file"
            })(req, res, (err) => {
                if (err) {
                    return reject(err);
                }
                // get body
                return resolve();
            });
        });
        // get json
        const view = this.context.model('CourseExamStudentGrade').getDataView('export');
        const courseExam = await this.context.model('CourseExam').where('id').equal(this.getId()).getTypedItem();
        courseExam.grades = view.fromLocaleArray(req.body);
        // run pre check
        await courseExam.preCheck();
        if (courseExam.validationResult.success === false) {
            throw new DataConflictError(courseExam.validationResult.message);
        }
        // create course exam document action
        let documentAction = {
            object: courseExam,
            file: req.file
        };
        // save exam document action and append request file as action attachment
        await this.context.model('ExamDocumentUploadAction').silent().save(documentAction);
        // assign course exam data
        Object.assign(documentAction, {
            object: courseExam
        });
        delete documentAction.file;
        // return data from document action
        return documentAction;
    }

    /**
     * @param {*} file
     * @returns {Promise<any>}
     */
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('import', 'ExamDocumentUploadAction')
    importFile(file) {
        return this.import(file);
    }

}

/**
 * Used to calculate the signed document that has been encrypted in chunks of 112 bytes each
 * @param {String} public_key_path
 * @param {Buffer} document
 * @return {String} base64 encoded signed document
 */
async function calculateSignedDocument(public_key_path, document) {
    try {
        const public_key = await readFile(public_key_path);
        const documentLength = document.length;
        let read_length = 0;
        let signedDocument = Buffer.alloc(0);
        for (let index = 0; index < documentLength; index += read_length) {
            if ((index + 112) <= documentLength) {
                read_length = 112
            } else {
                read_length = documentLength - index;
            }
            let data_chunk = document.slice(index, index + read_length);
            let tmp_buffer = crypto.publicEncrypt({
                key: public_key,
                padding: crypto.constants.RSA_PKCS1_PADDING
            }, data_chunk);
            signedDocument = Buffer.concat([signedDocument, tmp_buffer], signedDocument.length + tmp_buffer.length);
        }
        return signedDocument.toString('base64');
    } catch (e) {
        throw (e);
    }
}

CourseExam.calculateSignedDocument = calculateSignedDocument;

module.exports = CourseExam;
