import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class EventLog extends DataObject {

     
     /**
      * @description Id
      */
     public id: string; 
     
     /**
      * @description Event Type
      */
     public eventType: number; 
     
     /**
      * @description User
      */
     public username: string; 
     
     /**
      * @description Title
      */
     public title: string; 
     
     /**
      * @description Date
      */
     public eventDate: Date; 
     
     /**
      * @description Event Source
      */
     public eventSource: string; 
     
     /**
      * @description Application
      */
     public eventApplication: string; 
     
     /**
      * @description dateModified
      */
     public dateModified: Date; 

}

export = EventLog;