// eslint-disable-next-line no-unused-vars
import {EdmMapping, HasOneAssociation} from '@themost/data';
import {DataError, HttpNotFoundError, HttpConflictError} from "@themost/common";
import ActionStatusType from "./action-status-type-model";
let UploadAction = require('./upload-action-model');
import {PrivateContentService} from "../services/content-service";
import {csvPostParser} from "../middlewares/csv";
import {xlsPostParser, XlsxContentType} from "../middlewares/xls";
import {DataConflictError, GenericDataConflictError} from "../errors";
import _ from 'lodash';
import util from 'util';
import { TraceUtils } from '@themost/common';

/**
 * @this DataAdapter
 * @param func
 * @returns {Promise<unknown>}
 */
function executeInTransactionAsync(func) {
    return new Promise((resolve, reject) => {
        return this.executeInTransaction((callback) => {
            return func.call(this).then(res => {
                return callback(null, res);
            }).catch(err => {
                return callback(err);
            });
        }, (err, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(res);
        });
    });
}

/**
 * @class

 * @property {ExamDocumentUploadAction|any} additionalResult
 * @property {Array<StudentGradeActionTrace|any>} grades
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('ExamDocumentUploadAction')
class ExamDocumentUploadAction extends UploadAction {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * @returns {ExamDocumentUploadAction}
     */
    @EdmMapping.action('approve', 'ExamDocumentUploadAction')
    async approve() {
        return await this.changeActionStatus(ActionStatusType.CompletedActionStatus);
    }

    /**
     * @returns {ExamDocumentUploadAction}
     */
    @EdmMapping.action('reject', 'ExamDocumentUploadAction')
    async reject() {
        return await this.changeActionStatus(ActionStatusType.FailedActionStatus);
    }

    /**
     * @returns {Action}
     */
     @EdmMapping.func('ActionResult', 'Action')
     async getActionResult() {
         /**
          * @type {HasOneAssociation}
          */
         const actionResult = this.property('actionResult');
         return await actionResult.silent().expand({
             name: 'owner',
             options: {
                 $select: 'UserSnapshot'
             }
         }, {
            name: 'createdBy',
            options: {
                $select: 'UserSnapshot'
            }
        }).getItem();
     }

    /**
     *
     * @returns {ExamDocumentUploadAction}
     */
    @EdmMapping.action('complete', 'ExamDocumentUploadAction')
    async complete() {
        const documentAction = await this.context.model('ExamDocumentUploadAction').where('id').equal(this.getId()).getItem();
        if (typeof documentAction === 'undefined') {
            throw (new HttpNotFoundError('Document action cannot be found or is inaccessible'));
        }
        // validate action status
        if (documentAction.actionStatus.alternateName !== 'PotentialActionStatus') {
            throw (new HttpConflictError('Document action has invalid status.'));
        }
        documentAction.actionStatus = {
            "alternateName": "ActiveActionStatus"
        };
        const courseExam = this.context.model('CourseExam').convert(documentAction.object);
        await this.context.model('ExamDocumentUploadAction').silent().save(documentAction);
        // set course exam object
        documentAction.object = courseExam;
        //get course exam result document
        if (documentAction.additionalResult && typeof documentAction.additionalResult!=='object') {
            documentAction.additionalResult = await this.context.model('CourseExamDocument').where('identifier').equal(documentAction.additionalResult).silent().getItem();
        }
        // return document action
        return documentAction;
    }


    async changeActionStatus(status) {

        // check status
        // get action status
        const actionStatus = await this.property('actionStatus').getItem();
        // if action status is other that active
        if (actionStatus.alternateName !== ActionStatusType.ActiveActionStatus) {
            throw new DataError('ERR_INVALID_DATA', this.context.__('Cannot approve or reject exam action due to its state'));
        }

        /**
         * get ExamDocumentUploadAction
         * @type {ExamDocumentUploadAction}
         */
        let uploadAction = await this.context.model('ExamDocumentUploadAction')
            .asQueryable()
            .where('id').equal(this.getId())
            .expand(
                {
                    "name": "object",
                    "options":
                        {
                            "$expand": "course($expand=department)"
                        }
                },
                "grades", "additionalResult", "attachments"
            )
            .silent().getItem();
        //check if there is a previous active action
        /**
         * get CourseExam
         * @type {CourseExam}
         */
        const courseExam = await this.context.model('CourseExam')
            .where('id').equal(uploadAction.object.id)
            .expand('status', 'course', 'year', 'examPeriod')
            .getTypedItem();
        const documentActions = await courseExam.getDocumentActions()
            .where('additionalResult').notEqual(null)
            .expand('grades')
            .getItems();
        /* --- Start action validations --- */
        if (documentActions.length > 1) {
            const findPreviousActive = documentActions.find(x => {
                return x.actionStatus.alternateName === 'ActiveActionStatus' && x.id < this.getId();
            });
            if (findPreviousActive) {
                throw new DataError('ERR_INVALID_DATA', this.context.__('You must first approve or reject previous active action'));
            }
        }
        if (status === ActionStatusType.CompletedActionStatus) {
            // get institute id
            const instituteId = uploadAction
                && uploadAction.object
                && uploadAction.object.course
                && uploadAction.object.course.department
                && uploadAction.object.course.department.organization;
            // get institute configuration
            const instituteConfiguration = await this.context.model('Institute')
                .where('id').equal(instituteId)
                .select('instituteConfiguration/useDigitalSignature as useDigitalSignature')
                .silent()
                .getItem();
            // if the institute requires digital signature
            if (instituteConfiguration.useDigitalSignature) {
                // try to get SignerWorker service
                const signerWorker = this.context.getApplication().getService(function SignerWorker() {});
                if (signerWorker == null) {
                    // if service is not present, throw error
                    throw new DataConflictError(this.context.__('CannotValidateSignature'));
                }
                /**
                 * get private content service
                 * @type {PrivateContentService}
                 */
                let privateContentService = this.context.getApplication().getService(PrivateContentService);
                if (privateContentService == null) {
                    throw new DataConflictError(this.context.__('CannotValidateSignature'));
                }
                // get physical path
                const resolvePhysicalPath = util.promisify(privateContentService.resolvePhysicalPath).bind(privateContentService);
                const physicalPath = await resolvePhysicalPath(this.context, uploadAction.attachments && uploadAction.attachments[0]);

                if (physicalPath == null) {
                    throw new DataConflictError(this.context.__('CannotValidateSignature'));
                }

                // verify digital signature
                const signatureVerificationResults = await signerWorker.verify(this.context, physicalPath, {
                    contentType: uploadAction.attachments && uploadAction.attachments[0].contentType || 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    filename: uploadAction.attachments && uploadAction.attachments[0].name || 'file.xlsx'
                });

                if (signatureVerificationResults == null) {
                    throw new DataConflictError(this.context.__('CannotValidateSignature'));
                }

                if (signatureVerificationResults.length === 0) {
                    throw new DataConflictError(this.context.__('InvalidSignature'));
                }

                let isValidSignature = true;
                for (const result of signatureVerificationResults) {
                    if (!(result && result.valid)) {
                        isValidSignature = false;
                        break;
                    }
                }
                if (!isValidSignature) {
                    throw new DataConflictError(this.context.__('InvalidSignature'));
                }
            }
             // validate attachment against trace before proceeding
            const passedCrosscheck = await this.validateAttachmentAgainstTrace(this.context, uploadAction, documentActions);
            if (!passedCrosscheck) {
                throw new GenericDataConflictError('ERR_INV_GRADES', this.context.__('InconsistancyOnApproval'), 'ExamDocumentUploadAction');
            }
        }
        /* --- End action validations --- */
        /* --- Start async action approval/rejection --- */
        let changeStatusAction;
        // create and save Accept/Reject action according to status
        if (status === ActionStatusType.CompletedActionStatus) {
            const acceptAction = await this.context.model('AcceptAction').silent().save({
                actionStatus: {alternateName: ActionStatusType.ActiveActionStatus}, // active, as in "in-progress"
                description: this.context.__('Accept exam grades'),
                additionalType: "AcceptAction", // useful for recognizing the action result
                initiator: uploadAction.id // set initiator
            });
            changeStatusAction = acceptAction;
        } else {
            const rejectAction = await this.context.model('RejectAction').silent().save({
                actionStatus: {alternateName: ActionStatusType.ActiveActionStatus}, // active, as in "in-progress"
                description: this.context.__('Reject exam grades'),
                additionalType: "RejectAction", // useful for recognizing the action result
                initiator: uploadAction.id // set initiator 
            });
            changeStatusAction = rejectAction;
        }
        // set in progress action result
        uploadAction.actionResult = changeStatusAction;
        await this.context.model('ExamDocumentUploadAction').silent().save(uploadAction);
        // reject/approve async
        this.changeActionStatusAsync(this.context, uploadAction, courseExam, status, changeStatusAction);
        return changeStatusAction;
        /* --- End async action approval/rejection --- */
    }

    changeActionStatusAsync(appContext, uploadAction, courseExam, status, changeStatusAction) {
        const app = appContext.getApplication();
        const context = app.createContext();
        context.user = appContext.user;
        context.locale = appContext.locale;
        let sseUser;
        (async function () {
            sseUser = context.user || await context.model('User')
                .where('id').equal(changeStatusAction.owner.id || changeStatusAction.owner)
                .silent()
                .getItem();
            await executeInTransactionAsync.bind(context.db)(async () => {
                // pre-fetch active action status status id
                const activeStatus = await context.model('ActionStatusType')
                    .where('alternateName').equal(ActionStatusType.ActiveActionStatus)
                    .select('id')
                    .silent()
                    .value();
                if (uploadAction.grades.length > 0) {
                    // save student grades
                    const grades = [];
                    const examGrades = await context.model('StudentGrade').where('courseExam').equal(courseExam.id).getAllItems();
    
                    for (let i = 0; i < uploadAction.grades.length; i++) {
                        // check if accept/reject action is active (may have been cancelled by the user)
                        const isActive = await context.model('Action')
                            .where('id').equal(changeStatusAction.id)
                            .and('actionStatus').equal(activeStatus)
                            .select('id')
                            .silent()
                            .count();
                        if (!isActive) {
                            throw new DataError(context.__('The action has been cancelled by the user'));
                        }
                        const newGrade = uploadAction.grades[i];
                        // check if grade exists
                        /**
                         * @type {StudentGrade}
                         */
                        let examGrade = examGrades.find(x => {
                            return x.student === newGrade.student;
                        });
                        if (examGrade) {
                            if (examGrade.status.alternateName !== 'normal' && status === ActionStatusType.FailedActionStatus) {   // remove grades only if status is pending
                                // find last approved grade if any
                                const lastGradeTrace = await context.model('StudentGradeActionTrace').where('status/alternateName').equal('normal')
                                    .and('student').equal(examGrade.student).and('courseExam').equal(courseExam.id).orderByDescending('dateCreated').getItem();
                                if (lastGradeTrace) {
                                    // update grade from trace
                                    examGrade.examGrade = lastGradeTrace.examGrade;
                                    examGrade.grade1 = lastGradeTrace.examGrade;
                                    if (lastGradeTrace.examGrade === null) {
                                        // remove grade
                                        await context.model('StudentGrade').remove(examGrade);
                                    } else {
                                        // update grade
                                        examGrade.status = {alternateName:'normal'};
                                        await context.model('StudentGrade').save(examGrade);
                                    }
                                }
                                else {
                                    // remove grade
                                    await context.model('StudentGrade').remove(examGrade);
                                }
                            }
                            if (status === ActionStatusType.CompletedActionStatus) {
                                examGrade.examGrade = newGrade.examGrade;
                                examGrade.grade1 = newGrade.examGrade;
                                examGrade.status = {"alternateName": "normal"};
                                if (examGrade.grade1===null)
                                {
                                    // mark grade for delete
                                    examGrade.$state = 4;
                                }
                                grades.push(examGrade);
                            }
                        } else {
                            // add grade
                            if (status === ActionStatusType.CompletedActionStatus) {
                                examGrade = {
                                    student: newGrade.student,
                                    courseExam: newGrade.courseExam,
                                    examGrade: newGrade.examGrade,
                                    grade1: newGrade.examGrade,
                                    status: {"alternateName": "normal"}
                                };
                                grades.push(examGrade);
                            }
                        }
                    }
                    const failedGrades = [];
                    if (grades.length > 0) {
                        //save grades
                        for (let grade of grades) {
                            // check if accept/reject action is active (may have been cancelled by the user)
                            const isActive = await context.model('Action')
                                .where('id').equal(changeStatusAction.id)
                                .and('actionStatus').equal(activeStatus)
                                .select('id')
                                .silent()
                                .count();
                            if (!isActive) {
                                throw new DataError(context.__('The action has been cancelled by the user'));
                            }
                            try {
                                await context.model('StudentGrade').save(grade);
                            } catch (err) {
                                failedGrades.push({"grade": grade, "error": err});
                                // delete invalid grade
                                await context.model('StudentGrade').remove(grade);
                            }
                        }
                    }
                    // change also gradeStatus of trace
                    uploadAction.grades = uploadAction.grades.map(x => {
                        if (status === ActionStatusType.CompletedActionStatus) {
                            const failed = failedGrades.find(y => {
                                return y.grade.student === x.student;
                            });
                            x.status = failed ? {"alternateName": "failed"} : {"alternateName": "normal"};
                            x.description = failed ? failed.error.message : null;
                        } else {
                            x.status = {"alternateName": "failed"};
                            x.description = context.__('Exam action is rejected');
                        }
                        return x;
                    });
                    await context.model('StudentGradeActionTrace').silent().save(uploadAction.grades);
                    uploadAction.failed = failedGrades;
                }
                let documentStatus = await context.model('DocumentStatus').where('alternateName').equal(status === ActionStatusType.FailedActionStatus ? 'cancelled' : 'closed').getItem();
                uploadAction.actionResult = changeStatusAction; // will be completed
                //change action status
                uploadAction.actionStatus = {alternateName: status};
                if(typeof uploadAction?.additionalResult === 'object'){
                    uploadAction.additionalResult = {...uploadAction?.additionalResult, documentStatus, documentStatusReason: documentStatus.name, $state: 2}
                } else {
                    uploadAction.additionalResult = { identifier: uploadAction?.additionalResult, documentStatus, documentStatusReason: documentStatus.name, $state: 2}
                }
                uploadAction.additionalResult = (await context.model('CourseExamDocument').save(uploadAction.additionalResult)).identifier;
                await context.model('ExamDocumentUploadAction').silent().save(uploadAction);
                // update total graded students
                const gradedStudents = await context.model('StudentGrade').where('courseExam').equal(courseExam.id)
                    .and('status/alternateName').equal('normal').silent().count();
                courseExam.numberOfGradedStudents = gradedStudents || 0;
                await context.model('CourseExam').save(courseExam);
            });
        })().then(() => {
            context.finalize(() => {
                // complete accept/reject action
                changeStatusAction.actionStatus = {alternateName: ActionStatusType.CompletedActionStatus};
                changeStatusAction.endTime = new Date();
                if (status === ActionStatusType.CompletedActionStatus) {
                    return context.model('AcceptAction').silent().save(changeStatusAction).then(action => {
                        // emit sse
                        this.sendSse(sseUser, context, uploadAction, null);
                        // if this is an approval action
                        // and some grades failed
                        if (uploadAction.failed && uploadAction.failed.length) {
                            // find out if the owner is an instructor
                            return context.model('Instructor')
                                .where('user').equal(uploadAction.owner && (uploadAction.owner.id || uploadAction.owner))
                                .select('id')
                                .silent()
                                .getItem().then(instructor => {
                                    if (instructor == null) {
                                        // exit
                                        return;
                                    }
                                    // use an async function
                                    (async function () {
                                        // set table headers
                                        const studentsTableHeaders = `<thead><tr><th scope='col'>${context.__('Student')}</th><th scope='col'>${context.__('Student Identifier')}</th><th scope='col'>${context.__('Student Status')}</th><th scope='col'>${context.__('Grade')}</th></tr></thead>`;
                                        let studentsTableRows = "<tbody>";
                                        for (const grade of uploadAction.failed) {
                                            // do not interfere by expanding grades/student attribute of action
                                            // get students
                                            const student = await context.model('Student').where('id').equal(grade.grade.student)
                                                .select('id', 'person/familyName as familyName', 'person/givenName as givenName', 'studentIdentifier', 'studentStatus/name as status')
                                                .silent()
                                                .getItem();
                                            if (student == null) {
                                                return;
                                            }
                                            // set table rows
                                            const innerRow = `<td>${student.familyName} ${student.givenName}</td><td>${student.studentIdentifier}</td><td>${student.status}</td><td>${grade.grade.formattedGrade}</td>`;
                                            studentsTableRows += `<tr> ${innerRow} </tr>`;
                                        }
                                        studentsTableRows += "</tbody>";
                                        // return students table
                                        return `<table class='table'> ${studentsTableHeaders} ${studentsTableRows} </table>`
                                    })().then((studentsTable) => {
                                        let dateCreated;
                                        // if for some reason date created is null, fallback to now
                                        if (uploadAction.dateCreated == null) {
                                            dateCreated = new Date();
                                        } else {
                                            dateCreated = new Date(uploadAction.dateCreated);
                                        }
                                        const successfulyApproved = uploadAction.grades.length - uploadAction.failed.length;
                                        // build message body
                                        const messageBody = `<h6 class='pt-1'>${courseExam.course.name} (${courseExam.examPeriod.name} - ${courseExam.year.alternateName}) - ${dateCreated.toLocaleDateString(context.locale || 'el')}, ${dateCreated.toLocaleTimeString(context.locale || 'el')}</h6>  ${context.__('GradeTableBody')} <div class='pt-3'>${studentsTable}</div> <div class='pt-2'> ${context.__('Total grades')}: ${uploadAction.grades.length} , ${context.__('Successfully approved')}: ${successfulyApproved}, ${context.__('Failed')}: ${uploadAction.failed.length}</div>`;
                                        // create a message to inform instructor
                                        const message = {
                                            instructor: instructor.id,
                                            message: context.__("Exam grade table submission"),
                                            subject: context.__("Failed grades after approval from the registrar"),
                                            body: messageBody,
                                            sender: context.user,
                                            category: 'InstructorMessage',
                                            about: `${uploadAction.id}`,
                                            recipient: uploadAction.owner.id || uploadAction.owner,
                                            dateCreated: new Date()
                                        }
                                        // and send
                                        return context.model('InstructorMessage').silent().save(message).then(() => {
                                            // 
                                        }).catch(err => {
                                            TraceUtils.error(err);
                                        });
                                    }).catch(err => {
                                        TraceUtils.error(err);
                                    });
                                }).catch(err => {
                                    TraceUtils.error(err);
                                });
                        }
                    }).catch(err => {
                        TraceUtils.error(err);
                    });
                } else {
                    return context.model('RejectAction').silent().save(changeStatusAction).then(action => {
                        // emit sse
                        this.sendSse(sseUser, context,  uploadAction, null);
                    }).catch(err => {
                        TraceUtils.error(err);
                    });
                }
            });
        }).catch(err => {
            context.finalize(() => {
                // set accept/reject as failed
                changeStatusAction.actionStatus = {alternateName: ActionStatusType.FailedActionStatus};
                changeStatusAction.endTime = new Date();
                // set error description
                TraceUtils.error(err);
                changeStatusAction.description = err.code || err.message;
                if (status === ActionStatusType.CompletedActionStatus) {
                    return context.model('AcceptAction').silent().save(changeStatusAction).then(action => {
                        // emit sse
                        this.sendSse(sseUser, context, uploadAction, err);
                    }).catch(err => {
                        TraceUtils.error(`(ExamDocumentUploadAction) An error occurred while approving action with id ${uploadAction.id}`);
                        TraceUtils.error(err);
                    });
                } else {
                    return context.model('RejectAction').silent().save(changeStatusAction).then(action => {
                        // emit sse
                        this.sendSse(sseUser, context, uploadAction, err);
                    }).catch(err => {
                        TraceUtils.error(`(ExamDocumentUploadAction) An error occurred while rejecting action with id ${uploadAction.id}`);
                        TraceUtils.error(err);
                    });
                }
            });
        });
    }

    sendSse(user, context, uploadAction, error) {
        // try to find ServerSentEventService
        const sse = context.getApplication().getService(function ServerSentEventService() {});
        // only if it exists - it is not required
        if (sse != null) {
            if (user != null && uploadAction != null) {
                // create an event
                const event = {
                    entitySet: "ExamDocumentUploadActions",
                    entityType: "ExamDocumentUploadAction",
                    target: {
                        id: uploadAction.id,
                        object: uploadAction.object.id || uploadAction.object, // course exam
                        actionResult: {
                            additionalType: uploadAction.actionResult.additionalType
                        }
                    },
                    status: {
                        success: true,
                    },
                    dateCreated: new Date()
                }
                if (error) {
                    Object.assign(event, {
                        status: {
                            success: false
                        },
                        error: {
                            message: error.code || error.message
                        }
                    })
                }
                // and send it to the user that is performing the approval (registrar scope)
                sse.emit(user.name, user.authenticationScope || "registrar", event);
            }
        }
    }


    async parseFile (context, attachment)
    {
    /**
     * get private content service
     * @type {PrivateContentService}
     */
    let service = context.getApplication().getService(PrivateContentService);
    /**
     * get attachment readable stream
     * @type {ReadableStream}
     */
    let stream = await new Promise((resolve, reject) => {
        context.unattended((cb)=> {
            service.createReadStream(context, attachment, (err, result) => {
                return cb(err, result);
            });
        }, (err, result) => {
            if (err) {
                return reject(err);
            }
            return resolve(result);
        });
    });
    let body;
    let buffer = await this.readStream(stream);
    // parse text/csv
    if (attachment.contentType === 'text/csv' || attachment.contentType === 'application/csv' ||
        ((attachment.contentType === 'application/vnd.ms-excel' || attachment.contentType === 'application/octet-stream')
            && (/\.(csv)$/i).test(attachment.name))) {
        body = await new Promise((resolve, reject) => {
            // create a request like object
            let req = {
                file: {
                    buffer: buffer,
                    mimetype: "text/csv"
                }
            };
            // call csvPostHandler to parse csv
            csvPostParser({
                name: "file"
            })(req, null, (err) => {
                if (err) {
                    return reject(err);
                }
                // get body
                return resolve(req.body);
            });
        });
    }
    // parse application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    else if (attachment.contentType === XlsxContentType || ((attachment.contentType === 'application/octet-stream')
        && (((/\.(xlsx)$/i).test(attachment.name)) || (/\.(xls)$/i).test(attachment.name)))) {
        body = await new Promise((resolve, reject) => {
            // create a request like object
            let req = {
                file: {
                    buffer: buffer,
                    mimetype: XlsxContentType
                }
            };
            // call csvPostHandler to parse csv
            xlsPostParser({
                name: "file"
            })(req, null, (err) => {
                if (err) {
                   return reject(err);
                }
                // get body
                return resolve(req.body);
            });
        });
    }
    return body;
    }

    readStream(stream) {
        return new Promise((resolve, reject) => {
            let buffers = [];
            stream.on('data', (d) => {
                buffers.push(d);
            });
            stream.on('end', () => {
                return resolve(Buffer.concat(buffers));
            });
            stream.on('error', (err) => {
                return reject(err);
            });
        });
    }

    async validateAttachmentAgainstTrace(context, uploadAction, documentActions) {
        // prepare student grade traces
        let studentGradeTraces = [];
        let groupedTraces = [];
        let totalTracesUpToNow = [];
        if (documentActions) {
            // filter actions until exactly the current one
            documentActions = documentActions.filter(action => {
                return (action.actionStatus.alternateName === 'ActiveActionStatus'
                    || action.actionStatus.alternateName === 'CompletedActionStatus'
                    || action.actionStatus.alternateName === 'FailedActionStatus')
                    && (action.id <= uploadAction.id)
            });
        }
        if (documentActions && documentActions.length > 1) {
            // if there are previous actions
            documentActions.forEach(action => {
                action.grades.forEach(grade => {
                    // gather traces up to current action
                    totalTracesUpToNow.push(grade);
                });
            })
            // group traces by student
            groupedTraces = _.groupBy(totalTracesUpToNow, 'student');
            for (const [student, traces] of Object.entries(groupedTraces)) {
                // ensure that traces are always sorted by id desc
                traces.sort((traceOne, traceNext) => (traceOne.id < traceNext.id) ? 1 : -1);
                // first element of trace must always depict the latest attachment grades situation
                studentGradeTraces.push({
                    student: traces[0] && traces[0].student,
                    courseExam: traces[0] && traces[0].courseExam,
                    examGrade: traces[0] && traces[0].examGrade,
                    action: traces[0] && traces[0].action
                });
            }
        } else {
            // if there are no previous actions, get current student grade trace
            studentGradeTraces = uploadAction && uploadAction.grades;
        }
        // get attachment
        const attachment = uploadAction 
            && uploadAction.attachments
            && uploadAction.attachments[0];
        // parse attachment and get grades
        let attachmentGrades = await this.parseFile(context, attachment);
        const view = context.model('CourseExamStudentGrade').getDataView('export');
        attachmentGrades = view.fromLocaleArray(attachmentGrades);
        // setup grade scale for conversion
        const gradeScale = await context.model('GradeScale')
            .where('id').equal(
                uploadAction.object
                && uploadAction.object.course
                && uploadAction.object.course.gradeScale)
            .silent()
            .getTypedItem();
        if (studentGradeTraces && studentGradeTraces.length > 0) {
            // validate data
            for (const trace of studentGradeTraces) {
                // find student
                const findStudentAttachmentGrade = attachmentGrades.find(attachmentGrade => attachmentGrade.student === trace.student);
                if (findStudentAttachmentGrade == null) {
                    if (trace.action === uploadAction.id) {
                        return false;
                    } else {
                        continue;
                    }
                }
                if (findStudentAttachmentGrade && findStudentAttachmentGrade.formattedGrade == null) {
                    const findGradeInTrace = uploadAction.grades.find(studentGrade => studentGrade.student === findStudentAttachmentGrade.student);
                    if (findGradeInTrace == null) {
                        continue;
                    }
                }
                // convert formatted grade to examGrade
                if (_.isObject(findStudentAttachmentGrade.formattedGrade)) {
                    findStudentAttachmentGrade.formattedGrade = findStudentAttachmentGrade.formattedGrade.result;
                }
                let attachmentExamGrade = gradeScale.convertFrom(findStudentAttachmentGrade && findStudentAttachmentGrade.formattedGrade);
                // map undefined to null to satisfy '==='
                if (typeof attachmentExamGrade === 'undefined') {
                    attachmentExamGrade = null;
                }
                // validate courseExam, examGrade properties
                if (!(findStudentAttachmentGrade && findStudentAttachmentGrade.courseExam === trace.courseExam && attachmentExamGrade === trace.examGrade)) {
                    // if grade does not belong to current action
                    if (trace.action !== uploadAction.id) {
                        // try to find the grade in any previous grade trace, for that student and courseExam
                        const previousGradeTraces = totalTracesUpToNow.find(item => {
                            return item.student === findStudentAttachmentGrade.student && item.action < trace.action && item.examGrade === attachmentExamGrade
                                && item.courseExam === findStudentAttachmentGrade.courseExam;
                        });
                        if (!previousGradeTraces) {
                            // if the grade is not found, throw error
                            TraceUtils.error(`(ExamDocumentUploadAction) An error occurred while approving action with id ${uploadAction.id} and student ${trace.student}. Trace grade is: ${trace.examGrade}, Attachment grade is: ${attachmentExamGrade}.`);
                            return false;
                        }
                    } else {
                        TraceUtils.error(`(ExamDocumentUploadAction) An error occurred while approving action with id ${uploadAction.id} and student ${trace.student}. Trace grade is: ${trace.examGrade}, Attachment grade is: ${attachmentExamGrade}.`);
                        return false;
                    }
                }
                // mark attachment grade as validated
                Object.assign(findStudentAttachmentGrade, {validated: true});
            }
        }
        let validateUnknownTraces = context.getConfiguration().getSourceAt('settings/universis/validateUnknownTraces'); // boolean
        if (validateUnknownTraces == null) {
            // true by default
            validateUnknownTraces = true;
        }
        // try to find any not validated attachment grade
        // catches grades that are present in the attachment but not registered in trace
        const notValidatedAttachmentGrade = attachmentGrades.find(attachmentGrade => (attachmentGrade.formattedGrade && !attachmentGrade.validated));
        if (notValidatedAttachmentGrade) {
            // log grade
            TraceUtils.error(`(ExamDocumentUploadAction) An error occurred while approving action with id ${uploadAction.id}. Found attachment grade ${notValidatedAttachmentGrade.formattedGrade} for student ${notValidatedAttachmentGrade.student} that is not present in any traces.`)
            if (validateUnknownTraces) {
                // throw error
                return false;
            }
        }
        return true;
    }
}

module.exports = ExamDocumentUploadAction;
