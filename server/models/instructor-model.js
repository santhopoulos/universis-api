import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataPermissionEventListener, PermissionMask} from "@themost/data";
import {promisify} from "es6-promisify";
import {HttpForbiddenError} from '@themost/common/errors';
import {DataObject} from "@themost/data/data-object";
import _ from 'lodash';
import * as SendSmsAfterInstructorUserCreation from "../services/send-sms-after-instructor-user-creation";

@EdmMapping.entityType('Instructor')
/**
 * @class
 */
class Instructor extends DataObject {

    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * @swagger
     * /api/Instructors/Me:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Returns an instructor which is associated with the current logged in user
     *    security:
     *      - OAuth2:
     *          - teachers
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/Instructor'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    @EdmMapping.func("Me","Instructor")
    static getMe(context) {
        const model = context.model('Instructor');
        return new Promise(((resolve, reject) => {
            model.filter("id eq instructor()", (err, q) => {
                if (err) {
                    return reject(err);
                }
                return resolve(q);
            });
        }));
    }

    @EdmMapping.func("classes",EdmType.CollectionOf("CourseClass"))
    getInstructorClasses() {
        return this.context.model('CourseClass').where('instructors/instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("currentClasses",EdmType.CollectionOf("CourseClass"))
    getCurrentClasses() {
        return this.context.model('CourseClass').where('instructors/instructor').equal(this.getId())
            .and('year').equal('$it/course/department/currentYear')
            .and('period').equal('$it/course/department/currentPeriod')
            .prepare();
    }

    @EdmMapping.func("exams",EdmType.CollectionOf("CourseExam"))
    getInstructorExams() {
        return this.context.model('CourseExam').where('instructors/instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("currentExams",EdmType.CollectionOf("CourseExam"))
    getCurrentExams() {
        return this.context.model('CourseExam').where('instructors/instructor').equal(this.getId())
            .and('year').equal('$it/course/department/currentYear')
            .prepare();
    }

    @EdmMapping.func("reports",EdmType.CollectionOf("ReportTemplate"))
    getAvailableReports() {
        return this.context.model('ReportTemplate').where('reportCategory/appliesTo').in(['CourseExam','ExamDocumentUploadAction']).prepare();
    }


    @EdmMapping.func("uploadActions",EdmType.CollectionOf("ExamDocumentUploadAction"))
    getExamDocumentUploadActions() {
        return this.context.model('ExamDocumentUploadAction').where('owner/name').equal(this.context.user.name)
            .and('additionalResult').notEqual(null)
            .prepare();
    }

    @EdmMapping.func("theses",EdmType.CollectionOf("Thesis"))
    getInstructorTheses() {
        return this.context.model('Thesis').where('instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("activeTheses",EdmType.CollectionOf("Thesis"))
    getInstructorActiveTheses() {
        return this.context.model('Thesis').where('instructor').equal(this.getId()).and('status/alternateName').equal('active').prepare();
    }

    @EdmMapping.func("classStudents",EdmType.CollectionOf("StudentCourseClass"))
    getInstructorClassStudents() {
        return this.context.model('StudentCourseClass').where('courseClass/instructors/instructor').equal(this.getId()).select('InstructorClassStudents').prepare();
    }

    @EdmMapping.func("thesisStudents",EdmType.CollectionOf("StudentThesis"))
    getInstructorThesisStudents() {
        return this.context.model('StudentThesis').where('thesis/instructor').equal(this.getId()).select('InstructorThesisStudents').prepare();
    }

    @EdmMapping.func("Messages",EdmType.CollectionOf("InstructorMessage"))
    getMessages() {
        return this.context.model('InstructorMessage').where('instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("OutgoingMessages",EdmType.CollectionOf("StudentMessage"))
    getOutgoingMessages() {
        return this.context.model('StudentMessage').where('owner/name').equal(this.context.user.name).select('InstructorStudentMessages').prepare();
    }
    @EdmMapping.func("consultedStudents",EdmType.CollectionOf("StudentCounselor"))
    getConsultedStudents() {
        return this.context.model('StudentCounselor').where('instructor').select('InstructorStudentCounselor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("teachingEvents", EdmType.CollectionOf('TeachingEvent'))
    getTeachingEvents(){
        return this.context.model('TeachingEvent')
            .where('courseClass/period').equal('$it/courseClass/course/department/currentPeriod')
            .and('courseClass/year').equal('$it/courseClass/course/department/currentYear')
            .and('courseClass/instructors/instructor').equal(this.getId())
            .prepare();
    }

    @EdmMapping.action('CreateUser', 'CreateInstructorUserAction')
    async createUser() {
        const context = this.context;
        // save a CreateInstructorUserAction for this instructor
        // (no need to perform any validations at this moment)
        const result = await context.model('CreateInstructorUserAction')
            .silent()
            .save({
                instructor: this.getId(),
                activationCode: _.sampleSize('ABCDEFGHIKLMNOPQRTUVWXYZ0123456789', 20).join('')
            });
        // protect activation code
        delete result.activationCode;
        // and return the action
        return result;
    }

    @EdmMapping.action('sendActivationMessage', 'Instructor')
    async sendActivationMessage()
    {
        // get validator listener
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate)
            .bind(validator);
        // validate Instructor/SendActivationMessage execute permission
        const event = {
            model: this.getModel(),
            privilege: 'Instructor/SendActivationMessage',
            mask: PermissionMask.Execute,
            target: this,
            throwError: false
        }
        await validateAsync(event);
        if (event.result === false) {
            throw new HttpForbiddenError();
        } 
        /**
         * @type {ApplicationBase}
         */
        const app = this.context.getApplication();
        let service = app.getService(function SendSmsAfterInstructorUserCreation() {});
        if (service != null) {
            return await service.send(this.context, this.getId());
        }
        // otherwise check if application has sms service enabled
        const parentService = app.getService(function SmsService() {});
        if (parentService == null) {
            throw new Error('The operation cannot be completed due to invalid application configuration. A required service is missing');
        }
        return await SendSmsAfterInstructorUserCreation.SendSmsAfterInstructorUserCreation.prototype.send(this.context, this.getId());
    }
}

module.exports = Instructor;
