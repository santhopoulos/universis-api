import {EdmMapping,EdmType} from '@themost/data/odata';
import Department = require('./department-model');

/**
 * @class
 */
declare class LocalDepartment extends Department {

     
     /**
      * @description Μοναδικός κωδικός τμήματος
      */
     public id: string; 
     
     /**
      * @description Έλεγχος προϋποθέσεων μαθήματος κατά τη δήλωση μαθήματος (ΝΑΙ/ΟΧΙ).
      */
     public checkCourseRules?: boolean; 
     
     /**
      * @description Έλεγχος προϋποθέσεων εξαμήνου προγράμματος σπουδών κατά τη δήλωση μαθημάτων (ΝΑΙ/ΟΧΙ)
      */
     public checkSemesterRules?: boolean; 
     
     /**
      * @description Έλεγχος ειδικών προϋποθέσεων που αφορούν ειδικούς κανόνες που ισχύουν για το τμήμα
      */
     public checkCustomRules?: boolean; 
     
     /**
      * @description Έλεγχος προϋποθέσεων προγράμματος διδασκαλίας (επικαλύψεις ωρών κλπ)
      */
     public checkScheduleRules?: boolean; 
     
     /**
      * @description Είναι περίοδος δηλώσεων (ΝΑΙ/ΟΧΙ).
      */
     public isRegistrationPeriod?: boolean; 
     
     /**
      * @description Πληροφορίες περιόδου δηλώσεων (σημειώσεις και οδηγίες που εμφανίζονται στους φοιτητές κατά τη δήλωση μαθημάτων).
      */
     public registrationPeriodNotes?: string; 
     
     /**
      * @description Ημερομηνία έναρξης δηλώσεων μαθημάτων
      */
     public registrationPeriodStart?: Date; 
     
     /**
      * @description Ημερομηνία λήξης δηλώσεων μαθημάτων
      */
     public registrationPeriodEnd?: Date; 
     
     /**
      * @description Επιτρέπεται η τροποποίηση δηλώσεων μαθημάτων (ΝΑΙ/ΟΧΙ).
      */
     public allowRegistrationChange?: boolean; 
     
     /**
      * @description Εμφάνιση βαθμολογιών μαθημάτων στο site των φοιτητών (ΝΑΙ/ΟΧΙ)
      */
     public webShowGrades?: boolean; 
     
     /**
      * @description Ακαδημαϊκό έτος εμφάνισης βαθμολογιών μαθημάτων στο site των φοιτητών
      */
     public webResultsYear?: number; 
     
     /**
      * @description Μορφή προβολής βαθμολογιών μαθημάτων (ονοματεπώνυμο, μόνο αριθμοί μητρώου)
      */
     public webResultsViewType?: number; 
     
     /**
      * @description Επιτρέπεται η επιλογή κατεύθυνσης φοιτητή εφόσον το πρόγραμμα σπουδών το επιτρέπει
      */
     public allowSpecialtySelection?: boolean; 

}

export = LocalDepartment;