import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import _ from "lodash";
import {ValidationResult} from "../errors";
import {TraceUtils} from "@themost/common/utils";
import {promisify} from "es6-promisify";
import {DataObjectState, DataPermissionEventListener} from "@themost/data";
import '@themost/promise-sequence';
const Rule = require('./rule-model');

/**
 * @class
 
 * @property {number} id
 * @property {StudyProgram|any} program
 * @property {ProgramGroupType|any} groupType
 * @property {ProgramGroup} parentGroup
 * @property {string} displayCode
 * @property {string} name
 * @property {string} description
 * @property {boolean} isCalculated
 * @property {GradeScale|any} gradeScale
 * @property {number} units
 * @property {string} comments
 * @property {number} coefficient
 * @property {Semester|any} semester
 * @property {CourseType|any} courseType
 * @property {number} minCourses
 * @property {number} maxCourses
 * @property {string} notes
 * @property {string} dgDepartments
 * @property {string} dgAreas
 * @property {boolean} isActive
 * @property {string} nameEn
 * @property {Date} dateModified
 */
@EdmMapping.entityType('ProgramGroup')
class ProgramGroup extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async validate(student) {
        const context = this.context;
        const st = await context.model('Student').where("id").equal(student).silent().getTypedItem();
        const data = {
            student: st
        };
        // validate program group rules
        const rules = await context.model('Rule').where('additionalType').equal('GroupRegistrationRule')
            .and('targetType').equal('ProgramGroup')
            .and('target').equal(this.getId().toString()).getItems();

        if (rules && rules.length===0) {
            return;
        }
        let validationResults = [];
        // add async function for validating graduation rules
        let forEachRule = async groupRule => {
            try {
                return await new Promise((resolve) => {
                    const ruleModel = context.model(groupRule.refersTo + 'Rule');
                    if (_.isNil(ruleModel)) {
                        validationResults = validationResults || [];
                        validationResults.push(new ValidationResult(false, 'FAIL', context.__('Student validation rule type cannot be found.')));
                    }
                    const rule = ruleModel.convert(groupRule);
                    rule.validate(data, function (err, result) {
                        if (err) {
                            validationResults = validationResults || [];
                            validationResults.push(new ValidationResult(false, 'FAIL', context.__('Student validation rule type cannot be found.')));
                        }
                        /**
                         * @type {ValidationResult[]}
                         */
                        validationResults = validationResults || [];
                        validationResults.push(result);
                        groupRule.validationResult = result;
                        return resolve();
                    });
                });
            } catch (err) {
                TraceUtils.error(err);
            }
        };
        // call all promises
        await Promise.sequence(rules.map((rule) => {
            return () => forEachRule(rule);
        }));

        const fnValidateExpression = function (x) {
            try {
                let expr = x['ruleExpression'];
                if (_.isEmpty(expr)) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return v.success.toString();
                    }
                    return 'false';
                });
                expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                return eval(expr);
            } catch (e) {
                return e;
            }
        };

        const fnTitleExpression = function (x) {
            try {
                let expr = x['ruleExpression'];
                if (_.isEmpty(expr)) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return '(' + v.message.toString() + ')';
                    }
                    return 'Unknown Rule';
                });
                expr = expr.replace(/\bAND\b/ig, context.__(' AND ')).replace(/\bOR\b/ig, context.__(' OR ')).replace(/\bNOT\b/ig, context.__(' NOT '));
                return expr;
            } catch (e) {
                return e;
            }
        };

        let res, title;

        //apply default expression
        const expr = rules.find(function (x) {
            return !_.isEmpty(x['ruleExpression']);
        });
        let finalResult;
        if (expr) {
            res = fnValidateExpression(expr);
            title = fnTitleExpression(expr);
            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'GroupRegistrationRule';
            expr.ruleExpression= expr.ruleExpression.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
            expr.ruleExpression = expr.ruleExpression.replace(/\[%(\d+)]/g, function () {
                if (arguments.length === 0) return;
                return parseInt(arguments[1]);
            });

        } else {
            //get expression (for this rule type)
            const ruleExp1 = rules.map(function (x) {
                return '[%' + x.id + ']';
            }).join(' AND ');
            res = fnValidateExpression({ruleExpression: ruleExp1});
            title = fnTitleExpression({ruleExpression: ruleExp1});
            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'GroupRegistrationRule';
            finalResult.expression = null;
        }

        return {finalResult, "programGroupRules": rules};

    }

    /**
     * Returns a collection of rules which are going to be validated during study program group registration
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('RegistrationRules', EdmType.CollectionOf('Rule'))
    async getRegistrationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'ProgramGroup', 'GroupRegistrationRule');
    }

    /**
     * Updates study program group registration rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('RegistrationRules', EdmType.CollectionOf('Rule'))
    async setRegistrationRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'ProgramGroup';
            // set additional type
            converted.additionalType = 'GroupRegistrationRule';
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getRegistrationRules();
    }
}
module.exports = ProgramGroup;
