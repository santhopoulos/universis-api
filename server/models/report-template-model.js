import {promisify} from 'util';
import {EdmMapping, EdmType, DataObject} from '@themost/data';
import {DataError, Args} from '@themost/common';
// eslint-disable-next-line no-unused-vars
import {ExpressDataContext} from '@themost/express';
import {DataPermissionEventListener, PermissionMask} from '@themost/data'
import {PrintService} from "@universis/reports";
import {PassThrough} from 'stream';
import { DocumentNumberService, DocumentNumberVerifierService } from '@universis/docnumbers';
/**
 * @class
 
 * @property {number} id
 * @property {string} additionalType
 * @property {string} appliesTo
 * @property {boolean} signReport
 * @property {string} signReason
 * @property {string} signLocation
 * @property {ReportCategory|any} ReportCategory
 * @property {string} inLanguage
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @property {string} identifier
 * @property {ExpressDataContext|*} context
 */
@EdmMapping.entityType('ReportTemplate')
class ReportTemplate extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * @param {ExpressDataContext} context
     */
    @EdmMapping.func('availableServerTemplates', EdmType.CollectionOf('Object'))
    static async getAvailableServerTemplates(context) {
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate)
            .bind(validator);
        // try to validate if user has permission to create a report template
        // only report designers can get server report templates
        await validateAsync({
            model: context.model('ReportTemplate'), // current model
            state: PermissionMask.Create, // set state to create
            target: null // any target
        });
        /**
         * get print service
         * @type {PrintService}
         */
        const printService = context.getApplication().getService(PrintService);
        if (printService == null) {
            throw new DataError('E_SERVICE_MISSING', 'Application print services cannot be found or are inaccessible');
        }
        return printService.readReports(printService.settings.rootDirectory);
    }

    /**
     * Reads report metadata like parameters, resources etc
     * @param params
     * @returns {Promise<*>}
     */
    @EdmMapping.func('read', 'Object')
    async read() {
        // noinspection JSUnresolvedFunction
        /**
         * @type {PrintService}
         */
        const printService = this.context.getApplication().getService(PrintService);
        // fetch url
        const location = await this.context.model(ReportTemplate)
            .where('id').equal(this.getId())
            .select('url')
            .value();
        if (location == null) {
            return null;
        }
        return await printService.readReport(location);
    }

    /**
     * Prints the current report template by the specified parameters
     * and returns a binary content which represents a pdf document
     * @param params
     * @returns {Promise<*>}
     */
    @EdmMapping.param('params','Object', true, true)
    @EdmMapping.action('print', EdmType.EdmStream)
    print(params) {
        return new Promise((resolve, reject) => {
            let res;
            this.context.db.executeInTransaction((cb)=> {
                return this._printAsync(params).then((result) => {
                    // hold result
                    res = result;
                    return cb();
                }).catch( (err) => {
                    return cb(err);
                })
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                // return result
                return resolve(res);
            })
        });
    }

    /**
     * @private
     * @param {*} params 
     */
    async _printAsync(params) {
        // create an one-time print token
        // noinspection JSUnresolvedFunction
        /**
         * @type {PrintService}
         */
        const printService = this.context.getApplication().getService(PrintService);
        if (printService == null) {
            throw new DataError('E_SERVICE_MISSING', 'Application print services cannot be found or are inaccessible');
        }
        // prepare report parameters
        const queryParams = Object.assign({
            REPORT_USE_DOCUMENT_NUMBER: false,
            REPORT_DOCUMENT_SERIES: null
        }, params, {
            REPORT_CLIENT_TOKEN: this.context.user.authenticationToken
        });
        // fetch report category
        const reportCategory = await this.getModel().where('id').equal(this.getId())
            .select('reportCategory').expand('reportCategory').value();
        if (reportCategory == null) {
            throw new DataError('E_REPORT_CATEGORY', 'Report template category cannot be empty at this context');
        }
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate)
            .bind(validator);
        // try to validate if user has permission to execute print action for item
        const targetModel = this.context.model(reportCategory.appliesTo);
        if (targetModel == null) {
            throw new DataError('E_TARGET_MODEL', 'Report template target model cannot be found');
        }
        /**
         * define target object by assigning target model primary key
         * @type {*}
         */
        let target = {};
        Object.defineProperty(target, targetModel.primaryKey, {
            configurable: true,
            enumerable: true,
            value: queryParams.ID // use the default report parameter `ID`
        });
        // validate execute permission
        await validateAsync({
            model: targetModel, // current model
            privilege: targetModel.name.concat('/', 'Print'), // set privilege e.g. Student/Print
            mask: PermissionMask.Execute, // set mask to Execute
            target: target, // set target
        });
        // get original report template url
        const reportTemplateUri = await this.getModel()
            .where('id').equal(this.getId())
            .select('url').value();
        if (reportTemplateUri == null) {
            throw new DataError('E_REPORT_URI', 'Report template URI cannot be empty at this context');
        }
        // fetch report name
        this.name = await this.getModel().where('id').equal(this.id).select('name').value();
        // try to get description for object
        // todo: use toStringAsync() nethod if exists
        target = await targetModel.where(targetModel.primaryKey).equal(queryParams.ID).select(targetModel.primaryKey).getTypedItem();
        // set report description
        let description = this.name;
        if (typeof target.getDescription === 'function') {
            const targetDescription = await target.getDescription();
            description += ' ' + targetDescription;
        }
        // manage document numbers
        const documentNumberService = this.context.getApplication().getService(DocumentNumberService);
        let documentSeries;
        if (queryParams.REPORT_USE_DOCUMENT_NUMBER || queryParams.REPORT_DOCUMENT_NUMBER) {
            Args.check(documentNumberService != null, 'Application document number service cannot be found but current print operation is requesting a document number.');
            // get department series
            documentSeries = await this.context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(queryParams.REPORT_DOCUMENT_SERIES)
                .silent()
                .getTypedItem();
            Args.check(documentSeries != null, 'An active department document series cannot be found.');
            let formattedIndex;
            let documentNumberExists = false;
            if (queryParams.REPORT_DOCUMENT_NUMBER) {
                formattedIndex = queryParams.REPORT_DOCUMENT_NUMBER;
                documentNumberExists = true;
            } else {
                // get next document number
                const nextIndex = await documentSeries.next({
                    description
                });
                formattedIndex = await documentSeries.format(nextIndex);
            }
            // set REPORT_NUMBER
            Object.assign(queryParams, {
                REPORT_DOCUMENT_NUMBER: formattedIndex
            });
            // check if document verifier service exists
            // noinspection JSUnresolvedFunction
            /**
             * @type {DocumentNumberVerifierService}
             */
            const documentVerifier = this.context.getApplication().getService(DocumentNumberVerifierService);
            if (documentVerifier) {
                // generate verification URI
                Object.assign(queryParams, {
                    REPORT_DOCUMENT_QR_CODE: documentVerifier.generateURI(documentSeries.id, formattedIndex)
                });
                const attribute = this.context.model('DocumentNumberSeriesItem').getAttribute('shortDocumentCode');
                if (attribute != null) {
                    // get short document number
                    let seriesItem;
                    if (documentNumberExists) {
                        seriesItem = await this.context.model('DocumentNumberSeriesItem')
                            .where('documentNumber').equal(formattedIndex)
                            .and('parentDocumentSeries').equal(documentSeries.id)
                            .select('id', 'shortDocumentCode')
                            .silent().getItem();
                        if (seriesItem == null) {
                            throw new DataError('E_ITEM', 'The specified document series item cannot be found', null, 'DocumentNumberSeriesItem');
                        }
                        // validate short document code
                        if (seriesItem.shortDocumentCode == null) {
                            seriesItem.shortDocumentCode = documentVerifier.generateShortDocumentCode(this.context);
                            // update series item with short document code
                            await this.context.model('DocumentNumberSeriesItem').silent().save(seriesItem);
                        }
                        // assign verifier form properties
                        Object.assign(queryParams, {
                            REPORT_DOCUMENT_VERIFIER_URI: documentVerifier.getVerifierURI(),
                            REPORT_DOCUMENT_VERIFIER_CODE: seriesItem.shortDocumentCode
                        });
                    } else {
                        Object.assign(queryParams, {
                            REPORT_DOCUMENT_VERIFIER_URI: documentVerifier.getVerifierURI(),
                            REPORT_DOCUMENT_VERIFIER_CODE: documentVerifier.generateShortDocumentCode()
                        });
                    }
                }
            }
        }
        // get report buffer
        // todo: use jasper server export types e.g. *.doc, *.pdf etc
        const buffer = await printService.print(reportTemplateUri, '.pdf', queryParams);
        let contentLocation = null;
        if (queryParams.REPORT_USE_DOCUMENT_NUMBER) {
            // prepare to save a document number series item
            const newDocument = await documentNumberService.addFrom(this.context, buffer, {
                name: `${this.name} ${queryParams.REPORT_DOCUMENT_NUMBER}.pdf`.replace(/[<>:"/\\/|?*]/g, ""),
                description: description,
                contentType: 'application/pdf',
                documentNumber: queryParams.REPORT_DOCUMENT_NUMBER,
                parentDocumentSeries: documentSeries,
                shortDocumentCode: queryParams.REPORT_DOCUMENT_VERIFIER_CODE
            });
            
            // check if target item has a collection of documents and create an association between item and document
            const attribute = targetModel.attributes.find( attribute => {
                return attribute.name === 'documents';
            });
            if (attribute != null) {
                await target.property('documents').insert(newDocument);
                // get content alternate location
                contentLocation = newDocument.url;
            }
        }
        const bufferStream = new PassThrough();
        bufferStream.end(buffer);
        // docx => application/vnd.openxmlformats-officedocument.wordprocessingml.document
        bufferStream.contentType = 'application/pdf';
        // if content alternate location is defined
        if (contentLocation) {
            // set an extra property to stream in order to use it
            // as Content-Location response header
            bufferStream.contentLocation = contentLocation;
        }
        return bufferStream;
    }
}
module.exports = ReportTemplate;
