import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
import Person = require('./person-model');
import Department = require('./department-model');
import StudyProgram = require('./study-program-model');
import StudyProgramSpecialty = require('./study-program-specialty-model');
import StudentStatus = require('./student-status-model');
import Semester = require('./semester-model');
import AcademicPeriod = require('./academic-period-model');
import AcademicYear = require('./academic-year-model');
import InscriptionMode = require('./inscription-mode-model');
import InscriptionModeCategory = require('./inscription-mode-category-model');
import GradeScale = require('./grade-scale-model');
import MilitaryStatus = require('./military-status-model');
import StudentCategory = require('./student-category-model');
import User = require('./user-model');
import StudentPeriodRegistration = require('./student-period-registration-model');

/**
 * @class
 */
declare class Student extends DataObject {

     
     /**
      * @description Ο κωδικός της εγγραφής
      */
     public id: number; 
     
     /**
      * @description Ο κωδικός της επαφής
      */
     public person: Person|any; 
     
     /**
      * @description Αριθμός μητρώου φοιτητή
      */
     public studentIdentifier?: string; 
     
     /**
      * @description Το τμήμα που ανήκει ο φοιτητής
      */
     public department: Department|any; 
     
     /**
      * @description Το πρόγραμμα σπουδών που ακολουθεί ο φοιτητής.
      */
     public studyProgram: StudyProgram|any; 
     
     /**
      * @description Η κατεύθυνση του προγράμματος σπουδών που ακολουθεί ο φοιτητής.
      */
     public studyProgramSpecialty: StudyProgramSpecialty|any; 
     
     /**
      * @description Η κατάσταση του φοιτητή
      */
     public studentStatus: StudentStatus|any; 
     
     /**
      * @description Το τρέχον εξάμηνο του φοιτητή
      */
     public semester: number; 
     
     /**
      * @description Σχολείο αποφοίτησης
      */
     public schoolGraduated?: string; 
     
     /**
      * @description Σχολικό έτος αποφοίτησες
      */
     public schoolGraduatedYear?: number; 
     
     /**
      * @description Αριθμός δελτίου επιτυχίας
      */
     public inscriptionNumber?: number; 
     
     /**
      * @description Αριθμός απολυτηρίου
      */
     public schoolGraduationNumber?: string; 
     
     /**
      * @description Αριθμός σειράς εισαγωής
      */
     public inscriptionIndex?: number; 
     
     /**
      * @description Βαθμός απολυτηρίου
      */
     public schoolGraduationGrade?: string; 
     
     /**
      * @description Ημερομηνία εισαγωγής
      */
     public inscriptionDate: Date; 
     
     /**
      * @description Εξάμηνο εισαγωγής
      */
     public inscriptionSemester: Semester|any; 
     
     /**
      * @description Περίοδος εισαγωγής
      */
     public inscriptionPeriod: AcademicPeriod|any; 
     
     /**
      * @description Ακαδημαϊκό έτος εισαγωγής
      */
     public inscriptionYear: AcademicYear|any; 
     
     /**
      * @description Τρόπος εισαγωγής
      */
     public inscriptionMode: InscriptionMode|any; 
     
     /**
      * @description Τμήμα προέλευσης
      */
     public inscriptionDepartment?: string; 
     
     /**
      * @description Σχόλια εισαγωγής
      */
     public inscriptionComments?: string; 
     
     /**
      * @description Αριθμός απόφασης εισαγωγής
      */
     public inscriptionDecision?: string; 
     
     /**
      * @description Αναδρομικό έτος εισαγωγής
      */
     public inscriptionRetroYear?: AcademicYear|any; 
     
     /**
      * @description Αναδρομική περίοδος εισαγωγής
      */
     public inscriptionRetroPeriod?: AcademicPeriod|any; 
     
     /**
      * @description Μόρια εισαγωγής
      */
     public inscriptionPoints?: number; 
     
     /**
      * @description Χαρακτηρισμός τρόπου εισαγωγής
      */
     public inscriptionModeCategory?: InscriptionModeCategory|any; 
     
     /**
      * @description Ημερομηνία ανακήρυξης
      */
     public declaredDate?: Date; 
     
     /**
      * @description Ημερομηνία ορκωμοσίας
      */
     public graduationDate?: Date; 
     
     /**
      * @description Περίοδος ορκωμοσίας
      */
     public graduationPeriod?: AcademicPeriod|any; 
     
     /**
      * @description Ακαδημαϊκό έτος ορκωμοσίας
      */
     public graduationYear?: AcademicYear|any; 
     
     /**
      * @description Βαθμός πτυχίου (0-1)
      */
     public graduationGrade?: number; 
     
     /**
      * @description Τίτλος πτυχίου
      */
     public graduationTitle?: string; 
     
     /**
      * @description Βαθμός πτυχίου ολογράφως
      */
     public graduationGradeWrittenInWords?: string; 
     
     /**
      * @description Αριθμός πτυχίου
      */
     public graduationNumber?: string; 
     
     /**
      * @description Κλίμακα βαθμού πτυχίου
      */
     public graduationGradeScale?: GradeScale|any; 
     
     /**
      * @description Βαθμός πτυχίου (ανηγμένος στην κλίμακα)
      */
     public graduationGradeAdjusted?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας υποχρέωσης
      */
     public lastObligationDate?: Date; 
     
     /**
      * @description Τύπος ορκωμοσίας (Ορκωμοσία/Διαβεβαίωση)
      */
     public graduationType?: string; 
     
     /**
      * @description Ποσοστιαία βαθμολογία ανά έτος εισαγωγής
      */
     public graduationRankByInscriptionYear?: number; 
     
     /**
      * @description Ποσοστιαία βαθμολογία ανά έτος ορκωμοσίας
      */
     public graduationRankByGraduationYear?: number; 
     
     /**
      * @description Ημερομηνία διαγραφής
      */
     public removalDate?: Date;
     
     /**
      * @description Περίοδος διαγραφής
      */
     public removalPeriod?: AcademicPeriod|any;
     
     /**
      * @description Ακαδημαϊκό έτος διαγραφής
      */
     public removalYear?: AcademicYear|any;
     
     /**
      * @description Αίτητη διαγραφής
      */
     public removalRequest?: string;
     
     /**
      * @description Λόγος διαγραφής
      */
     public removalReason?: string;
     
     /**
      * @description Αριθμός απόφασης διαγραφής
      */
     public removalDecision?: string;
     
     /**
      * @description Τμήμα προορισμού
      */
     public removalDepartment?: string;
     
     /**
      * @description Αριθμός διαγραφής
      */
     public removalNumber?: string;
     
     /**
      * @description Σχόλια διαγραφής
      */
     public removalComments?: string;
     
     /**
      * @description Σχόλια
      */
     public comments?: string; 
     
     /**
      * @description Στρατιωτικές υποχρεώσεις
      */
     public militaryStatus?: MilitaryStatus|any; 
     
     /**
      * @description Κατηγορία φοιτητή
      */
     public category?: StudentCategory|any; 
     
     /**
      * @description Ημερομηνία αλλαγής κατεύθυνσης
      */
     public specialtyModifiedDate?: Date; 
     
     /**
      * @description Μοναδικός κωδικός φοιτητή
      */
     public uniqueIdentifier?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Δείκτης προόδου
      */
     public rate?: number; 
     
     /**
      * @description Ο συνδεδεμένος χρήστης
      */
     public user?: User|any; 
     
     /**
      * @description Student Registrations
      */
     public registrations?: Array<StudentPeriodRegistration|any>; 
     
     /**
      * @description Κατεύθυνση προγράμματος σπουδών (περιγραφή)
      */
     public specialty?: string; 
     
     /**
      * @description Κωδικός κατεύθυνσης προγράμματος σπουδών
      */
     public specialtyId?: number;

     getStudentGraduationRules(): Promise<any>;

     setStudentGraduationRules(items: any[]): Promise<any>;

}

export = Student;
