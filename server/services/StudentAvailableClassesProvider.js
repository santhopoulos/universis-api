import { ApplicationService, HttpNotFoundError } from '@themost/common';
import { DataObject, SchemaLoaderStrategy, ModelClassLoaderStrategy, EdmMapping, EdmType, DataQueryable } from '@themost/data';
import { QueryEntity, QueryField, QueryExpression, SqlFormatter } from '@themost/query';
import '../extensions/sql-formatter-extensions';

class Student extends DataObject {
    /**
     * @returns {Promise<DataQueryable>}
     */
     @EdmMapping.func('availableClasses',EdmType.CollectionOf('StudentAvailableClass'))
     async getAvailableClasses() {
        
        const studentSnapshot = await this.getModel().select(
            'id', 'department', 'department/currentYear as currentYear', 
            'department/currentPeriod as currentPeriod', 'studyProgram', 'specialtyId as specializationIndex'
            )
            .where('id').equal(this.id).flatten().getItem();
        if (studentSnapshot == null) {
            throw new HttpNotFoundError();
        }
        const CourseClass = this.context.model('CourseClass');
        const CourseClasses = CourseClass.viewAdapter;
        const StudentAvailableClasses = 'CourseClasses0';
        
        /**
         * @type {DataQueryable}
         */
        const q = new QueryExpression().where(
            new QueryField('year').from(StudentAvailableClasses)
        ).equal(studentSnapshot.currentYear)
            .and(
                new QueryField('period').from(StudentAvailableClasses)
            ).equal(studentSnapshot.currentPeriod).prepare()
            .and(
                new QueryField('isPassed').from('studentCourses')
            ).notEqual(1).prepare();
        q.select(
            new QueryField('id').from('specializationCourses'), // id
            new QueryField('id').from(StudentAvailableClasses).as('courseClass'), // courseClass
            new QueryField('id').from('studyProgramCourses').as('programCourse'), // programCourse
            new QueryField('id').from('student').as('student'), // student
            new QueryField('id').from('registrationType').as('registrationType'), // registrationType
            new QueryField('section').from('studentClasses'), // section
            new QueryField('id').from('status').as('status'), // status
            Object.assign(new QueryField(), {
                units: {
                    $cond: [
                        new QueryExpression().where(
                            new QueryField('units').from('studentCourses')
                        ).equal(null),
                        new QueryField('units').from('specializationCourses'),
                        new QueryField('units').from('studentCourses')
                    ]
                }
            }), // units
            Object.assign(new QueryField(), {
                coefficient: {
                    $cond: [
                        new QueryExpression().where(
                            new QueryField('coefficient').from('studentCourses')
                        ).equal(null),
                        new QueryField('coefficient').from('specializationCourses'),
                        new QueryField('coefficient').from('studentCourses')
                    ]
                }
            }), // coefficient
            new QueryField('weekHours').from(StudentAvailableClasses).as('hours'), // hours
            Object.assign(new QueryField(), {
                semester: {
                    $cond: [
                        new QueryExpression().where(
                            new QueryField('semester').from('studentCourses')
                        ).equal(null),
                        new QueryField('semester').from('specializationCourses'),
                        new QueryField('semester').from('studentCourses')
                    ]
                }
            }), // semester
            Object.assign(new QueryField(), {
                courseType: {
                    $cond: [
                        new QueryExpression().where(
                            new QueryField('courseType').from('studentCourses')
                        ).equal(null),
                        new QueryField('courseType').from('specializationCourses'),
                        new QueryField('courseType').from('studentCourses')
                    ]
                }
            }), // courseType
            Object.assign(new QueryField(), {
                ects: {
                    $cond: [
                        new QueryExpression().where(
                            new QueryField('ects').from('studentCourses')
                        ).equal(null),
                        new QueryField('ects').from('specializationCourses'),
                        new QueryField('ects').from('studentCourses')
                    ]
                }
            }), // ects
            new QueryField('displayCode').from('course'), // displayCode
            new QueryField('title').from(StudentAvailableClasses).as('name'), // name
            new QueryField('programGroup').from('studentCourses'), // programGroup
            Object.assign(new QueryField(), {
                registered: {
                    $cond: [
                        new QueryExpression().where(
                            new QueryField('lastRegistrationYear').from('studentCourses')
                        ).equal(null),
                        0,
                        -1
                    ]
                }
            }), // registered
            new QueryField('specializationIndex').from('specializationCourses').as('specialty'), // specialty
            new QueryField('parentCourse').from('course'), // parentCourse
            new QueryField('course').from(StudentAvailableClasses), // course
            new QueryField('courseArea').from('course'), // courseArea
            new QueryField('calculatedInRegistration').from('course'), // calculatedInRegistration
            new QueryField('mustRegisterSection').from(StudentAvailableClasses), // mustRegisterSection
            new QueryField('isPassed').from('studentCourses') // isPassed
        )
        q.from(new QueryEntity(CourseClasses).as(StudentAvailableClasses));
        

        const Student = this.context.model('Student');
        // JOIN StudentBase AS student 
        // ON student.id = ?
        q.join(new QueryEntity(Student.sourceAdapter).as('student')).with(
            new QueryExpression().where(new QueryField('id').from('student'))
                .equal(studentSnapshot.id)
        );

        const Course = this.context.model('Course');
        // JOIN CourseBase AS course 
        // ON course.id = StudentAvailableClasses.course
        q.join(new QueryEntity(Course.sourceAdapter).as('course')).with(
            new QueryExpression().where(new QueryField('id').from('course'))
                .equal(new QueryField('course').from(StudentAvailableClasses))
        );

        const RegistrationType = this.context.model('RegistrationType');
        // JOIN RegistrationTypeBase AS registrationType 
        // ON registrationType.alternateName = 'teaching'
        q.join(new QueryEntity(RegistrationType.sourceAdapter).left().as('registrationType')).with(
            new QueryExpression().where(new QueryField('alternateName').from('registrationType'))
                .equal('teaching')
        );

        const StudentClassStatus = this.context.model('StudentClassStatus');
        // LEFT JOIN RegistrationTypeBase AS registrationType 
        // ON registrationType.alternateName = 'teaching'
        q.join(new QueryEntity(StudentClassStatus.sourceAdapter).left().as('status')).with(
            new QueryExpression().where(new QueryField('id').from('status'))
                .equal(1)
        );


        const StudyProgramCourse = this.context.model('StudyProgramCourse');
        // JOIN StudyProgramCourseBase AS studyProgramCourses 
        // ON studyProgramCourses.course = CourseClassData.course
        // AND studyProgramCourses.studyProgram = ?
        q.join(new QueryEntity(StudyProgramCourse.sourceAdapter).as('studyProgramCourses')).with(
            new QueryExpression().where(new QueryField('course').from(StudentAvailableClasses))
                .equal(new QueryField('course').from('studyProgramCourses'))
                .and(new QueryField('studyProgram').from('studyProgramCourses'))
                .equal(studentSnapshot.studyProgram)
        );
        const SpecializationCourse = this.context.model('SpecializationCourse');
        const SpecializationCourses = SpecializationCourse.sourceAdapter;
        // JOIN SpecializationCourseBase AS specializationCourses 
        // ON StudyProgramCourses.id = specializationCourses.studyProgramCourse
        // AND SpecializationCourses.specializationIndex = ? (OR SpecializationCourses.specializationIndex = -1) 

        const qSpecializationCourses = new QueryExpression().where(new QueryField('id').from('studyProgramCourses'))
            .equal(new QueryField('studyProgramCourse').from('specializationCourses'))
            .prepare();
        if (studentSnapshot.specializationIndex > -1) {
            qSpecializationCourses.where(
                    new QueryField('specializationIndex').from('specializationCourses')
                )
                .equal(studentSnapshot.specializationIndex)
                .or(
                    new QueryField('specializationIndex').from('specializationCourses')
                )
                .equal(-1);
        } else {
            qSpecializationCourses.where(
                new QueryField('specializationIndex').from('specializationCourses')
            )
            .equal(-1);
        }
        
        q.join(new QueryEntity(SpecializationCourses).as('specializationCourses')).with(
            qSpecializationCourses
        );
        const StudentCourse = this.context.model('StudentCourse');
        const StudentCourses = StudentCourse.sourceAdapter;
        // LEFT JOIN StudentCourseBase AS studentCourses 
        // ON studentCourses.course = studyProgramCourses.course
        // AND studentCourses.student = ? 
        // (AND studentCourses.isPassed <> -1)
        q.join(new QueryEntity(StudentCourses).left().as('studentCourses')).with(
            new QueryExpression().where(new QueryField('course').from('studentCourses'))
                .equal(new QueryField('course').from('studyProgramCourses'))
                .and(new QueryField('student').from('studentCourses'))
                .equal(studentSnapshot.id)
        );

        const StudentCourseClass = this.context.model('StudentCourseClass');
        const StudentCourseClasses = StudentCourseClass.sourceAdapter;
        // LEFT JOIN (
        // 
        // SELECT StudentCourseClassBase.course, StudentCourseClassBase.section,
        // CourseClass0.year, CourseClass0.period
        //  FROM StudentCourseClassBase INNER JOIN CourseClassBase AS CourseClass0 ON 
        // StudentCourseClassBase.courseClass = CourseClass0.courseClass 
        // AND StudentCourseClassBase.student = ?
        // ) AS studentClasses 
        //
        // ON studentClasses.course = studentCourses.course
        // AND studentClasses.year = ?
        // AND studentClasses.period = ?

        // prepare nested query
        const q1 = new QueryExpression().from(StudentCourseClasses)
            .select(
                new QueryField('course').from(StudentCourseClasses),
                new QueryField('section').from(StudentCourseClasses),
                new QueryField('year').from('CourseClass0'),
                new QueryField('period').from('CourseClass0')
            )
            .join(new QueryEntity(CourseClasses).as('CourseClass0'))
            .with(
                new QueryExpression().where(new QueryField('courseClass').from(StudentCourseClasses))
                    .equal(new QueryField('id').from('CourseClass0'))
                    .and(new QueryField('student').from(StudentCourseClasses))
                    .equal(studentSnapshot.id)
            );
        q1.$alias =  'studentClasses';
        q1.$join = 'left';
        q.join(q1).with(
            new QueryExpression().where(new QueryField('course').from('studentCourses'))
                .equal(new QueryField('course').from('studentClasses'))
                .and(new QueryField('year').from('studentClasses'))
                .equal(new QueryField('lastRegistrationYear').from('studentCourses'))
                .and(new QueryField('period').from('studentClasses'))
                .equal(new QueryField('lastRegistrationPeriod').from('studentCourses'))
        );

        q.$alias = 'StudentAvailableClasses';
        const final = this.context.model('StudentAvailableClass').asQueryable();
        final.query.from(q);
        return final;
     }
}

class StudentAvailableClassesProvider extends ApplicationService {
    constructor(app) {
        super(app);
        this.apply();
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Student');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const LocalDepartmentBase = loader.resolve(model);
        // extend class
        Object.assign(LocalDepartmentBase.prototype, {
            getAvailableClasses: Student.prototype.getAvailableClasses
        });
    }
}

export {
    StudentAvailableClassesProvider
}