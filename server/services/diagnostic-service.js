import {ApplicationService} from "@themost/common/app";
import {serviceRouter} from '@themost/express';
import {Args} from '@themost/common';
import path from 'path';

/**
 * @interface ApiServerStatus
 * @property {string} version - The current version of api server
 * @property {string|*} database - The current status of backed database (e.g. online, offline or sync)
 * @property {Date|*} modifiedAt - The date and time when database has been last modified
 * @property {string|*} attachedAt - The date and time when database has been last attached
 */

// noinspection JSUnusedGlobalSymbols
/**
 * @class
 */
export class DiagnosticService extends ApplicationService {

    /**
     * @param {ExpressDataApplication} app
     */
    constructor(app) {
        super(app);
        const self = this;
        serviceRouter.get('/diagnostics/services', async function getServices(req, res) {
            const result = await self.getServices();
            return res.json(result);
        });
        /**
         * @swagger
         *
         * /api/diagnostics/status:
         *  get:
         *    tags:
         *      - Diagnostics
         *    description: Returns api server status including version, database status etc
         *    security:
         *      - OAuth2:
         *          - teachers
         *          - students
         *          - registrar
         *    responses:
         *      '200':
         *        description: success
         *        content:
         *          application/json:
         *            schema:
         *              type: object
         *      '403':
         *        description: forbidden
         *      '500':
         *        description: internal server error
         */
        serviceRouter.get('/diagnostics/status', async function getStatus(req, res, next) {
            try {
                const result = await self.getStatus(req.context);
                return res.json(result);
            }
            catch(err) {
                // continue with error
                return next(err);
            }

        });
    }

    /**
     * Returns api server status including version, database status etc
     * @param {ExpressDataContext} context - The current request context
     * @returns {Promise<ApiServerStatus>}
     */
    async getStatus(context) {
        const packageJson = require(path.resolve(process.cwd(), './package.json'));
        /**
         * @type {ApiServerStatus|*}
         */
        const result = {
            version: packageJson.version
        };
        // get workspace data
        let workspace  = await context.model('Workspace').asQueryable().silent().getItem();
        if (workspace == null) {
            // set defaults
            workspace = {
                databaseStatus: 'online',
                dateLastAttached: null,
                dateModified: new Date()
            };
        }
        // assign properties
        Object.assign(result, {
            database:  workspace.databaseStatus,
            modifiedAt:  workspace.dateModified,
            attachedAt: workspace.dateLastAttached
        });
        // and finally return result
        return result;
    }

    async getServices() {
        // get services collection
        Args.check(this.getApplication().services != null, 'Invalid application dependency. Express data application middleware version does not meet diagnostic service requirements.');
        const services = this.getApplication().services || [];
        return Object.keys(services).map(key => {
            //get service
            let service = services[key];
            if (service && service.constructor && service.constructor.name === key) {
                // super service is ApplicationService  so return only serviceType
                return {
                    serviceType: service.constructor.name
                };
            }
            return {
                serviceType: key,
                strategyType: service.constructor.name
            };
        });
    }

}
