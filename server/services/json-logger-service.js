// eslint-disable-next-line no-unused-vars
import {ApplicationService, TraceUtils, IApplication} from "@themost/common";
import moment from "moment";

const DEFAULTS = {
    dateFormat: 'DD/MMM/YYYY:HH:mm:ss Z',
    logLevel: null, // error, info, verbose, debug
    format: null // pretty, raw
}

const LEVELS = [
    'INFO',
    'ERROR',
    'VERBOSE',
    'DEBUG'
]

class JsonLogger {
    constructor(options) {
        this.options = options || DEFAULTS;
        if (this.options.logLevel == null) {
            // set log level based on process env
            this._level = (process.env.NODE_ENV === 'development') ? JsonLogger.Levels.debug : JsonLogger.Levels.info;
        } else if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, this.options.logLevel)) {
            // set the given log level
            this._level = JsonLogger.Levels[this.options.logLevel]
        } else {
            // set default log level
            this._level = JsonLogger.Levels.info;
        }
        if (this.options.format == null) {
            this.options.format = (process.env.NODE_ENV === 'development') ? 'pretty' : 'raw';
        }
    }

    level(level) {
        // set level if the given level is valid
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, level)) {
            // set level
            this._level = JsonLogger.Levels[level];
        }
        // otherwise throw error
        else {
            throw new Error('Invalid log level. Expected one of error, warn, info, verbose, debug.')
        }
        return this;
    }

    static get Levels() {
        return {
            error: 0,
            warn: 1,
            info: 2,
            verbose: 3,
            debug: 4
        };
    }

    /**
     * @param args
     * @return {string|*}
     * @private
     */
    static _trimLogLevel(args) {
        if (Array.isArray(args)) {
            for (let i = 0; i < args.length; i++) {
                const arg = args[i];
                if (typeof arg === 'string') {
                    if (LEVELS.includes(arg)) {
                        const removed = args.splice(i,1);
                        return removed[0] && removed[0].toLowerCase();
                    }
                }
            }
        }
    }

    // eslint-disable-next-line no-unused-vars
    log(_data) {
        const args = Array.from(arguments);
        // trim level defined as argument
        const inlineLevel = JsonLogger._trimLogLevel(args);
        // if a log level has been defined inline use this instead of the level defined for this call
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, inlineLevel)) {
            args.unshift(inlineLevel);
            return this._write.apply(this, args);
        }
        // otherwise use log level defined for this call
        args.unshift("info");
        this._write.apply(this, args);
    }
    // eslint-disable-next-line no-unused-vars
    info(_data) {
        const args = Array.from(arguments);
        // trim level defined as argument
        const inlineLevel = JsonLogger._trimLogLevel(args);
        // if a log level has been defined inline use this instead of the level defined for this call
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, inlineLevel)) {
            args.unshift(inlineLevel);
            return this._write.apply(this, args);
        }
        args.unshift("info");
        this._write.apply(this, args);
    }
    // eslint-disable-next-line no-unused-vars
    error(_data) {
        const args = Array.from(arguments);
        // trim level defined as argument
        const inlineLevel = JsonLogger._trimLogLevel(args);
        // if a log level has been defined inline use this instead of the level defined for this call
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, inlineLevel)) {
            args.unshift(inlineLevel);
            return this._write.apply(this, args);
        }
        args.unshift("error");
        this._write.apply(this, args);
    }
    // eslint-disable-next-line no-unused-vars
    warn(_data) {
        const args = Array.from(arguments);
        // trim level defined as argument
        const inlineLevel = JsonLogger._trimLogLevel(args);
        // if a log level has been defined inline use this instead of the level defined for this call
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, inlineLevel)) {
            args.unshift(inlineLevel);
            return this._write.apply(this, args);
        }
        args.unshift("warn");
        this._write.apply(this, args);
    }
    // eslint-disable-next-line no-unused-vars
    verbose(_data) {
        const args = Array.from(arguments);
        // trim level defined as argument
        const inlineLevel = JsonLogger._trimLogLevel(args);
        // if a log level has been defined inline use this instead of the level defined for this call
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, inlineLevel)) {
            args.unshift(inlineLevel);
            return this._write.apply(this, args);
        }
        args.unshift("verbose");
        this._write.apply(this, args);
    }
    // eslint-disable-next-line no-unused-vars
    debug(_data) {
        const args = Array.from(arguments);
        // trim level defined as argument
        const inlineLevel = JsonLogger._trimLogLevel(args);
        // if a log level has been defined inline use this instead of the level defined for this call
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, inlineLevel)) {
            args.unshift(inlineLevel);
            return this._write.apply(this, args);
        }
        // set log level as first param
        args.unshift("debug");
        // and write
        this._write.apply(this, args);
    }

    /**
     * @param _data
     * @private
     */
    // eslint-disable-next-line no-unused-vars
    _write(_data) {
        const args = Array.from(arguments);
        if (args.length === 0) {
            // do nothing
            return;
        }
        // get first argument which is the log level
        let level = args[0] || 'info';
        if (Object.prototype.hasOwnProperty.call(JsonLogger.Levels, level) === false) {
            level = 'info';
        }
        if (this._level >= JsonLogger.Levels[level]) {
            // eslint-disable-next-line no-console
            let log = console.log.bind(console);
            if (level === 'error') {
                // eslint-disable-next-line no-console
                log = console.error.bind(console);
            }
            // add formatted timestamp
            args.unshift(moment().format(this.options.dateFormat || 'DD/MMM/YYYY:HH:mm:ss Z'));
            // remove log level in arguments
            // (because it's already defined by this call)
            for (let i = 0; i < args.length; i++) {
                const arg = args[i];
                if (arg instanceof Error) {
                    const cloned = {};
                    Object.getOwnPropertyNames(arg).forEach(key => {
                        cloned[key] = arg[key];
                    });
                    // log error type
                    const proto = Object.getPrototypeOf(arg);
                    if (proto && proto.constructor) {
                        Object.defineProperty(cloned, 'type', {
                            configurable: true,
                            enumerable: true,
                            writable: true,
                            value: proto.constructor.name || 'Error'
                        });
                    }
                    // replace arg
                    args[i] = cloned;
                }


            }
            if (this.options.format === 'pretty') {
                // eslint-disable-next-line no-console
                log(JSON.stringify(args, null, 2));
            } else {
                // otherwise log raw data
                // eslint-disable-next-line no-console
                log(JSON.stringify(args));
            }
        }
    }

}

class JsonLoggerService extends  ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        const options = app.getConfiguration().getSourceAt('settings/universis/jsonLogger');
        // use json logger
        TraceUtils.useLogger(new JsonLogger(options));
    }
}

export {
    JsonLogger,
    JsonLoggerService
}
