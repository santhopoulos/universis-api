import path from 'path';
import {ODataModelBuilder} from '@themost/data/odata';
import {DataConfigurationStrategy} from '@themost/data/data-configuration';
import {DataModel} from '@themost/data/data-model';
import {ApplicationService} from '@themost/common/app';
import {URL} from 'url';
import swaggerJSDoc from 'swagger-jsdoc';
let npmPackage = require(path.resolve(process.cwd(),'package.json'));

const ODATA_ENTITY_SET_PARAMETERS = [
    {
        "name": "$filter",
        "in": "query",
        "description": "The $filter system query option restricts the set of items returned. e.g. $filter=id eq 1",
        "required": false,
        "schema": {
            "type": "string"
        }
    },
    {
        "name": "$orderby",
        "in": "query",
        "description": "The $orderby System Query option specifies the order in which items are returned from the service. e.g. $orderby=dateCreated desc",
        "required": false,
        "schema": {
            "type": "string"
        }
    },
    {
        "name": "$select",
        "in": "query",
        "description": "The $select system query option requests that the service return only the properties, dynamic properties requested by the client e.g. $select=id,name,dateCreated",
        "required": false,
        "schema": {
            "type": "string"
        }
    },
    {
        "name": "$top",
        "in": "query",
        "description": "The $top system query option specifies a non-negative integer n that limits the number of items returned from a collection e.g. $top=10",
        "required": false,
        "schema": {
            "type": "integer"
        }
    },
    {
        "name": "$skip",
        "in": "query",
        "description": "The $skip system query option specifies a non-negative integer n that excludes the first n items of the queried collection from the result.",
        "required": false,
        "schema": {
            "type": "integer"
        }
    },
    {
        "name": "$expand",
        "in": "query",
        "description": "The set of expanded entities can be further refined through the application of expand options, expressed as a semicolon-separated list of system query options.",
        "required": false,
        "schema": {
            "type": "string"
        }
    }
];

const ODATA_ENTITY_PARAMETERS = [
    {
        "name": "$select",
        "in": "query",
        "description": "The $select system query option requests that the service return only the properties, dynamic properties requested by the client e.g. $select=id,name,dateCreated",
        "required": false,
        "schema": {
            "type": "string"
        }
    },
    {
        "name": "$expand",
        "in": "query",
        "description": "The set of expanded entities can be further refined through the application of expand options, expressed as a semicolon-separated list of system query options.",
        "required": false,
        "schema": {
            "type": "string"
        }
    }
];

/**
 * @this ODataModelBuilder
 * @param {EntityTypeProperty} property
 * @returns {*}
 */
function getPropertyReturnType(property) {
    let type;
    switch (property.type) {
        case "Edm.String":
        case "Edm.DateTimeOffset":
        case "Edm.Duration":
        case "Edm.Guid":
        case 'Edm.DateTime':
        case 'Edm.Date':
        case 'Edm.URL':
        case 'Edm.Text':
            type = 'string';
            break;
        case "Edm.Boolean":
            type = 'boolean';
            break;
        case "Edm.Decimal":
        case "Edm.Double":
        case "Edm.Single":
        case 'Edm.Number':
            type = 'number';
            break;
        case "Edm.Int64":
            type = "int64";
            break;
        case "Edm.Int32":
        case "Edm.Int16":
        case 'Edm.Integer':
        case 'Edm.Counter':
            type = 'integer';
            break;
        default:
            type = 'object';
    }
    let result = {
        "type": type
    }
    if (property.description) {
        Object.assign(result, {
            "description": property.description
        });
    }
    return result;
}

/**
 * @this ODataModelBuilder
 * @param {EntityTypeNavigationProperty} navigationProperty
 * @returns {*}
 */
function getNavigationPropertyReturnType(navigationProperty) {
    let result;
    if (/^Collection\((\w+)\)$/.test(navigationProperty.type)) {
        let type = /^Collection\((\w+)\)$/.exec(navigationProperty.type)[1];
        let entity = this.getEntity(type);
        if (typeof entity === 'undefined') {
            result = {
                "type": "array",
                "items": {
                    "type": getPropertyReturnType.bind(this)(`Edm.${type}`).type,
                }
            };
        }
        else {
            result = {
                "type": "array",
                "items": {
                    "$ref": `#/components/schemas/${type}`,
                }
            };
        }

    }
    else {
        result = {
            "$ref": `#/components/schemas/${navigationProperty.type}`
        }
    }
    if (navigationProperty.description) {
        Object.assign(result, { "description": navigationProperty.description })
    }
    return result;


}

/**
 * @this {ODataModelBuilder}
 * @param {EntityTypeConfiguration} entityType
 * @returns {*}
 */
function getEntityTypePropertyList(entityType) {
    let result = {};
    entityType.property.forEach(x => {
        result[x.name] = x;
    });
    let baseEntityType = this.getEntity(entityType.baseType);
    while (baseEntityType) {
        baseEntityType.property.forEach(x=> {
            result[x.name] = x;
        });
        baseEntityType = this.getEntity(baseEntityType.baseType);
    }
    return result;
}

/**
 * @this {ODataModelBuilder}
 * @param {EntityTypeConfiguration} entityType
 * @return {*}
 */
function getEntityTypeNavigationPropertyList(entityType) {
    let result = {};
    entityType.navigationProperty.forEach(x => {
        result[x.name] = x;
    });
    let baseEntityType = this.getEntity(entityType.baseType);
    while (baseEntityType) {
        baseEntityType.navigationProperty.forEach(x=> {
            result[x.name] = x;
        });
        baseEntityType = this.getEntity(baseEntityType.baseType);
    }
    return result;
}

/**
 * @this IApplication
 * @param {string} name
 * @return {DataModel}
 */
function getModel(name) {
    let self = this;
    /**
     *
     * @type {DataConfigurationStrategy}
     */
    let dataConfiguration = self.getConfiguration().getStrategy(DataConfigurationStrategy);
    /**
     * @type {DataModel}
     */
    let result = new DataModel(dataConfiguration.model(name));
    result.context = {
        getConfiguration: function () {
            return self.getConfiguration();
        },
        model: function (name) {
            let result = new DataModel(dataConfiguration.model(name));
            result.context = this;
            return result;
        }
    };
    return result;
}

/**
 * @this ODataModelBuilder
 * @param {EntitySetConfiguration} entitySet
 * @param {FunctionConfiguration} func
 */
function collectionFunctionConfigurationToSwaggerPath(entitySet, func) {
    let builder = this;
    if (func.returnCollectionType) {
        return {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity function",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                ].concat(ODATA_ENTITY_SET_PARAMETERS),
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "total": {
                                            "type": "integer",
                                            "description": "Defines the total number of entities"
                                        },
                                        "skip": {
                                            "type": "integer",
                                            "description": "Defines the number of entities that were skipped according to paging parameters"
                                        },
                                        "value": {
                                            "type": "array",
                                            "description": "An array of items",
                                            "items": {
                                                "$ref": `#/components/schemas/${func.returnCollectionType}`
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }
    else {
        return {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity function",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [

                ].concat(ODATA_ENTITY_PARAMETERS),
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }

}

/**
 * @this ODataModelBuilder
 * @param {EntitySetConfiguration} entitySet
 * @param {ActionConfiguration} action
 */
function collectionActionConfigurationToSwaggerPath(entitySet, action) {
    let builder = this;
    if (action.returnCollectionType) {
        return {
            "post": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity action",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "total": {
                                            "type": "integer",
                                            "description": "Defines the total number of entities"
                                        },
                                        "skip": {
                                            "type": "integer",
                                            "description": "Defines the number of entities that were skipped according to paging parameters"
                                        },
                                        "value": {
                                            "type": "array",
                                            "description": "An array of items",
                                            "items": {
                                                "$ref": `#/components/schemas/${action.returnCollectionType}`
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }
    else {
        return {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity action",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }

}

/**
 * @this ODataModelBuilder
 * @param {EntitySetConfiguration} entitySet
 * @param {FunctionConfiguration} func
 */
function functionConfigurationToSwaggerPath(entitySet, func) {
    let builder = this;
    if (func.returnCollectionType) {
        return {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity function",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The identifier of the object",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ].concat(ODATA_ENTITY_SET_PARAMETERS),
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "total": {
                                            "type": "integer",
                                            "description": "Defines the total number of entities"
                                        },
                                        "skip": {
                                            "type": "integer",
                                            "description": "Defines the number of entities that were skipped according to paging parameters"
                                        },
                                        "value": {
                                            "type": "array",
                                            "description": "An array of items",
                                            "items": {
                                                "$ref": `#/components/schemas/${func.returnCollectionType}`
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }
    else {
        return {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity function",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The identifier of the object",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ].concat(ODATA_ENTITY_PARAMETERS),
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }

}

/**
 * @this ODataModelBuilder
 * @param {EntitySetConfiguration} entitySet
 * @param {ActionConfiguration} action
 */
function actionConfigurationToSwaggerPath(entitySet, action) {
    let builder = this;
    if (action.returnCollectionType) {
        return {
            "post": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity action",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The identifier of the object",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "total": {
                                            "type": "integer",
                                            "description": "Defines the total number of entities"
                                        },
                                        "skip": {
                                            "type": "integer",
                                            "description": "Defines the number of entities that were skipped according to paging parameters"
                                        },
                                        "value": {
                                            "type": "array",
                                            "description": "An array of items",
                                            "items": {
                                                "$ref": `#/components/schemas/${action.returnCollectionType}`
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }
    else {
        return {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Executes an entity action",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The identifier of the object",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            }
        }
    }

}

/**
 * @param {IApplication} app
 */
function getSwaggerComponents(app) {
    /**
     * @type {ODataModelBuilder|*}
     */
    let builder = app.getStrategy(ODataModelBuilder);
    let schema = builder.getEdmSync();
    let swaggerDocument = {
        "openapi": "3.0.0",
        "info": {
            "description": npmPackage.description,
            "version": npmPackage.version,
            "title": npmPackage.name,
            "termsOfService": "https://example.com/terms/",
            "contact": {
                "email": "support@example.com"
            },
            "license": {
                "name": "Apache 2.0",
                "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
            }
        },
        "paths": {},
        "components": {
            "schemas": {

            }
        }
    };
    let ignoreEntityType = [];
    // enumerate entities
    schema.entityType.forEach((entityType) => {
        //add model definition
        let schemaDefinition = {
            "type": "object",
            "required": [],
            "properties":{}
        };
        /**
         * @type {DataModel}
         */
        let entityTypeModel = getModel.bind(app)(entityType.name);
        if (entityTypeModel && entityTypeModel.description) {
            schemaDefinition.description = entityTypeModel.description;
        }

        let propertyList = getEntityTypePropertyList.bind(builder)(entityType);
        Object.keys(propertyList).map(key=> {
            return propertyList[key];
        }).forEach(property => {
            let returnType = getPropertyReturnType.bind(builder)(property);
            let attribute = entityTypeModel.getAttribute(property.name);
            if (attribute && attribute.description) {
                returnType.description = attribute.description;
                returnType.title = attribute.title;
            }
            schemaDefinition.properties[property.name] = returnType;
        });


        let navigationPropertyList = getEntityTypeNavigationPropertyList.bind(builder)(entityType);
        Object.keys(navigationPropertyList).map(key=> {
            return navigationPropertyList[key];
        }).forEach(property => {
            let returnType = getNavigationPropertyReturnType.bind(builder)(property);
            //get navigation property attribute
            let navigationPropertyAttribute = entityTypeModel.getAttribute(property.name);
            if (!navigationPropertyAttribute.expandable) {
                // get mapping
                let navigationPropertyMapping = entityTypeModel.inferMapping(property.name);
                if (navigationPropertyMapping) {
                    if (navigationPropertyMapping.associationType === 'association' && (navigationPropertyAttribute.many === false || typeof navigationPropertyAttribute.many === 'undefined')) {
                        // this navigation property is a foreign key association
                        // get parent key
                        let parentModel = getModel.bind(app)(navigationPropertyMapping.parentModel);
                        if (parentModel) {
                            let parentAttribute = parentModel.getAttribute(navigationPropertyMapping.parentField);
                            returnType = getPropertyReturnType.bind(builder)({
                                "name": parentAttribute.name,
                                "type": `Edm.${parentAttribute.type}`,
                                "nullable": typeof parentAttribute.nullable === 'boolean' ? parentAttribute.nullable : true
                            });
                        }
                    }
                }
            }
            // set navigation property description
            let attribute = entityTypeModel.getAttribute(property.name);
            if (attribute && attribute.description) {
                returnType.description = attribute.description;
                returnType.title = attribute.title;
            }
            // validate return type
            let entityTypeName;
            // get entity type name
            if (returnType.hasOwnProperty('$ref')) {
                entityTypeName = /^#\/components\/schemas\/(\w+)$/.exec(returnType.$ref)[1];
            }
            else if (returnType.type === 'array' && returnType.items.hasOwnProperty('$ref')) {
                entityTypeName = /^#\/components\/schemas\/(\w+)$/.exec(returnType.items.$ref)[1];
            }
            if (entityTypeName) {
                // find entity type in schema
                let findEntity = schema.entityType.find(x=> {
                    return x.name === entityTypeName;
                });
                // if entity cannot be found
                if (typeof findEntity === 'undefined') {
                    // try to find entity in ignored entities
                    findEntity = ignoreEntityType.find(x=> {
                        return x.name === entityTypeName;
                    });
                    // if entity cannot be found in ignored entities
                    if (typeof findEntity === 'undefined') {
                        // get entity from builder
                        let entity = builder.getEntity(entityTypeName);
                        // add entity to ignored entities
                        if (entity) {
                            ignoreEntityType.push(entity);
                        }
                    }
                }
            }
            schemaDefinition.properties[property.name] = returnType
        });

        let requiredProperty = Object.keys(propertyList).map(key=> {
            return propertyList[key];
        }).filter(x=> {
            return typeof x.nullable === 'boolean' ? !x.nullable : false;
        }).map(x=> {
            return x.name;
        });

        let requiredNavigationProperty = Object.keys(navigationPropertyList).map(key=> {
            return navigationPropertyList[key];
        }).filter(x=> {
            return typeof x.nullable === 'boolean' ? !x.nullable : false;
        }).map(x=> {
            return x.name;
        });

        schemaDefinition.required = requiredProperty.concat(requiredNavigationProperty);

        swaggerDocument.components.schemas[entityType.name] = schemaDefinition;
    });
    // enumerate ignored entities
    ignoreEntityType.forEach((entityType) => {
        //add model definition
        let schemaDefinition = {
            "type": "object",
            "properties":{}
        };
        let propertyList = getEntityTypePropertyList.bind(builder)(entityType);
        Object.keys(propertyList).map(key=> {
            return propertyList[key];
        }).forEach(property => {
            schemaDefinition.properties[property.name] = getPropertyReturnType.bind(builder)(property)
        });
        let navigationPropertyList = getEntityTypeNavigationPropertyList.bind(builder)(entityType);
        Object.keys(navigationPropertyList).map(key=> {
            return navigationPropertyList[key];
        }).forEach(property => {
            schemaDefinition.properties[property.name] = getNavigationPropertyReturnType.bind(builder)(property);
        });
        swaggerDocument.components.schemas[entityType.name] = schemaDefinition;
    });
    swaggerDocument.components.securitySchemes = {
        OAuth2: {
            type: "oauth2",
            description: "For more information, see https://api.slack.com/docs/oauth",
            flows: {
                authorizationCode: {
                    authorizationUrl: new URL("authorize", app.getConfiguration().getSourceAt('settings/auth/server_uri')).toString(),
                    tokenUrl: new URL("access_token", app.getConfiguration().getSourceAt('settings/auth/server_uri')).toString(),
                    scopes: {
                        "students": "Read and write student information",
                        "teachers": "Read and write instructor information"
                    }
                }
            }
        }
    };

    return swaggerDocument;

}

function getSwaggerUserDefinition(app) {
    const swaggerDefinition = {
        "openapi": "3.0.0",
        "info": {
            "description": npmPackage.description,
            "version": npmPackage.version,
            "title": npmPackage.name,
            "termsOfService": "https://example.com/terms/",
            "contact": {
                "email": "support@example.com"
            },
            "license": {
                "name": "Apache 2.0",
                "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
            }
        },
        "paths": {},
        "components": {
            "schemas": {
            },
            "securitySchemes": {
            }
        }
    };
    Object.assign(swaggerDefinition.components, getSwaggerComponents(app).components);
    const options = {
        swaggerDefinition,
        apis: [path.resolve(process.cwd(),'server/models/*.js'), path.resolve(process.cwd(),'server/routes/*.js')]
    };
    return swaggerJSDoc(options);
}

/**
 * @param {IApplication} app
 */
function getSwaggerDocument(app) {
    /**
     * @type {ODataModelBuilder|*}
     */
    let builder = app.getStrategy(ODataModelBuilder);
    let schema = builder.getEdmSync();
    let swaggerDocument = {
        "openapi": "3.0.0",
        "info": {
            "description": npmPackage.description,
            "version": npmPackage.version,
            "title": npmPackage.name,
            "termsOfService": "https://example.com/terms/",
            "contact": {
                "email": "support@example.com"
            },
            "license": {
                "name": "Apache 2.0",
                "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
            }
        },
        "paths": {},
        "components": {
            "schemas": {

            }
        }
    };


    // enumerate entity sets
    schema.entityContainer.entitySet.forEach(entitySet => {
        // GET items
        swaggerDocument.paths[app.resolveUrl(`~/api/${entitySet.url}/`)] = {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Returns an entity set based on the given parameters",
                "security": [
                    {
                        "OAuth2": [
                        "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "$filter",
                        "in": "query",
                        "description": "The $filter system query option restricts the set of items returned. e.g. $filter=id eq 1",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "$orderby",
                        "in": "query",
                        "description": "The $orderby System Query option specifies the order in which items are returned from the service. e.g. $orderby=dateCreated desc",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "$select",
                        "in": "query",
                        "description": "The $select system query option requests that the service return only the properties, dynamic properties requested by the client e.g. $select=id,name,dateCreated",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "$top",
                        "in": "query",
                        "description": "The $top system query option specifies a non-negative integer n that limits the number of items returned from a collection e.g. $top=10",
                        "required": false,
                        "schema": {
                            "type": "integer"
                        }
                    },
                    {
                        "name": "$skip",
                        "in": "query",
                        "description": "The $skip system query option specifies a non-negative integer n that excludes the first n items of the queried collection from the result.",
                        "required": false,
                        "schema": {
                            "type": "integer"
                        }
                    },
                    {
                        "name": "$expand",
                        "in": "query",
                        "description": "The set of expanded entities can be further refined through the application of expand options, expressed as a semicolon-separated list of system query options.",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "total": {
                                            "type": "integer",
                                            "description": "Defines the total number of entities"
                                        },
                                        "skip": {
                                            "type": "integer",
                                            "description": "Defines the number of entities that were skipped according to paging parameters"
                                        },
                                        "value": {
                                            "type": "array",
                                            "description": "An array of items",
                                            "items": {
                                                "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad request"
                    }
                }
            },
            "post": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Inserts or updates an array of entities",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "array",
                                "description": "An array of items",
                                "items": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "description": "An array of items",
                                    "items": {
                                        "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                    }
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "500": {
                        "description": "Internal Server Error"
                    }
                }
            }
        };
        // GET entitySet/:id
        swaggerDocument.paths[app.resolveUrl(`~/api/${entitySet.url}/{id}/`)] = {
            "get": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Gets an entity based on the given identifier",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The identifier of the object",
                        "required": true,
                        "schema": {
                            "type": "integer"
                        }
                    },
                    {
                        "name": "$expand",
                        "in": "query",
                        "description": "The set of expanded entities can be further refined through the application of expand options, expressed as a semicolon-separated list of system query options.",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No content"
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            },
            "post": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Updates an entity based on the given identifier",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The identifier of the object",
                        "required": true,
                        "schema": {
                            "type": "integer"
                        }
                    }
                ],
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": `#/components/schemas/${entitySet.entityType.name}`
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "500": {
                        "description": "Internal Server Error"
                    }
                }
            },
            "delete": {
                "tags": [
                    entitySet.entityType.name
                ],
                "summary": "Removes an entity based on the given identifier",
                "security": [
                    {
                        "OAuth2": [
                            "students"
                        ]
                    }
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The identifier of the object",
                        "required": true,
                        "schema": {
                            "type": "integer"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": `#/components/schemas/${entitySet.entityType.name}`
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not Found"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "500": {
                        "description": "Internal Server Error"
                    }
                }
            }
        };
        // GET entitySet/:id/:navigationProperty
        entitySet.entityType.navigationProperty.forEach(navigationProperty => {
            swaggerDocument.paths[app.resolveUrl(`~/api/${entitySet.url}/{id}/${navigationProperty.name}`)] = {
                "get": {
                    "tags": [
                        entitySet.entityType.name
                    ],
                    "security": [
                        {
                            "OAuth2": [
                                "students"
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "name": "id",
                            "in": "path",
                            "description": "The identifier of the object",
                            "required": true,
                            "schema": {
                                "type": "integer"
                            }
                        }
                    ],
                    "responses": {
                        "200": {
                            "description": "successful operation",
                            "content": {
                                "application/json": {
                                    "schema": getNavigationPropertyReturnType.bind(builder)(navigationProperty)
                                }
                            }
                        },
                        "204": {
                            "description": "No content"
                        },
                        "400": {
                            "description": "Bad request"
                        },
                        "404": {
                            "description": "Not Found"
                        }
                    }
                }
            };
        });

        entitySet.entityType.functions.forEach((func) => {
            swaggerDocument.paths[app.resolveUrl(`~/api/${entitySet.url}/{id}/${func.name}`)] = functionConfigurationToSwaggerPath.bind(builder)(entitySet, func);
        });

        entitySet.entityType.collection.functions.forEach((func) => {
            swaggerDocument.paths[app.resolveUrl(`~/api/${entitySet.url}/${func.name}`)] = collectionFunctionConfigurationToSwaggerPath.bind(builder)(entitySet, func);
        });

        entitySet.entityType.actions.forEach((action) => {
            swaggerDocument.paths[app.resolveUrl(`~/api/${entitySet.url}/{id}/${action.name}`)] = actionConfigurationToSwaggerPath.bind(builder)(entitySet, action);
        });

        entitySet.entityType.collection.actions.forEach((action) => {
            swaggerDocument.paths[app.resolveUrl(`~/api/${entitySet.url}/${action.name}`)] = collectionActionConfigurationToSwaggerPath.bind(builder)(entitySet, action);
        });

    });

    // set swagger document components (schemas and securitySchemes)
    let swaggerComponents = getSwaggerComponents(app);
    Object.assign(swaggerDocument.components,swaggerComponents.components);

    // set swagger application defined swagger definitions
    let swaggerUserDefinitions = getSwaggerUserDefinition(app);
    Object.assign(swaggerDocument.paths,swaggerUserDefinitions.paths);

    return swaggerDocument;
}



/**
 * @class
 */
class SwaggerService extends ApplicationService {

    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
    }

    /**
     *
     * @param {ExpressDataContext} context
     * @returns {*}
     */
    getSwaggerDocument(context) {
        return getSwaggerDocument(context.getApplication());
    }

}

module.exports.SwaggerService = SwaggerService;
