import {assert} from 'chai';
import app from '../app';
import crypto from 'crypto';
import util from 'util';
import fs from 'fs';
import path from 'path';
const readFile = util.promisify(fs.readFile);
import cmd from 'node-cmd';

const CourseExam = require("../models/course-exam-model");

describe('Test Encryption', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set test user
        context.user = {
            "name": "sysadmin"
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    function encrypt(input, output) {
        return new Promise((resolve, reject) => {
            const command = `openssl rsautl -encrypt -pubin -inkey ${path.resolve('./server/tests/openssl/server_pub_key.pem')} -in ${path.resolve(input)} -out ${path.resolve(output)}`;
            cmd.get(command, (err, data) => {
                if (err) {
                    return reject(err);
                }
                return resolve(data);
            });
        });
    }

    async function calculateSignedDocument(document) {
        try {
            const documentLength = document.length;
            let read_length = 0;
            let signedDocument = Buffer.alloc(0);
            // eslint-disable-next-line no-inner-declarations
            const writeFileAsync = util.promisify(fs.writeFile);
            const readFileAsync = util.promisify(fs.readFile);
            for (let index = 0; index < documentLength; index += read_length) {
                if ((index + 112) <= documentLength) {
                    read_length = 112
                } else {
                    read_length = documentLength - index;
                }
                let data_chunk = document.slice(index, index + read_length);
                await writeFileAsync('./server/tests/openssl/chunk', data_chunk);
                await encrypt('./server/tests/openssl/chunk', './server/tests/openssl/chunk.bin');
                let tmp_buffer = await readFileAsync('./server/tests/openssl/chunk.bin');
                signedDocument = Buffer.concat([signedDocument,tmp_buffer], signedDocument.length + tmp_buffer.length);
            }
            return signedDocument.toString('base64');
        } catch (e) {
            throw (e);
        }
    }

    /**
     * @param {string} input
     * @param {string} output
     * @return {Promise<*>}
     */
    // eslint-disable-next-line no-inner-declarations
    function sign(input, output) {
        return new Promise((resolve, reject) => {
            const command = `openssl dgst -sha1 -sign ${path.resolve('./server/tests/openssl/key.nopassword.pem')} -out ${output} ${input}`;
            cmd.get(command, (err, data) => {
                if (err) {
                    return reject(err);
                }
                return resolve(data);
            });
        });
    }

    it('should sign course exam document with openssl', async () => {
        const courseExamDocument = await context.model('CourseExamDocument')
            .where('id').equal(65590)
            .expand('courseExam')
            .silent()
            .getItem();
        assert.isDefined(courseExamDocument);

        // get encrypted document
        const bufferedDocument = Buffer.from(courseExamDocument.originalDocument);
        let signedDocument = await calculateSignedDocument(bufferedDocument);
        assert.isDefined(signedDocument);
        // get hash
        const hash_algorithm = crypto.createHash('sha1');
        hash_algorithm.update(Buffer.from(signedDocument, 'base64'));
        let checkHashKey = hash_algorithm.digest('base64');

        assert.isDefined(checkHashKey);

        // create signature block
        const writeFileAsync = util.promisify(fs.writeFile);
        const readFileAsync = util.promisify(fs.readFile);
        await writeFileAsync('./server/tests/.tmp/signedDocument.bin', Buffer.from(signedDocument, 'base64'));
        await sign('./server/tests/.tmp/signedDocument.bin', './server/tests/.tmp/signatureBlock.bin');
        const signatureBlockBuffer = await readFileAsync('./server/tests/.tmp/signatureBlock.bin');
        let signatureBlock = signatureBlockBuffer.toString('base64');
        assert.isDefined(signatureBlock);

        // create user certificate
        let userCertificate = (await readFileAsync('./server/tests/openssl/cert.pem', 'utf8'))
            .replace('-----BEGIN CERTIFICATE-----','')
            .replace('-----END CERTIFICATE-----','')
            .replace(/\r\n/g,'')
            .replace(/\n/g,'');
        assert.isDefined(userCertificate);

        courseExamDocument.signedDocument = signedDocument;
        courseExamDocument.signatureBlock = signatureBlock;
        courseExamDocument.checkHashKey = checkHashKey;
        courseExamDocument.userCertificate = userCertificate;

        await context.model('CourseExamDocument')
            .silent()
            .save(courseExamDocument);

    });

    it.only('should sign course exam document with crypto', async () => {
        /**
         * @type {CourseExamDocument}
         */
        const courseExamDocument = await context.model('CourseExamDocument')
            .where('id').equal(65590)
            .expand('courseExam')
            .silent()
            .getTypedItem();
        assert.isDefined(courseExamDocument);
        // update course exam document status
        courseExamDocument.documentStatus = 1;
        await context.model('CourseExamDocument')
            .silent()
            .save(courseExamDocument);

        const publicKeyPath = path.resolve('./server/tests/openssl/server_pub_key.pem');
        // get encrypted document
        let signedDocument = await CourseExam.calculateSignedDocument(publicKeyPath, Buffer.from(courseExamDocument.originalDocument));
        assert.isDefined(signedDocument);
        // get hash
        const hash_algorithm = crypto.createHash('sha1');
        hash_algorithm.update(Buffer.from(signedDocument, 'base64'));
        let checkHashKey = hash_algorithm.digest('base64');
        assert.isDefined(checkHashKey);

        // create signature block
        const writeFileAsync = util.promisify(fs.writeFile);
        const readFileAsync = util.promisify(fs.readFile);
        await writeFileAsync('./server/tests/.tmp/signedDocument.bin', Buffer.from(signedDocument, 'base64'));
        await sign('./server/tests/.tmp/signedDocument.bin', './server/tests/.tmp/signatureBlock.bin');
        const signatureBlockBuffer = await readFileAsync('./server/tests/.tmp/signatureBlock.bin');
        let signatureBlock = signatureBlockBuffer.toString('base64');
        assert.isDefined(signatureBlock);

        // create user certificate
        let userCertificate = (await readFileAsync('./server/tests/openssl/cert.pem', 'utf8'))
            .replace('-----BEGIN CERTIFICATE-----','')
            .replace('-----END CERTIFICATE-----','')
            .replace(/\r\n/g,'')
            .replace(/\n/g,'');
        assert.isDefined(userCertificate);

        courseExamDocument.signedDocument = signedDocument;
        courseExamDocument.signatureBlock = signatureBlock;
        courseExamDocument.checkHashKey = checkHashKey;
        courseExamDocument.userCertificate = userCertificate;

        await context.model('CourseExamDocument')
            .silent()
            .save(courseExamDocument);

    });

});
