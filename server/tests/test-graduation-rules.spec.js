import {assert} from 'chai';
import app from '../app';
import Student from '../models/student-model';
import {TestUtils} from '../utils';
import {ValidationResult} from "../errors";
import _ from "lodash";
import {TraceUtils} from "@themost/common";
import '@themost/promise-sequence';

var esprima = require('esprima');

describe('test graduation rules', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        // user with success: 'user_20100248@example.com'
        // user with success and complex rules: 'user_20131066@example.com'
        // user failure: 'user_20138601@example.com'
        context.user = {
            name: 'user_20138601@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('should check graduation rules', async ()=> {
        /**
         * @type Student
         */
        const student = await Student.getMe(context).getTypedItem();
        const data = {
            student: student
        };
        let validationResult = {};
        // check if student has personal graduation rules
        let graduationRules = await context.model('Rule').where('additionalType').equal('GraduateRule')
            .and('targetType').equal('Student')
            .and('target').equal(student.id.toString())
            .getAllItems();

        // get program specialty graduation rules
        if (graduationRules && graduationRules.length===0)
        {
            graduationRules = await context.model('Rule').where('additionalType').equal('GraduateRule')
                .and('targetType').equal('Program')
                .and('target').equal(student.studyProgram.toString())
                .and('programSpecialty').equal(student.specialtyId)
                .getAllItems();

        }
        if (graduationRules && graduationRules.length===0) {
            validationResult = new ValidationResult(false, 'FAIL', 'Graduation rules have not been set.');
        }
        else {
            let validationResults = [];
            // add async function for validating graduation rules
            let forEachRule = async graduationRule => {
                try {
                    return await new Promise((resolve) => {
                        const ruleModel = context.model(graduationRule.refersTo + 'Rule');
                        if (_.isNil(ruleModel)) {
                            validationResults = validationResults || [];
                            validationResults.push(new ValidationResult(false, 'FAIL', context.__('Student validation rule type cannot be found.')));
                        }
                        const rule = ruleModel.convert(graduationRule);
                        rule.validate(data, function (err, result) {
                            if (err) {
                                validationResults = validationResults || [];
                                validationResults.push(new ValidationResult(false, 'FAIL', context.__('Student validation rule type cannot be found.')));
                            }
                            /**
                             * @type {ValidationResult[]}
                             */
                            validationResults = validationResults || [];
                            validationResults.push (result);
                            graduationRule.validationResult = result;
                            return resolve();
                        });
                    });
                } catch (err) {
                    TraceUtils.error(err);
                }
            };
            // call all promises
            await Promise.sequence(graduationRules.map((graduationRule) => {
                return () => forEachRule(graduationRule);
            }));

            const fnValidateExpression = function (x) {
                try {
                    let expr = x['ruleExpression'];
                    if (_.isEmpty(expr)) {
                        return false;
                    }
                    expr = expr.replace(/\[%(\d+)]/g, function () {
                        if (arguments.length === 0) return;
                        const id = parseInt(arguments[1]);
                        const v = validationResults.find(function (y) {
                            return y.id === id;
                        });
                        if (v) {
                            return v.success.toString();
                        }
                        return 'false';
                    });
                    expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                    return eval(expr);
                } catch (e) {
                    return e;
                }
            };

            const fnTitleExpression = function (x) {
                try {
                    let expr = x['ruleExpression'];
                    if (_.isEmpty(expr)) {
                        return false;
                    }
                    expr = expr.replace(/\[%(\d+)]/g, function () {
                        if (arguments.length === 0) return;
                        const id = parseInt(arguments[1]);
                        const v = validationResults.find(function (y) {
                            return y.id === id;
                        });
                        if (v) {
                            return '(' + v.message.toString() + ')';
                        }
                        return 'Unknown Rule';
                    });
                    expr = expr.replace(/\bAND\b/ig, context.__(' AND ')).replace(/\bOR\b/ig, context.__(' OR ')).replace(/\bNOT\b/ig, context.__(' NOT '));
                    return expr;
                } catch (e) {
                    return e;
                }
            };

            let res, title;

            //apply default expression
            const expr = graduationRules.find(function (x) {
                return !_.isEmpty(x['ruleExpression']);
            });
            let finalResult;


            if (expr) {
                res = fnValidateExpression(expr);
                title = fnTitleExpression(expr);
                finalResult= new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                finalResult.type = 'GraduationRule';
                expr.ruleExpression= expr.ruleExpression.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                expr.ruleExpression = expr.ruleExpression.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    return parseInt(arguments[1]);
                    });
                finalResult.expression= esprima.parse(expr.ruleExpression);

            } else {
                //get expression (for this rule type)
                const ruleExp1 = graduationRules.map(function (x) {
                    return '[%' + x.id + ']';
                }).join(' AND ');
                res = fnValidateExpression({ruleExpression: ruleExp1});
                title = fnTitleExpression({ruleExpression: ruleExp1});
                finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                finalResult.type = 'GraduationRule';
                finalResult.expression = null;
            }
            //build
            let response = {"finalResult":finalResult,"graduationRules":graduationRules}
            console.log (JSON.stringify(response,null,2));


        }

    });
});
