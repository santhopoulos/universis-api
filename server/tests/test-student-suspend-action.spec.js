import {expect} from 'chai';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from '@themost/express';
import app from '../app';
import {TestUtils} from "../utils";

describe('StudentSuspendAction', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    before((done) => {
        context = app.get(ExpressDataApplication.name).createContext();
        return done();
    });

    it('should use model', async () => {
        const model = context.model('StudentSuspendAction');
        expect(model).to.be.ok;
    });


    it('should insert a student suspend action', async () => {
        await TestUtils.executeInTransaction(context, async () => {
            // get an active student
            const student = await context.model('Student')
                .where('semester').equal(2)
                .and('studentStatus/alternateName').equal('active')
                .silent().getTypedItem();
            expect(student).to.be.ok;
            // add a student suspend action
            let newAction = {
                object: student.id,
                suspensionDate: new Date(),
                suspensionPeriod: 1,
                suspensionYear: 2017,
                suspensionPeriods: 2,
                comments: 'A reason',
                suspensionExpiredDate: '2020-08-01'
            };
            // save action
            await context.model('StudentSuspendAction').silent().save(newAction);
            expect(newAction.id).to.be.ok;
            // expect state to be active
            newAction = await context.model('StudentSuspendAction').where('id').equal(newAction.id)
                .silent()
                .getTypedItem();
            expect(newAction).to.be.ok;
            expect(newAction.actionStatus.alternateName).to.be.equal('ActiveActionStatus');
        });
    });

    it('should accept a student suspend action', async () => {
        await TestUtils.executeInTransaction(context, async () => {
            // get an active student
            const student = await context.model('Student')
                .where('semester').equal(2)
                .and('studentStatus/alternateName').equal('active')
                .orderByDescending('dateModified')
                .silent().getTypedItem();
            expect(student).to.be.ok;
            // add a student completed suspend action
            let newAction = {
                object: student.id,
                suspensionDate: new Date(),
                suspensionPeriod: 1,
                suspensionYear: 2017,
                suspensionPeriods: 2,
                comments: 'A reason',
                suspensionExpiredDate: '2020-08-01',
                actionStatus: {
                    alternateName: "CompletedActionStatus"
                }
            };
            // save action
            await context.model('StudentSuspendAction').silent().save(newAction);
            expect(newAction.id).to.be.ok;
            // expect state to be active
            newAction = await context.model('StudentSuspendAction').where('id').equal(newAction.id)
                .silent()
                .getTypedItem();
            expect(newAction).to.be.ok;
            expect(newAction.actionStatus.alternateName).to.be.equal('CompletedActionStatus');
        });
    });

    it('should reintegrate a student with suspend action', async () => {
        await TestUtils.executeInTransaction(context, async () => {
            // get an active student
            let studentSuspension = await context.model('StudentSuspension')
                .where('reintegrated').equal(0)
                .orderByDescending('dateModified')
                .silent().getTypedItem();

            // update   student suspension
            studentSuspension.reintegrationDate = new Date();
            studentSuspension.reintegrated = 1,
                studentSuspension.reintegrationYear = 2018,
                studentSuspension.reintegrationPeriod = 1;
            studentSuspension.reintegrationSemester = '3';


            // save action
            await context.model('StudentSuspension').silent().save(studentSuspension);
            expect(studentSuspension.id).to.be.ok;
            // expect state to be active
            studentSuspension = await context.model('StudentSuspension').where('id').equal(studentSuspension.id)
                .silent()
                .getTypedItem();
            expect(studentSuspension).to.be.ok;
            expect(studentSuspension.reintegrated).to.be.equal(1);
        });
    });

});
