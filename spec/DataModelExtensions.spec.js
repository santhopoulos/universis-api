import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";

describe('DataModel', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });
    it('should use DataModel.getReferenceMappings', async ()=> {
        let dataModel = context.model('Institute');
        let mappings = await dataModel.getReferenceMappings();
        expect(mappings).toBeTruthy();
        expect(mappings.length).toBeGreaterThan(0);
        expect(dataModel.getReferenceMappings.cache).toBeTruthy();
        // validate cache
        let value = dataModel.getReferenceMappings.cache.get(`${dataModel.name}.getReferenceMappings(false)`);
        expect(value).toBeTruthy();
        dataModel.getReferenceMappings.cache.clear();
        value = dataModel.getReferenceMappings.cache.get(`${dataModel.name}.getReferenceMappings(false)`);
        expect(value).toBeFalsy();
        // call again
        mappings = await dataModel.getReferenceMappings();
        expect(mappings).toBeTruthy();
        expect(mappings.length).toBeGreaterThan(0);
        value = dataModel.getReferenceMappings.cache.get(`${dataModel.name}.getReferenceMappings(false)`);
        expect(value).toBeTruthy();

    });
});
