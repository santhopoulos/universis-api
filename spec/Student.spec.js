import app from '../server/app';
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';


const executeInTransaction = TestUtils.executeInTransaction;

describe('Student', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });

    it('should get a student unique identifier', async() => {
        await executeInTransaction(context, async () => {
            expect(context).toBeInstanceOf(ExpressDataContext);
            // get a student with candidate status
            const student = await context.model('Student')
                .where('studentStatus/alternateName')
                .equal('candidate')
                .expand({
                    'name': 'department',
                    'options': {'expand': 'departmentConfiguration', 'filter': 'departmentConfiguration ne null'}
                })
                .silent()
                .first();
            expect(student).toBeTruthy();
            // get department configuration
            const departmentConfiguration = await context.model('DepartmentConfiguration')
                .where('department').equal(student.department.id)
                .silent()
                .first();
            expect(departmentConfiguration).toBeTruthy();
            // update configuration
            departmentConfiguration.studentUniqueIdentifierFormat = 'T12311;R10;M10';
            departmentConfiguration.studentUniqueIdentifierIndex = 1;
            await context.model('DepartmentConfiguration').silent().save(departmentConfiguration);
            /*---Student Update---*/
            student.studentStatus = {
                alternateName: 'active'
            };
            // mock an admin user for student-update listener
            const user = await context.model('User').where('groups/name').equal('Administrators').silent().first();
            expect(user).toBeTruthy();
            context.user = {
                name: user.name
            };
            // validate results
            const result = await context.model('Student').silent().save(student);
            expect(result).toBeTruthy();
            expect(result.uniqueIdentifier).toBeTruthy();
            expect(result.uniqueIdentifier).toBeInstanceOf(String)
            expect(result.uniqueIdentifier).toMatch(/^12311/);
            expect(result.uniqueIdentifier.length).toEqual(16);
            const crc = Math.floor(parseInt(result.uniqueIdentifier) / 10) % 10;
            expect(result.uniqueIdentifier).toMatch(`${crc}$`);
            const exists = await context.model('Student').where('uniqueIdentifier').equal(result.uniqueIdentifier).silent().count();
            expect(exists).toBeTruthy();
            expect(exists).toEqual(1);
            /*---Student Create---*/
            const newStudent = student;
            // delete old properties
            delete newStudent.id;
            delete newStudent.person.id;
            delete newStudent.studentIdentifier;
            delete newStudent.studentInstituteIdentifier;
            delete newStudent.uniqueIdentifier;
            // insert new student
            const resultNewStudent = await context.model('Student').silent().save(newStudent);
            // validate results
            expect(resultNewStudent).toBeTruthy();
            expect(resultNewStudent.uniqueIdentifier).toBeTruthy();
            expect(resultNewStudent.uniqueIdentifier).toBeInstanceOf(String)
            expect(resultNewStudent.uniqueIdentifier).toMatch(/^12311/);
            expect(resultNewStudent.uniqueIdentifier.length).toEqual(16);
            const crcNew = Math.floor(parseInt(resultNewStudent.uniqueIdentifier) / 10) % 10;
            expect(resultNewStudent.uniqueIdentifier).toMatch(`${crcNew}$`);
            const existsNew = await context.model('Student').where('uniqueIdentifier').equal(result.uniqueIdentifier).silent().count();
            expect(existsNew).toBeTruthy();
            expect(existsNew).toEqual(1);
        });
    });

});
