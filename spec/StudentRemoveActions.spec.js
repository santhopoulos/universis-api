import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
const executeInTransaction = TestUtils.executeInTransaction;
describe('StudentRemoveAction', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        // re-enable development environment
        process.env.NODE_ENV = 'development';
        return done();
    });
    afterEach((done) => {
       delete context.user;
       return done();
    });

    it('should get action', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .silent()
                .expand('user', 'department')
                .getTypedItem();
            expect(student).toBeTruthy();
            // add action
            let newAction = {
                object: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod
            };
            await context.model('StudentRemoveAction').silent().save(newAction);
            newAction = await context.model('StudentRemoveAction')
                .where('id').equal(newAction.id)
                .silent()
                .expand('object')
                .getItem();
            expect(newAction).toBeTruthy();
            expect(newAction.object.id).toBe(student.id);
            // get student status
            const isActive = await student.isActive();
            expect(isActive).toBeTruthy();
        });
    });
    it('should add action as student', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .silent()
                .expand('user', 'department')
                .getTypedItem();
            expect(student).toBeTruthy();
            // set context user (student)
            context.user = {
                name: student.user.name
            };
            // add action
            let newAction = {
                object: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            await context.model('StudentRemoveAction').save(newAction);
            newAction = await context.model('StudentRemoveAction')
                .where('id').equal(newAction.id)
                .expand('object')
                .getItem();
            expect(newAction).toBeTruthy();
            expect(newAction.object.id).toBe(student.id);
            // get student status
            const isActive = await student.isActive();
            expect(isActive).toBeTruthy();
            // throw error
            // get another student
            let otherStudent = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('id').notEqual(student.id)
                .silent()
                .getItem();
            // try to add action for another student
            newAction = {
                object: otherStudent,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            await expectAsync((function () {
                return context.model('StudentRemoveAction').save(newAction);
            })()).toBeRejectedWithError('Access Denied');
            // try to add action with invalid status
            newAction = {
                object: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'CompletedActionStatus'
                }
            };
            await expectAsync((function () {
                return context.model('StudentRemoveAction').save(newAction);
            })()).toBeRejectedWithError('Access Denied');
        });
    });

    it('should get action as student', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .silent()
                .expand('user', 'department')
                .getTypedItem();
            expect(student).toBeTruthy();
            // set context user (student)
            context.user = {
                name: student.user.name
            };
            // add action
            let newAction = {
                object: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            await context.model('StudentRemoveAction').save(newAction);
            newAction = await context.model('StudentRemoveAction')
                .where('id').equal(newAction.id)
                .expand('object')
                .getItem();
            expect(newAction).toBeTruthy();
            expect(newAction.object.id).toBe(student.id);

            // get another student
            let otherStudent = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('id').notEqual(student.id)
                .silent()
                .getItem();
            newAction = {
                object: otherStudent,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            // save silently
            newAction = await context.model('StudentRemoveAction').silent().save(newAction);
            // try to get item
            newAction = await context.model('StudentRemoveAction')
                .where('id').equal(newAction.id)
                .getItem();
            expect(newAction).toBeFalsy();

        });
    });
});
