import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import {createStudyProgram} from './data';
import Course from '../server/models/course-model';
import StudyProgramCourse from "../server/models/study-program-course-model";
const executeInTransaction = TestUtils.executeInTransaction;

describe('StudyProgramCourse', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        process.env.NODE_ENV = 'development';
        return done();
    });

    it('should create study program course', async () => {
        await executeInTransaction(context, async () => {
            // create studyProgram
            const studyProgram = await createStudyProgram(context);
            expect(studyProgram).toBeTruthy();
            // get course
            const course = await context.model(Course)
                .where('displayCode').equal('L100')
                .silent().getItem();
            expect(course).toBeTruthy();
            let studyProgramCourse = {
                course: course,
                studyProgram: studyProgram,
                sortIndex: 1
            }
            await context.model('StudyProgramCourse')
                .silent().save(studyProgramCourse);
            studyProgramCourse = await context.model(StudyProgramCourse)
                .where('course').equal(course.id)
                .and('studyProgram').equal(studyProgram.id)
                .silent().getItem();
            expect(studyProgramCourse).toBeTruthy();
        });

    });

});
