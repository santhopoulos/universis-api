import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import { TraceUtils } from '@themost/common';
const executeInTransaction = TestUtils.executeInTransaction;

describe('Thesis', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        // disable sql logging
        process.env.NODE_ENV = 'test';
        // disable debug logging
        TraceUtils.level('info');
    });
    
    afterAll(() => {
        // enable sql logging for further testing
        process.env.NODE_ENV = 'development';
        // enable debug logging
        TraceUtils.level('debug');
    });
    
    beforeEach( done => {
        // clear configuration cache
        const configuration = app.get(ExpressDataApplication.name).getConfiguration();
        configuration.cache = {};
        context = app.get(ExpressDataApplication.name).createContext();
        return done();
    });

    afterEach( done => {
       if (context) {
           return context.finalize( ()=> done() );
       }
       return done();
    });
    
    function createNewThesisFor(student) {
        return {
                // set name
                name: 'Student information systems in medium and large universities',
                // set department from student
                department: student.department,
                // set academic year and period
                startYear: student.inscriptionYear,
                startPeriod: student.inscriptionPeriod,
                // start date
                startDate: new Date(),
                type: {
                    alternateName: 'degree'
                },
                status: {
                    alternateName: 'active'
                },
                // set grade scale from study program
                gradeScale: student.studyProgram.gradeScale
            }
    }

    it('should add thesis', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            const student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .expand('studyProgram')
                .silent()
                .getItem();
            expect(student).toBeTruthy();
            // get an instructor
            const instructor = await context.model('Instructor')
                .where('department').equal(student.department)
                .silent()
                .getItem();
            expect(instructor).toBeTruthy();
            // create a thesis
            let thesis = createNewThesisFor(student);
            // set instructor
            thesis.instructor = instructor;
            // save thesis
            await context.model('Thesis').silent().save(thesis);
            expect(thesis.id).toBeTruthy();
            // get thesis
            thesis = await context.model('Thesis').silent().where('id').equal(thesis.id)
                .expand('members')
                .silent()
                .getItem();
            expect(thesis).toBeTruthy();
        });
    });

    it('should add thesis member', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            const student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .expand('studyProgram')
                .silent()
                .getItem();
            expect(student).toBeTruthy();
            // get an instructor
            let instructor = await context.model('Instructor')
                .where('department').equal(student.department)
                .silent()
                .getItem();
            expect(instructor).toBeTruthy();
            // create a thesis
            let thesis = createNewThesisFor(student);
            // set instructor
            thesis.instructor = instructor;
            // save thesis
            await context.model('Thesis').silent().save(thesis);
            expect(thesis.id).toBeTruthy();
            // update instructor
            let otherInstructor = await context.model('Instructor')
                .where('department').equal(student.department)
                .and('id').notEqual(instructor.id)
                .silent()
                .getItem();
            expect(otherInstructor).toBeTruthy();
            // add thesis member
            await context.model('ThesisRole').silent().insert({
                thesis: thesis,
                roleName: 'committee',
                member: otherInstructor
            });
            // get thesis
            thesis = await context.model('Thesis').silent().where('id').equal(thesis.id)
                .expand({ name: 'members', 
                    options: {
                        $expand: 'member'
                    }
                })
                .silent()
                .getItem();
            expect(thesis).toBeTruthy();
            expect(thesis.members.length).toBe(2);
        });
    });

    it('should update thesis instructor', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            const student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .expand('studyProgram')
                .silent()
                .getItem();
            expect(student).toBeTruthy();
            // get an instructor
            let instructor = await context.model('Instructor')
                .where('department').equal(student.department)
                .silent()
                .getItem();
            expect(instructor).toBeTruthy();
            // create a thesis
            let thesis = createNewThesisFor(student);
            // set instructor
            thesis.instructor = instructor;
            // save thesis
            await context.model('Thesis').silent().save(thesis);
            expect(thesis.id).toBeTruthy();
            // update instructor
            let otherInstructor = await context.model('Instructor')
                .where('department').equal(student.department)
                .and('id').notEqual(instructor.id)
                .silent()
                .getItem();
            expect(otherInstructor).toBeTruthy();
            // set instructor again
            thesis.instructor = otherInstructor;
            // save thesis
            await context.model('Thesis').silent().save(thesis);
            // get thesis
            thesis = await context.model('Thesis').silent().where('id').equal(thesis.id)
                .expand({ name: 'members', 
                    options: {
                        $expand: 'member'
                    }
                })
                .silent()
                .getItem();
            expect(thesis).toBeTruthy();
            expect(thesis.members.length).toBe(1);
            expect(thesis.members[0].member.familyName).toBe(otherInstructor.familyName);
        });
    });

    it('should get thesis as student', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            const student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .expand('studyProgram', 'user')
                .silent()
                .getItem();
            expect(student).toBeTruthy();
            // get an instructor
            const instructor = await context.model('Instructor')
                .where('department').equal(student.department)
                .silent()
                .getItem();
            expect(instructor).toBeTruthy();
            // create a thesis
            let thesis = createNewThesisFor(student);
            // add instructor
            thesis.instructor = instructor;
            // save thesis
            await context.model('Thesis').silent().save(thesis);
            expect(thesis.id).toBeTruthy();
            const id = thesis.id;
            thesis = await context.model('Thesis').where('id').equal(id).expand('instructor').silent().getItem();
            // set context user
            context.user = student.user;
            thesis = await context.model('Thesis').where('id').equal(id).getItem();
            expect(thesis).toBeFalsy();
            // get thesis silently
            thesis = await context.model('Thesis').where('id').equal(id).silent().getItem();
            // add student thesis
            let studentThesis = {
                thesis,
                student
            }
            await context.model('StudentThesis').silent().save(studentThesis);
            thesis = await context.model('Thesis')
                .where('id').equal(thesis.id)
                .expand('instructor')
                .getItem();
            expect(thesis).toBeTruthy();
            expect(thesis.instructor).toBeTruthy();
            expect(thesis.instructor.email).toBeFalsy();
        });
    });

    it('should remove thesis', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            const student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .expand('studyProgram')
                .silent()
                .getItem();
            expect(student).toBeTruthy();
            // create a thesis
            let thesis = createNewThesisFor(student);
            // save thesis
            await context.model('Thesis').silent().save(thesis);
            expect(thesis.id).toBeTruthy();
            // remove thesis
            await context.model('Thesis').silent().remove(thesis);
        });
    });

    it('should add student thesis', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            const student = await context.model('Student').where('studentStatus/alternateName').equal('active')
                .expand('studyProgram')
                .silent()
                .getItem();
            expect(student).toBeTruthy();
            // create a thesis
            let thesis = createNewThesisFor(student);
            // save
            await context.model('Thesis').silent().save(thesis);
            let studentThesis = {
                thesis,
                student
            }
            // add student to thesis
            await context.model('StudentThesis').silent().save(studentThesis);
            expect(studentThesis.id).toBeTruthy();
        });
    });

});
